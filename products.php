<?php
$PAGE_TITLE = "Products";
include_once("include/meta-header.php");
include_once("include/header.php");

$whereCls = "status = 'A' AND isDeleted = 'N'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_segments', $whereCls, '', '', 'createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
?>
  <section class="overflow-hidden journey-us-panel">
    <div class="divider8"></div>
    <div class="container mb-5 my-5">
      <div class="row justify-content-center align-items-center h-100">
        <div class="col-sm-12 mb-4">
          <div class="row gx-4 gy-4">
<?php
			$numOfRows = count($rcdInfoArr);
			$imgPathee = HTTP_PATH_ASSET.'/images/oil.png';
			$img = "<img src='$imgPathee' width='15' class='v-align-sub'>";
			for ($i = 0; $i < $numOfRows; $i++)
			{
				$imgPath = HTTP_PATH_ASSET_UPLOAD.'/segment_imgs/'.$rcdInfoArr[$i]['postThumb']; 
?>           
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
				  <a href="<?php echo HTTP_PATH?>/products/<?php echo $rcdInfoArr[$i]['slug']?>">
					<div class="position-relative product-section rounded-3">
					  <img src="<?php echo $imgPath?>" class="w-100 rounded rounded-border-radius">
					  <div class="card-set">
						<span><?php echo str_replace('OIL', $img.' IL', strtoupper($rcdInfoArr[$i]['postTitle']));?></span>
						<p style="font-size: 12px;"> SEGMENT</p>
					  </div>
					</div>
				  </a>
				</div> 
<?php
			}
?>

          </div>
        </div>
      </div>
    </div>
  </section>
<?php
}
    include_once("include/footer.php");
?> 