<?php
function generatePassword($value)
{
	return MD5(TOKEN_SALT . $value);
}

function replaceDataInList($string, $list)
{
	$find = array_keys($list);
	$replace = array_values($list);
	return str_ireplace($find, $replace, $string);
}

function randomMD5()
{
	return MD5(TOKEN_SALT . time() . mt_rand());
}

function trimFormValue($trace, $array, $isArrKeyExists = 'N')
{
	if ($isArrKeyExists == 'N' )
	{
		$rtnArr = array_map('trim', $array);
	}
	else 
	{
		$rtnArr = array();
		foreach ($array AS $key => $value)
		{
			if (is_array($value))
			{
				$rtnArr[$key] = trim(implode(DB_DATA_SPLITTER, $value));
			}
			else $rtnArr[$key] = trim($value); 
		}
	}
	if ($trace)
	{
		echo "<pre><-------------Input array value-------------><br>";
		print_r($array);
		echo "<-------------Output array value-------------><br>";
		print_r($rtnArr);		
		echo "</pre>";
		die;
	}
	return $rtnArr;
}

function strDataExplode($str, $splitterStr = DB_DATA_SPLITTER)
{
	return explode($splitterStr, $str);
}

function prepareKeyValue4Msql($trace, $array, $keyExcludeArr)
{
	$rtnArr = array();
	
	foreach ($array AS $key => $value)
	{
		if (!in_array($key, $keyExcludeArr)) $rtnArr[$key] = $value;
	}
	
	if ($trace)
	{
		echo "<pre><-------------Input array value-------------><br>";
		print_r($array);
		echo "<-------------Output array value-------------><br>";
		print_r($rtnArr);		
		echo "</pre>";
		die;
	}
	return $rtnArr;

}

function prepareKeyValueForDBQuery($trace, $array, $arrAllowKeys, $arrExcludeKeys)
{
	$rtnArr = array();
	
	foreach ($array AS $key => $value)
	{
		if (!in_array($key, $arrExcludeKeys) && in_array($key, $arrAllowKeys)) $rtnArr[$key] = $value;
	}
	
	if ($trace)
	{
		echo "<pre><-------------Input array value-------------><br>";
		print_r($array);
		echo "<-------------Input arrAllowKeys-------------><br>";
		print_r($arrAllowKeys);
		echo "<-------------Input arrExcludeKeys-------------><br>";
		print_r($arrExcludeKeys);
		echo "<-------------Output array value-------------><br>";
		print_r($rtnArr);		
		echo "</pre>";
		die;
	}
	return $rtnArr;

}

function checkTimeFormat($timeFormat)
{
	$timeFormat = trim($timeFormat);

	if (strstr($timeFormat, ':'))
	{
		list($hr,$min) = @explode(":", $timeFormat);

		if (@is_numeric($hr) && @is_numeric($min)) 
		{
			if (strlen($hr) == 2 && strlen($min) == 2) 
			{
				return 1;
			}
		}
	}

	return 0;
}

function makeRandNo6Digit()
{
	return rand(100000, 999999);
}

function makeRandNo8Digit()
{
	return rand(10000000, 99999999);
} 

function unixtime64($str)
{
   date_default_timezone_set("UTC");
   $dateTime = new DateTime($str);
   return $dateTime->format("U");
}

function mysqlDate($value)
{
	if ($value) {
		if(MYSQL_DATE_CONVERSION_STYLE == 'EU') list($dd, $mm, $yy) = explode(DATE_FORMAT_SPLITTER, $value);
		else if(MYSQL_DATE_CONVERSION_STYLE == 'US') list($mm, $dd, $yy) = explode(DATE_FORMAT_SPLITTER, $value);
		return "$yy-$mm-$dd"; // Obtain the final date
	}
}

function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	{				
		// Check ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} 
	else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	{   
		// Check if ip is pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} 
	else
	{
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}


function headerRedirect($url)
{
	ob_start();
	header('location:'.$url);
	exit;
}

function viewState($viewStateArray, $mode)
{
	if ($mode) 
	{
		foreach ($viewStateArray as $key => $value) 
		{
			$_SESSION['session_' . $key] = $value;
		}
	} 
	else 
	{
		foreach ($viewStateArray as $key => $value) 
		{
			unset($_SESSION['session_' . $key]);
		}
	}
}

function getTimestamp($value, $dateFormat)
{
	if ($value)
	{
		return @date($dateFormat,strtotime($value));
	}
}

function allowedFIleExten($indexName, $arrAllowedExtension = array('png', 'jpg', 'jpeg'))
{
	$rten = 0;
	if (!empty($_FILES[$indexName]['name']))
	{
		$fileName = trim($_FILES[$indexName]['name']);	
		$arrPathInfo = pathinfo($fileName);
		$fileExten = strtolower($arrPathInfo['extension']);
		if (!in_array($fileExten, $arrAllowedExtension)) $rten = 1;
	}
	return $rten;
}

function fileUpload($trace, $indexName, $dirLocation, $strConcatSym = '_')
{
	$newFileName = '';
	$filePath = HARD_PATH . "/public_assets/uploads/".$dirLocation;	

	if (!empty($_FILES[$indexName]['name']))
	{
		$newFileName = time(). $strConcatSym .substr(randomMD5(), 1, 7). $strConcatSym .str_replace(array(' ',  '-', '__'), array('_', '_', '_'),  $_FILES[$indexName]['name']);
		if (move_uploaded_file($_FILES[$indexName]['tmp_name'], $filePath .'/'. $newFileName)) chmod($filePath . '/'.$newFileName, 0777);		
	}

	if ($trace)
	{
		print_r($_FILES);
		echo "New File Name: $newFileName<br>File Path: $filePath/".$newFileName;
		die;
	}
	return $newFileName;
}

function unlinkFile($trace, $fileName, $dirLocation)
{	
	if ($trace)
	{
		echo "File Name: $fileName<br>File Path: $filePath/".$fileName;
		die;
	}

	if ($fileName)
	{
		$filePath = HARD_PATH . "/uploads/".$dirLocation;
		@chmod($filePath . '/'.$fileName, 0777);		
		@unlink($filePath . '/'.$fileName);		
	}
}

function showSessionMessage()
{
	if (isset($_SESSION['messageSession'])) 
	{
		echo $_SESSION['messageSession'];
		unset($_SESSION['messageSession']);
		unset($_SESSION['msgTrue']);
	}
}

function sendEmail($to, $subject, $body, $fromName, $from, $format = '')
{
	$ARR_CHARS_STR = array("�" => "&#192;", "�" => "&#193;", "�" => "&#194;", "�" => "&#195;", "�" => "&#199;", "�" => "&#201;", "�" => "&#202;", "�" => "&#205;", "�" => "&#211;", "�" => "&#212;", "�" => "&#213;", "�" => "&#218;", "�" => "&#220;", "�" => "&#224;", "�" => "&#225;", "�" => "&#226;", "�" => "&#227;", "�"=> "&#227;", "�"=> "&#233;", "�"=> "&#234;", "�"=> "&#237;", "�"=> "&#243;", "�"=> "&#244;", "�"=> "&#245;", "�" => "&#250;", "�" => "&#252;");	

	$enc1 = mb_detect_encoding($subject, "UTF-8,ISO-8859-1");
	$subject = iconv($enc1, "UTF-8", $subject);

	$enc = mb_detect_encoding($body, "UTF-8,ISO-8859-1");
	$body = iconv($enc, "UTF-8", $body);
	$body = $body."<br><br>Thanks,<br>".SIGNATURE;
	
	$headers = "MIME-Version: 1.0\r\n";
	if($format=='HTML')
	{		
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
	}

	$headers .= "From: $fromName <$from>" . "\r\n";
	$headers .= "Cc: " . "\r\n";
	$headers .= "Bcc: " . "\r\n";
	//<img src='{$url}'>
	$body = "<center>
				<table width='100%' cellpadding='2' cellspacing='2' bgcolor='#FFCC29' style='color: #fff; text-align:left; border: 2px solid #000;'>
				<tr>
					<td style='padding:15px 15px 15px 15px; font-size: 12px; color: #fff; line-height:1.3; text-align:justify; font-family: Arial,Helvetica,sans-serif;'>" . $body . "<td>
				</tr>
				</table>
			</center>";

	if ($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '10.1.1.5')
	{
		$str = "<font face='arial' size='2'><b>To Email:</b> $to<br><br><b>Subject:</b> $subject<br><br><b>From:</b> $fromName<br><br><b>From Email:</b> $from<br><br>$body</font>";
		$mailDir = HARD_PATH . '/gen_mail';

		$fp = fopen($mailDir . '/mail_' . date('U') . '_' . rand(10000, 99999) . '.html', 'w');
		fwrite($fp, $str);
		fclose($fp); 
	} 
	else
	{
		$res = mail($to, $subject, $body, $headers, '-f ' . NO_REPLY_EMAIL);		
		return $res;
	}
}

function makeEmailStructure($accessCase, $to, $fname, $username = '', $password = '', $newEmail = '')
{
	switch ($accessCase) 
	{
		case 'accountPasswordReset':
				$body = "Hello ".$fname.",<br><br>Your password has been reset in our record.<br><br>
				Please note your new login information for future use:<br><br>Email Id: ".$username."<br>Password: ".$password."<br><br>
				<a href='".HTTP_PATH."' target='_blank'>Click here to login</a><br><br>Thanks,<br>".SIGNATURE;

				sendEmail($to, USER_PANEL_TITLE.' - Your Account Password Reset', $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
				break;	

		case 'forgotPassword':
				$body = "Hello ".$fname.",<br><br>Your password has been reset in our record.<br><br>
				Please note your new login information for future use:<br><br>Email Id: ".$username."<br>Password: ".$password."<br><br>
				<a href='".HTTP_PATH_ADMIN."' target='_blank'>Click here to login</a><br><br>Thanks,<br>".SIGNATURE;
				
				sendEmail($to, USER_PANEL_TITLE.' - Your Account Password Reset', $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
				break;
				
		case 'changePassword':
				$body = "Hello ".$fname.",<br><br>Your account password has been updated in our record.<br><br>
				Please note your new login information for future use:<br><br>Email Id: ".$username."<br>Password: ".$password."<br><br>
				<a href='".HTTP_PATH_ADMIN."' target='_blank'>Click here to login</a><br><br>Thanks,<br>".SIGNATURE;
				
				sendEmail($to, USER_PANEL_TITLE.' -  Account Password Updated', $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
				break;		

		case 'updateYourEmail':
				$body = "Hello ".$fname.",<br><br>Your email address has been changed in our record.<br><br>
				Your new email address is ".$_SESSION['userDetails']['email']."<br><br>
				<a href='".HTTP_PATH_ADMIN."' target='_blank'>Click here to login</a><br><br>Thanks,<br>".SIGNATURE;
				
				sendEmail($to, USER_PANEL_TITLE.' - Your Email Address Changed', $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
				break;		

		case 'updateAdminEmail':
				$body = "Hello ".$fname.",<br><br>Your email address has been changed in our record.<br><br>
				Your new email address is ".$newEmail."<br><br>
				<a href='".HTTP_PATH_ADMIN."' target='_blank'>Click here to login</a><br><br>Thanks,<br>".SIGNATURE;
				
				sendEmail($to, USER_PANEL_TITLE.' - Your Email Address Changed', $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
				break;		

		case 'addUserAccount':
				$body = "Hello ".$fname.",<br><br>Your account has been created successfully.<br><br>
				Please note your login information for future use:<br><br>Email Id: ".$username."<br>Password: ".$password."<br><br>
				<a href='".HTTP_PATH_ADMIN."' target='_blank'>Click here to login</a><br><br>Thanks,<br>".SIGNATURE;
				
				sendEmail($to, USER_PANEL_TITLE.' - Account Created', $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
				break;
	}
 }

# Search value in multidimentional array
function inArrayMulti($findValue, $arrayName, $strict = false)
{
	foreach ($arrayName as $item) 
	{
		if (($strict ? $item === $findValue : $item == $findValue) || (is_array($item) && inArrayMulti($findValue, $item, $strict)))
		{
			return true;
		}
	}
	return false;
}

function getValPostORGet($indexName, $method = 'P')
{
	$rtenVal = '';
	if (!empty($_POST[$indexName]) && ($method == "P" || $method == "B")) $rtenVal = trim($_POST[$indexName]);
	else if (!empty($_GET[$indexName]) && ($method == "G" || $method == "B")) $rtenVal = trim($_GET[$indexName]);
	return $rtenVal;
}


function statusClsActive($frmVal, $statusVal)
{
	$rtnStr = "";
	if (strtolower($frmVal) == strtolower($statusVal)) $rtnStr = "class='media_active'";
	return $rtnStr;
}

function checkBxSeleted($ckBxVal, $selectedVal)
{
	$rtnStr = "";
	if (strtolower($ckBxVal) == strtolower($selectedVal)) $rtnStr = "checked";
	echo $rtnStr;
}

function makeSlugURL($str)
{
    $str = strip_tags($str);
	$str = str_replace(array(' ', '/','_', '--', '"', '"', ',', '.', '&', '|', '?', '+'), array('-', '-', '-', '-', '-', '-', '-', '-', 'and', 'or', '', ''), strtolower(trim($str)));
	return $str;
}

 function limitPageContent($text, $limit, $isAddBrTag = 'Y')
 {
	$fullTxt = trim(stripslashes($text));

	$isShowMoreBtn = 'N';

	$fullTxtLen = strlen($fullTxt);
	
	if ($fullTxtLen > $limit)
	{
		$slashedTxt = substr($fullTxt, 0, $limit);

		$lastCharOfSlashedTxt = substr(trim($slashedTxt), -1);	

		if ($lastCharOfSlashedTxt === '.') $slashedTxt = trim($slashedTxt) . '..';
		else  $slashedTxt .= '...';
		$isShowMoreBtn = 'Y';
	}
	else 
	{
		$slashedTxt = $fullTxt;
	}
	if ($isAddBrTag == 'Y') $slashedTxt = nl2br($slashedTxt);

   	return array('slashedTxt' => $slashedTxt, 'isShowMoreBtn' => $isShowMoreBtn, 'fullTxt' => nl2br($fullTxt));
 }

 function makeDropDownFromDB($dropDownName, $optionListArray, $optionValueDbFld, $optionTextDbFld, $selectedOptionValue, $mode = '', $style = '', $event = '', $hideShowPlzSelect = 'N', $isCustomMsg = '')
{
	$msg = "Please Select"; 
	if ($isCustomMsg) $msg = $isCustomMsg;
	$str  = "<select name = '$dropDownName' id = '$dropDownName' $style $event $mode>";
	//$str .= "<option value=''>Please Select</option>";
	if ($hideShowPlzSelect != 'Y') $str .= "<option value=''>$msg</option>";

	if (is_array($optionListArray))
	{
		$numOfRows = count($optionListArray);
		for ($i = 0; $i < $numOfRows; $i++) {

			if ($optionListArray[$i][$optionValueDbFld] == $selectedOptionValue)
			{
				$str .= "<option value='" . $optionListArray[$i][$optionValueDbFld] . "' selected>" . htmlspecialchars($optionListArray[$i][$optionTextDbFld]) . "</option>";
			} 
			else
			{
				$str .= "<option value='" . $optionListArray[$i][$optionValueDbFld] . "'>" . htmlspecialchars($optionListArray[$i][$optionTextDbFld]) . "</option>";
			}
		}
	}

	$str .= "</select>";
	echo $str;
}

 function makeMultiSelectDownFromDB($dropDownName, $optionListArray, $optionValueDbFld, $optionTextDbFld, $arrSelectedOptionValue = array(), $mode = '', $style = '', $event = '', $hideShowPlzSelect = 'N')
{
	$dropDownId = trim($dropDownName, '[]'); 
	$str  = "<select name = '$dropDownName' id = '$dropDownId' $style $event $mode  multiple title='Please Select'>";
	if ($hideShowPlzSelect != 'Y') $str .= "<option value=''>Please Select</option>";

	if (is_array($optionListArray))
	{
		$numOfRows = count($optionListArray);
		for ($i = 0; $i < $numOfRows; $i++) {

			if (in_array($optionListArray[$i][$optionValueDbFld], @$arrSelectedOptionValue))
			{
				$str .= "<option value='" . $optionListArray[$i][$optionValueDbFld] . "' selected>" . htmlspecialchars($optionListArray[$i][$optionTextDbFld]) . "</option>";
			} 
			else
			{
				$str .= "<option value='" . $optionListArray[$i][$optionValueDbFld] . "'>" . htmlspecialchars($optionListArray[$i][$optionTextDbFld]) . "</option>";
			}
		}
	}

	$str .= "</select>";
	echo $str;
}


function makeMultiSelectDown($dropDownName, $optionValueArray, $optionTextArray, $arrSelectedOptionValue = array(), $mode = '', $style = '', $event = '', $hideShowPlzSelect = 'N')
{
	$dropDownId = trim($dropDownName, '[]'); 
	$str  = "<select name = '$dropDownName' id = '$dropDownId' $style $event $mode  multiple title='Please Select'>";
	if ($hideShowPlzSelect != 'Y') $str .= "<option value=''>Please Select</option>";

	if(is_array($optionValueArray)) {
		$numOfRows = count($optionValueArray);

		for ($i = 0; $i < $numOfRows; $i++)
		{			 
			if (in_array($optionValueArray[$i], @$arrSelectedOptionValue))
			{
				$str .= "<option value='" . $optionValueArray[$i] . "' selected>" . htmlspecialchars($optionTextArray[$i]) . "</option>";
			} 
			else 
			{
				$str .= "<option value='" . $optionValueArray[$i] . "'>" . htmlspecialchars($optionTextArray[$i]) . "</option>";
			}
		}
	}

	$str .= "</select>";
	echo $str;
} 

function makeDropDown($dropDownName, $optionValueArray, $optionTextArray, $selectedOptionValue, $mode = '', $style = '', $event = '', $hideShowPlzSelect = 'N')
{
	$str  = "<select name = '$dropDownName' id = '$dropDownName' $style $event $mode>";
	if ($hideShowPlzSelect != 'Y') $str .= "<option value=''>Please Select</option>";

	if(is_array($optionValueArray)) {
		$numOfRows = count($optionValueArray);

		for ($i = 0; $i < $numOfRows; $i++)
		{
			if ($optionValueArray[$i] == $selectedOptionValue) 
			{
				$str .= "<option value='" . $optionValueArray[$i] . "' selected>" . htmlspecialchars($optionTextArray[$i]) . "</option>";
			} 
			else 
			{
				$str .= "<option value='" . $optionValueArray[$i] . "'>" . htmlspecialchars($optionTextArray[$i]) . "</option>";
			}
		}
	}

	$str .= "</select>";
	echo $str;
} 

function stripslashesHtmlChars($str)
{
	return  stripslashes(htmlspecialchars(trim($str)));
}

function checkPageAccessPermission($codeStr)
{
	if ($codeStr == '')
	{
		$_SESSION['messageSession'] = UNAUTHORIZED_MSG;	
		headerRedirect('logout.php?logout=2');
	}
}

 function cors() {

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

	header("Access-Control-Allow-Headers: X-Requested-With");
}

function showMsg($str, $msg = 'Not Available')
{
	if ($str == '') $str = $msg;
	return $str;
}

function getLandingPage($isBack = 'N')
{
	if ($_SESSION['userDetails']['accountType'] == 'U') $landingPage = "view-all-news-media.php";	
	else $landingPage = "view-all-news-media.php";
	if ($isBack == 'Y') $landingPage = "../{$landingPage}";
	return $landingPage;
}


function traceActivity($trace, $msgKey, $arrMsgKeyVal, $userCode = '')
{	
	$objDBQuery = $GLOBALS['objDBQuery'];
	$ARR_ACTIVITES = $GLOBALS['ARR_ACTIVITES'];	
	$strMsg = str_replace(array_keys($arrMsgKeyVal), array_values($arrMsgKeyVal), $ARR_ACTIVITES[$msgKey]);

	$objDBQuery->trackActivity($trace, randomMD5(), $strMsg, getRealIpAddr(), $userCode);
}

function removeStr($str, $replaceStr = '|')
{
	return trim(str_replace($replaceStr, '', $str));
}

function getPrice($value, $decimalPlace = 2)
{
	return sprintf("%01.{$decimalPlace}f", $value);
}

function makeArr2String($arr)
{
	$str = '';
	foreach($arr as $key => $val)
	{
		$str .= "$key: $val, ";
	}
	
	return trim($str, ', ');
}