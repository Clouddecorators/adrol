<?php
$PAGE_TITLE = "Welcome";
include_once("include/meta-header.php")
?>
  <section>    
    <div class="splash debug d-none d-md-none d-lg-block d-xl-block d-xxl-block">
      <video preload="metadata" autoplay loop muted poster="<?php echo HTTP_PATH_ASSET?>/images/poster.jpg">
        <source src="<?php echo HTTP_PATH_ASSET_UPLOAD.'/videos/engine-oil.mp4'?>" type="video/mp4">
      </video>
      <a href="<?php echo HTTP_PATH.'/home'?>"><img src="<?php echo HTTP_PATH_ASSET?>/images/giphy.gif" class="enter"/></a>
    </div> 
    <!--For tab-->
	     <div class="d-none d-md-block d-lg-none d-xl-none d-xxl-none"
        style="background: url(<?php echo HTTP_PATH_ASSET?>/images/tab-screen.jpg) no-repeat; background-size: cover; background-position: center;">
        <div class="container-fluid">
            <div class="row ht-100 justify-content-start">
                
            </div>
        </div>
       <a href="<?php echo HTTP_PATH.'/home'?>"><img src="<?php echo HTTP_PATH_ASSET?>/images/giphy.gif" class="enter"/></a>
    </div>
    
     <!--For mobile-->
	     <div class="d-block d-md-none d-lg-none d-xl-none d-xxl-none"
        style="background: url(<?php echo HTTP_PATH_ASSET?>/images/mobile-screen.jpg) no-repeat; background-size: cover; background-position: center;">
        <div class="container-fluid">
            <div class="row ht-100 justify-content-start">
                
            </div>
        </div>
       <a href="<?php echo HTTP_PATH.'/home'?>"><img src="<?php echo HTTP_PATH_ASSET?>/images/giphy.gif" class="enter"/></a>
    </div>
	     
  </section>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
  <script src="<?php echo HTTP_PATH_ASSET?>js/custom.js"></script>
</body>
</html>