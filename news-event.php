<?php
$PAGE_TITLE = "News Event";
include_once("include/meta-header.php");
include_once("include/header.php");

$whereCls = "status = 'A' AND isDeleted = 'N' AND postType = 'M'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_posts', $whereCls, '', '', 'createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{            

 ?>
 <div class="divider5"></div>
    <section class="journey-us-panel pb-5">
        <div class="container">
            <div class="text-center text-black py-5 mb-0 font-bebas-neue ft-4">News & Media</div>
            <div class="row g-4">
 <?php
	  		$numOfRows = count($rcdInfoArr);
			for ($i = 0; $i < $numOfRows; $i++)
			{
				$imgPath = HTTP_PATH_ASSET_UPLOAD.'/post_imgs/'.$rcdInfoArr[$i]['postThumb']; 
?>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6  hover-class">
                    <div class="card shadow-sm bg-radi">
                        <img class="bd-placeholder-img bg-radi card-img-top" width="100%" height="225"
                            src="<?php echo $imgPath?>">
                        <div class="card-body">
                            <p class="header font-bebas-neue fw-bold fs-5"><?php echo $rcdInfoArr[$i]['postTitle']?></p>
                            <p class="card-text"><?php echo limitPageContent($rcdInfoArr[$i]['postDesc'], 200, 'N')['slashedTxt']?></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a type="button" href="<?php echo $rcdInfoArr[$i]['postLink']?>" class="btn btn-sm btn-danger bg-radi px-4" target="_blank">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
			}
?>


            </div>
        </div>
    </section>
<?php
	}
?>


    <section class="photo-gallery pb-5">
        <!-- Topic Cards -->
        <div id="cards_landscape_wrap-2">
            <div class="container">
                <div class="text-center text-black py-5 mb-0 font-bebas-neue ft-4">Event Gallery</div> 
                <div class="row photos g-4">
   <?php
                $whereCls = "status = 'A' AND isDeleted = 'N' AND imgType = 'E'";
                $rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_imgs', $whereCls, '', '', 'imgOrder ASC, createdOn', 'DESC');
                if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
                {
                    $numOfRows = count($rcdInfoArr);
                    for ($i = 0; $i < $numOfRows; $i++)
                    {
                        $imgPath = HTTP_PATH_ASSET_UPLOAD.'/imgs/'.$rcdInfoArr[$i]['img'];                            

  ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="card-flyer">
                                <div class="text-box">
                                    <div class="image-box">
                                        <a href="<?php echo $imgPath?>" data-lightbox="photos">
                                            <img class="img-fluid" width="100%" height="100%" src="<?php echo $imgPath?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
<?php
                    }
                }
?> 

                </div>
            </div>
        </div>

    </section>

<?php
    include_once("include/footer.php");
?> 
  <!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
</script>