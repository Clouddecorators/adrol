<?php
$PAGE_TITLE = "Contact Us";
include_once("include/meta-header.php");
include_once("include/header.php");
?>

    <div class="divider6"></div>
    
    <section>
        <div class="photo-gallery photo-gallery2">
            <div class="container">
                <div class="intro">
                    <div class="text-center text-black py-5 mb-0 font-bebas-neue ft-4">Tutorial</div>
                </div>
                <div class="row photos">
<?php
                $whereCls = "status = 'A' AND isDeleted = 'N' AND imgType = 'V'";
                $rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_imgs', $whereCls, '', '', 'imgOrder ASC, createdOn', 'DESC');
                if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
                {
                    $numOfRows = count($rcdInfoArr);
                    for ($i = 0; $i < $numOfRows; $i++)
                    {
  ?>                    
                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="" data-lightbox="photos">
                                <iframe width="100%" height="200" src="<?php echo $rcdInfoArr[$i]['videoUrl']?>" title="<?php echo $rcdInfoArr[$i]['title']?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </a>
                        </div>
 <?php
                    }
                }  
 ?>                 
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="photo-gallery photo-gallery1">
            <div class="container">
                <div class="intro">
                    <div class="text-center text-black py-5 mb-0 font-bebas-neue ft-4">Testimonials</div>
                </div>
                <div class="row photos">
<?php
                $whereCls = "status = 'A' AND isDeleted = 'N' AND imgType = 'T'";
                $rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_imgs', $whereCls, '', '', 'imgOrder ASC, createdOn', 'DESC');
                if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
                {
                    $numOfRows = count($rcdInfoArr);
                    for ($i = 0; $i < $numOfRows; $i++)
                    {                                                  

  ?>                     
                    <div class="col-sm-6 col-md-4 col-lg-3 item">
                        <a href="" data-lightbox="photos">
                            <iframe width="100%" height="200" src="<?php echo $rcdInfoArr[$i]['videoUrl']?>" title="<?php echo $rcdInfoArr[$i]['title']?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </a>
                    </div>
                   
<?php
                    }
                }  
 ?>                                  
                </div>
            </div>
        </div>
    </section>

    <?php
    include_once("include/footer.php");
?> 
    <script>
        $(document).ready(function () {
            var pos = 0,
                slides = $('.slide'),
                numOfSlides = slides.length;

            function nextSlide() {
                // `[]` returns a vanilla DOM object from a jQuery object/collection
                slides[pos].video.stopVideo()
                slides.eq(pos).animate({ left: '-100%' }, 500);
                pos = (pos >= numOfSlides - 1 ? 0 : ++pos);
                slides.eq(pos).css({ left: '100%' }).animate({ left: 0 }, 500);
            }

            function previousSlide() {
                slides[pos].video.stopVideo()
                slides.eq(pos).animate({ left: '100%' }, 500);
                pos = (pos == 0 ? numOfSlides - 1 : --pos);
                slides.eq(pos).css({ left: '-100%' }).animate({ left: 0 }, 500);
            }

            $('.left').click(previousSlide);
            $('.right').click(nextSlide);
        })

        function onYouTubeIframeAPIReady() {
            $('.slide').each(function (index, slide) {
                // Get the `.video` element inside each `.slide`
                var iframe = $(slide).find('.video')[0]
                // Create a new YT.Player from the iFrame, and store it on the `.slide` DOM object
                slide.video = new YT.Player(iframe)
            })
        }
    </script>

    <!-- Initialize Swiper -->
    <!-- <script>
        var swiper = new Swiper(".mySwiper", {
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script> -->
    <!-- <script src='js/owl.carousel.min.js'></script> -->
