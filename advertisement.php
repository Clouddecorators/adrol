<?php
$PAGE_TITLE = "Advertisement";
include_once("include/meta-header.php");
include_once("include/header.php");
   
$whereCls = "videoUrl != '' AND status = 'A' AND isDeleted = 'N' AND imgType = 'Y'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_imgs', $whereCls, '', '', 'imgOrder ASC, createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{ 
?>   
    <section>
        <div class="video-slider">
            <!-- SLIDE 1 -->
    <?php
            $numOfRows = count($rcdInfoArr);
            for ($i = 0; $i < $numOfRows; $i++)
            {  
  ?>            
                <div class="slide">
                    <iframe class="video" src="<?php echo $rcdInfoArr[$i]['videoUrl']?>"></iframe>
                </div>
<?php
            }
?>                 
            <!-- END OF SLIDES -->
            <div class="slide-arrow left"></div>
            <div class="slide-arrow right"></div>
        </div>
    </section>
<?php    
}
$whereCls = "status = 'A' AND isDeleted = 'N' AND imgType = 'S'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_imgs', $whereCls, '', '', 'imgOrder ASC, createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{ 
?>   
    <section class="ads-bg">
        <div class="container">
            <div class="row justify-content-center align-items-center ht-100">
                <div class="col-md-12">
                    <div id="carouselExampleIndicators" class="carousel carousel-fade  position-relative"
                        data-bs-ride="carousel">
                        <div class="carousel-indicators">
   <?php
                            $numOfRows = count($rcdInfoArr);
                            for ($i = 0; $i < $numOfRows; $i++)
                            {
                                $imgPath = HTTP_PATH_ASSET_UPLOAD.'/imgs/'.$rcdInfoArr[$i]['img']; 

                                $clsActive = '';
                                if ($i == 0) $clsActive = 'active';                          

  ?>                            
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="<?php echo $i?>"
                                    class="<?php echo $clsActive?>" aria-current="true" aria-label="Slide <?php echo $i+1?>"></button>
<?php                                    
                            }
?>                                
                        </div>
                        <div class="carousel-inner">
<?php
                        $numOfRows = count($rcdInfoArr);
                        for ($i = 0; $i < $numOfRows; $i++)
                        {
                            $imgPath = HTTP_PATH_ASSET_UPLOAD.'/imgs/'.$rcdInfoArr[$i]['img']; 

                            $clsActive = '';
                            if ($i == 0) $clsActive = 'active'; 
?>                       
                                                           
                            <div class="carousel-item <?php echo $clsActive?> text-center">
                                <img src="<?php echo $imgPath?>" class="img-fluid" alt="<?php echo $rcdInfoArr[$i]['title']?>">
                            </div>  
<?php
                        }
?>                                                     
                        </div>

                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <?php
}
   ?>
    <section>
        <div class="photo-gallery">
            <div class="container">
                <div class="intro">
                    <div class="text-center py-5 mb-0 font-bebas-neue ft-4">Digital Gallery</div>
                </div>
                <div class="row photos">
   <?php
                $whereCls = "status = 'A' AND isDeleted = 'N' AND imgType = 'D'";
                $rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_imgs', $whereCls, '', '', 'imgOrder ASC, createdOn', 'DESC');
                if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
                {
                    $numOfRows = count($rcdInfoArr);
                    for ($i = 0; $i < $numOfRows; $i++)
                    {
                        $imgPath = HTTP_PATH_ASSET_UPLOAD.'/imgs/'.$rcdInfoArr[$i]['img'];                            

  ?>                     
                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="<?php echo $imgPath?>" data-lightbox="photos">
                                <img class="<?php echo HTTP_PATH_ASSET?> img-fluid" src="<?php echo $imgPath?>">
                            </a>
                        </div>
<?php
                    }
                }
?>                        
                </div>
            </div>
        </div>
    </section>
<?php
    include_once("include/footer.php");
?> 
      <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://www.youtube.com/iframe_api"></script>

    <script>
        $(document).ready(function () {
            var pos = 0,
                slides = $('.slide'),
                numOfSlides = slides.length;

            function nextSlide() {
                // `[]` returns a vanilla DOM object from a jQuery object/collection
                slides[pos].video.stopVideo()
                slides.eq(pos).animate({ left: '-100%' }, 500);
                pos = (pos >= numOfSlides - 1 ? 0 : ++pos);
                slides.eq(pos).css({ left: '100%' }).animate({ left: 0 }, 500);
            }

            function previousSlide() {
                slides[pos].video.stopVideo()
                slides.eq(pos).animate({ left: '100%' }, 500);
                pos = (pos == 0 ? numOfSlides - 1 : --pos);
                slides.eq(pos).css({ left: '-100%' }).animate({ left: 0 }, 500);
            }

            $('.left').click(previousSlide);
            $('.right').click(nextSlide);
        })

        function onYouTubeIframeAPIReady() {
            $('.slide').each(function (index, slide) {
                // Get the `.video` element inside each `.slide`
                var iframe = $(slide).find('.video')[0]
                // Create a new YT.Player from the iFrame, and store it on the `.slide` DOM object
                slide.video = new YT.Player(iframe)
            })
        }
    </script>
