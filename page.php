<?php
$slug = $_GET['slug'];
$PAGE_TITLE = ucwords(str_replace('-', ' ', $slug));
include_once("include/meta-header.php");
include_once("include/header.php");
$whereCls = "status = 'A' AND isDeleted = 'N' AND pageAbbr = '$slug'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_pages', $whereCls, '', '', 'pageOrder ASC, createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
	$imgPath = HTTP_PATH_ASSET_UPLOAD.'/page_images/'.$rcdInfoArr[0]['pageImg']; 
	if ($slug != 'adrol-journey')
	{
?>

<div class="divider5"></div>
	<section class="about-us-panel position-relative d-block">
	    <a href="<?php echo HTTP_PATH?>/home" class=" float-start ms-2 mt-5" aria-label="Close"><i class="fa fa-times fs-4 text-muted" alt="Close"></i></a> 
		<div class="container">
			<div class="row justify-content-center align-items-center hts-100 py-5">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6">
					<div class="pipe-set-about"><?php echo $rcdInfoArr[0]['title']?></div>

					<div class="ft-20 mt-4 force-overflow scrollbar" id="style-3">
						<p><?php echo $rcdInfoArr[0]['description']?></p>
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 text-center">
					<img src="<?php echo $imgPath?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
<?php 
	}
	else
	{
?>
<div class="divider5"></div>
	<section class="journey-us-panel position-relative d-block">
	    <a href="<?php echo HTTP_PATH?>/home" class=" float-start ms-2 mt-5" aria-label="Close"><i class="fa fa-times fs-4 text-muted" alt="Close"></i></a> 
	
		
			<div class="container">
			<div class="row justify-content-center align-items-center py-5">
			<div class="col-sm-12">
			    <div class="pipe-set-about">ADROL JOURNEY</div>
			</div>    	
		<div class="timeline">
  <div class="containers left">
    <div class="content">
      <h2 class="font-bebas-neue">1968</h2>
      <p><strong>Late Shri Roshan Lal Goel</strong> founder of
										<strong>R.G.Group</strong> of Industries started marketing's of high quality
										Transmissions Rubber Belts in the name and style of &nbsp;"<strong>STEEL
											GRIP</strong>"
									</p>
    </div>
  </div>
  <div class="containers right">
    <div class="content">
      <h2 class="font-bebas-neue">1990</h2>
      <p>Current Executive Director <strong> Rajan Goel </strong> joined
									the company
									and started making laurels under his father as mentor in the same year diversified
									into Conveyor Belts.</p>
    </div>
  </div>
  <div class="containers left">
    <div class="content">
      <h2 class="font-bebas-neue">2000</h2>
      <p>After analyzing the business trends entered into lubricants
									industry having strong vision they laid down the foundation stone of Gargo
									International and Build up strong marketing team and started exploring the network
									for the company in Indian markets.</p>
    </div>
  </div>
  <div class="containers right">
    <div class="content">
      <h2 class="font-bebas-neue">2002</h2>
      <p>The state of art blending, processing and packaging plant was
									set up at <strong>Mundka, Delhi</strong> with<strong> Centrifugal filtration digital
										M.R.P. printings and batching machine</strong>. Plant has production capacity of
									<strong>Ten million liters per annum.</strong>
								</p>
    </div>
  </div>
  <div class="containers left">
    <div class="content">
      <h2 class="font-bebas-neue">2008</h2>
      <p>An in house lab of International standards for Research and
									Developments was started .Every Batch is Tested thoroughly. So to make International
									Standards Products and Uniform Quality. Company got ISO9001:2008 Certification.</p>
    </div>
  </div>
  <div class="containers right">
    <div class="content">
      <h2 class="font-bebas-neue">2009</h2>
      <p>Technical Partner <strong>Pavand Paradeh Gostar
										Co. LTD.</strong> of &nbsp;<strong>IRAN</strong> Joined Hands to provide
									most modern Technology Supports. Hence "Adrol" Products became equivalents to
									International Standards.</p>
    </div>
  </div>
  
  <div class="containers left">
    <div class="content">
      <h2 class="font-bebas-neue">2013</h2>
     <p>ADROL Leaped high, strategically signed <strong>SUNNY
										DEOL</strong>, The Famous Bollywood Actor as brand Ambassador. This helped us to
									gain recognition in market, we marketed our products with the help of <strong>SUNNY
										DEOL</strong>&nbsp; and also introduced a famous slogan <strong>CHAK DE
										GADDI,</strong> which is exactly presenting our brand and brand ambassador's
									personality and in turn it helped us to gain good market share. We appreciate
									support of <strong>SUNNY DEOL</strong> sir's in this entire activity.</p>
    </div>
  </div>
  
  <div class="containers right">
    <div class="content">
      <h2 class="font-bebas-neue">2014</h2>
      <p>We explored new territories , got associated with new channel
									partners, who were existing and non existing business people belonging to lube and
									non lube industry. Through our uninterrupted support & skills we had made success
									stories of our associates who have done tremendous business from scratch.</p>
    </div>
  </div>
  
   <div class="containers left">
    <div class="content">
      <h2 class="font-bebas-neue">2016</h2>
      <p>ADROL engaged first grade IT company to develop an APP which
									helped our channel partners to do day to day business activities. This App helps
									them Analyze business .This App also help to give solution of problems in real time
									to our distributor, dealer , shopkeeper, Mechanics & even end user. App is very user
									friendly which has created for ease of doing business. It has been one of the most
									successful & innovative concepts introduced by the company.
								</p>
    </div>
  </div>
  
  <div class="containers right">
    <div class="content">
      <h2 class="font-bebas-neue">2019</h2>
     <p>ADROL Targeted to do aggressive marketing. Signed <strong>SONU
										SOOD</strong>
									a well reputed Bollywood&nbsp; Star having huge fan following. Now we are
									not only active on print, outdoor social media but also on television
									channels &amp; FM radio. These all activities benefited our channel
									partners as brand became more known to end-user which in turn increased
									the business volumes of our associates.

									*We do grass-root marketing &amp; survey to understand (ever changing
									dynamic ) requirements of market and accordingly our R &amp; D team
									develops products which help to serve our clients at par.</p>
    </div>
  </div>
</div>
</div>
</div>

		<!--<div class="container">-->
		<!--	<div class="row justify-content-center align-items-center hts-100 py-5">-->
		<!--		<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 position-relative" id="demo">-->

		<!--			<div class="customNavigation w-100">-->
		<!--				<a class="btn prev"><img src="<?=HTTP_PATH_ASSET?>/images/arrow-left.svg" width="30"></a>-->
		<!--				<a class="btn next"><img src="<?=HTTP_PATH_ASSET?>/images/arrow-right.svg" width="30"> </a>-->
		<!--			</div>-->
		<!--			<div id="owl-demo" class="owl-carousel">-->
		<!--				<div class="item">-->
		<!--					<div class="card border-0 bg-transparent p-4">-->
		<!--						<h3 class="text-start">1968</h3>-->
		<!--						<div class="card-body card-body-set p-0">-->
		<!--							<p class="text-justify"><strong>Late Shri Roshan Lal Goel</strong> founder of-->
		<!--								<strong>R.G.Group</strong> of Industries started marketing's of high quality-->
		<!--								Transmissions Rubber Belts in the name and style of &nbsp;"<strong>STEEL-->
		<!--									GRIP</strong>"-->
		<!--							</p>-->
		<!--						</div>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">1990</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">Current Executive Director <strong> Rajan Goel </strong> joined-->
		<!--							the company-->
		<!--							and started making laurels under his father as mentor in the same year diversified-->
		<!--							into Conveyor Belts.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2000</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">After analyzing the business trends entered into lubricants-->
		<!--							industry having strong vision they laid down the foundation stone of Gargo-->
		<!--							International and Build up strong marketing team and started exploring the network-->
		<!--							for the company in Indian markets.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2002</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">The state of art blending, processing and packaging plant was-->
		<!--							set up at <strong>Mundka, Delhi</strong> with<strong> Centrifugal filtration digital-->
		<!--								M.R.P. printings and batching machine</strong>. Plant has production capacity of-->
		<!--							<strong>Ten million liters per annum.</strong>-->
		<!--						</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2008</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">An in house lab of International standards for Research and-->
		<!--							Developments was started .Every Batch is Tested thoroughly. So to make International-->
		<!--							Standards Products and Uniform Quality. Company got ISO9001:2008 Certification.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2009</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">Technical Partner <strong>Pavand Paradeh Gostar-->
		<!--								Co. LTD.</strong> of &nbsp;<strong>IRAN</strong> Joined Hands to provide-->
		<!--							most modern Technology Supports. Hence "Adrol" Products became equivalents to-->
		<!--							International Standards.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2013</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">ADROL Leaped high, strategically signed <strong>SUNNY-->
		<!--								DEOL</strong>, The Famous Bollywood Actor as brand Ambassador. This helped us to-->
		<!--							gain recognition in market, we marketed our products with the help of <strong>SUNNY-->
		<!--								DEOL</strong>&nbsp; and also introduced a famous slogan <strong>CHAK DE-->
		<!--								GADDI,</strong> which is exactly presenting our brand and brand ambassador's-->
		<!--							personality and in turn it helped us to gain good market share. We appreciate-->
		<!--							support of <strong>SUNNY DEOL</strong> sir's in this entire activity.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2014</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">We explored new territories , got associated with new channel-->
		<!--							partners, who were existing and non existing business people belonging to lube and-->
		<!--							non lube industry. Through our uninterrupted support & skills we had made success-->
		<!--							stories of our associates who have done tremendous business from scratch.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2016</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">ADROL engaged first grade IT company to develop an APP which-->
		<!--							helped our channel partners to do day to day business activities. This App helps-->
		<!--							them Analyze business .This App also help to give solution of problems in real time-->
		<!--							to our distributor, dealer , shopkeeper, Mechanics & even end user. App is very user-->
		<!--							friendly which has created for ease of doing business. It has been one of the most-->
		<!--							successful & innovative concepts introduced by the company.-->
		<!--						</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--				<div class="card border-0 bg-transparent p-4">-->
		<!--					<h3 class="text-start">2019</h3>-->
		<!--					<div class="card-body card-body-set p-0">-->
		<!--						<p class="text-justify">ADROL Targeted to do aggressive marketing. Signed <strong>SONU-->
		<!--								SOOD</strong>-->
		<!--							a well reputed Bollywood&nbsp; Star having huge fan following. Now we are-->
		<!--							not only active on print, outdoor social media but also on television-->
		<!--							channels &amp; FM radio. These all activities benefited our channel-->
		<!--							partners as brand became more known to end-user which in turn increased-->
		<!--							the business volumes of our associates.-->

		<!--							*We do grass-root marketing &amp; survey to understand (ever changing-->
		<!--							dynamic ) requirements of market and accordingly our R &amp; D team-->
		<!--							develops products which help to serve our clients at par.</p>-->
		<!--					</div>-->
		<!--				</div>-->

		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</div>-->
	</section>

<?php
	}
}
include_once("include/footer.php");
?> 