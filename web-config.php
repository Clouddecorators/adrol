<?php
define('USER_PANEL_TITLE', 'Adrol Lubricants');
define('ADMIN_PANEL_TITLE', 'Adrol CMS Panel | ');
define('DASHBOARD_WELCOME_TXT', 'Welcome to Adrol Admin Panel!'); 
// no-reply@adrollubricants.com : no-reply@123
define('NO_REPLY_EMAIL', 'no-reply@adrollubricants.com');
define('INFO_EMAIL', 'vijay.chaudhary.19940201@gmail.com,info@adrollubricants.com');
define('FROM_NAME', USER_PANEL_TITLE);
define('SIGNATURE', 'Adrol Team');
define('DB_DATA_SPLITTER', ',');
define('TOKEN_SALT', 'E0xyJXmXvAPmeScqWrVLXX2L1ukycGqrtlQUXNZmuyC9kNCaVMpx8gDKhe5wP3');

// HERE DEFINE CONTS FOR DATE
define('DATE_FORMAT_SPLITTER', '-');
define('SHORT_DATE_FORMAT', 'm-d-Y');
define('LONG_DATE_FORMAT', 'm-d-Y H:i:s');
define('SHORT_MYSQL_DATE_FORMAT', 'Y-m-d');
define('MYSQL_DATE_CONVERSION_STYLE', 'EU');
define('LONG_MYSQL_DATE_FORMAT', 'Y-m-d H:i:s');
define('SET_PROJECT_TIMEZONE', 'Asia/Kolkata');
date_default_timezone_set(SET_PROJECT_TIMEZONE);

// HERE DEFINE CONTS FOR MISC
define('PHONE_VALIADTION_REGEX', '^[0-9]*$');
define('PHONE_VALIADTION_MSG', 'Please enter only numbers');
define('MIN_VALIADTION_REGEX', '^[0-9]*$');
define('MIN_VALIADTION_MSG', 'Please enter only numbers');
define('PER_PAGE', 10);
define('RECORD_NOT_FOUND_TXT', 'No Record Found');

define('SESSION_EXPIRED_MSG', 'Adrol web page session has been expired.');

// HERE DEFINE COMMON MSG
define('USER_KEY_MSG', 'Sorry, user key does not match.');
define('UNAUTHORIZED_MSG', 'Unauthorized access denied.');
define('USER_KEY_NOT_RECEIVE_MSG', 'Sorry, user key does not receive.');
define('APP_ID_NOT_EXIST_MSG', 'Sorry, App Id does not exist.');
define('USER_EMAIL_MSG', 'Please enter valid email address.');
define('USER_EMAIL_EXIST_MSG', 'Sorry, email address already exists, please try another.');
define('USER_NAME_EXIST_MSG', 'Sorry, username already exists, please try another.');
define('USER_EMAIL_NOT_FOUND_DB_MSG', 'Sorry, given email address is not present in our record.');
define('USER_PIN_CONFIRM_NOT_MATCH', 'Sorry, confirm password does not match.');
define('USER_PIN_OLD_NOT_FOUND_DB_MSG', 'Sorry, given current password does not match with our record.');
define('USER_EMAIL_PIN_NOT_FOUND_DB_MSG', 'Sorry, invalid email address or password.');
define('USER_EMAIL_USERNAME_MSG', 'Sorry, given email address or username is not present in our record.');

$cpYearInfo = '2021';
if (date('Y') != $cpYearInfo) $cpYearInfo = $cpYearInfo.'-'.date('Y');

$STATUS = array("A" => "Active", "I" => "Inactive");
$ARR_ACCOUNT_TYPES = array('U' => 'User', "A" => "Admin", "S" => "Super Admin");
$ARR_IMG_TYPES = array('S' => 'Slider', "D" => "Digital Gallery", "E" => "Event Gallery", "Y" => 'Youtube Link', "V" => 'Tutorial', "T" => 'Video Testimonial');

$DEFAULT_DROPDOWN_SELECTION = array("N" => "No", "Y" => "Yes");
$ARR_PRODUCT_QTY = array('100ML', '175ML', '250ML', '350ML', '500ML', '800ML', '900ML', '1LTR', '1200ML', '1500ML', '2LTR', '2.5LTR', '3LTR', '3.25ML', '3.5LTR', '4LTR', '5LTR', '6LTR', '7.5LTR', '10LTR', '15LTR', '18LTR', '20LTR', '26LTR', '50LTR', '180LTR',	'205LTR' , '500GM' , '1KG' , ' 2KG ', '5KG' , '7KG' , '10KG', '18KG' , '180KG');

$ARR_PRODUCT_SKU = array('0W40', ' API SN/CK4',' API CK4', '15W40','20W50','JASO MA2','SAE 0W-40','SAE 10W-30','SAE 10W-40','SAE 15W-40','SAE 15W-50','SAE 20W-40','SAE 20W-50','SAE 20W-50','SAE 5W-40','150/220/320/460','15W40 API CF','15W40CH4','20W40 API SC-CC','220/320/460','80W90/85W140 API GL5','API CD 20W40','API CH4 20W 40','API CH-4/SN','API CH4+','API CI4+ 15W 40','API CK-40W40' ,'API SG/CD','API SL','API SM','API SM-CF',' API SN','API SN/ JASO MB/ SAE 10W-30','API SN/CF','API SN-CH','GL4-EP90/EP140','GL-5 75W90','GL5 EP90/EP140','HLP 32/46/68','JD21/GL-4/C4','LITHIUM BASED GREASE','LR-AP 45000', 'AW-32/46/68', 'SOLUBLE/ NEAT/ SEMI SYNTHETIC', 'HIGH TEMPREATURE GREASE');

$ARR_OIL_FOR = array('G' => 'Glide', "ECG" => "Engine, Clutch & Gear", "GC" => "Gear & Clutch", "E" => "Engine", "C" => 'Clutch',"GE" => 'Gear');

$ARR_PRODUCT_TYPE = array('P' => 'Primary (Show in Top)', "S" => "Secondary (Show in Bottom)");


$ARR_ACTIVITES = array (
				'login' => '[FORM_EMAIL] logged into the portal.',
				'logout' => '[SESSION_EMAIL] logged out from the portal.',
				'updateYourProfile' => '[SESSION_EMAIL] updated his/her profile details.',
				'changePassword' => '[SESSION_EMAIL] updated his/her password.',
				'forgotPassword' => '[FORM_EMAIL] tried to retrieve his/her password through forgot password web page.',
			);

define('COPY_RIGHT_INFO', "&copy;".USER_PANEL_TITLE. " $cpYearInfo. All Rights Reserved.");

if ($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '10.1.1.5')
{
	define('HTTP_PATH', 'http://localhost/adrol');
	define('HTTP_PATH_ASSET', HTTP_PATH.'/public_assets');
	define('HTTP_PATH_ASSET_UPLOAD', HTTP_PATH.'/public_assets/uploads');
	define('HTTP_PATH_ADMIN', HTTP_PATH.'/webcmspanel');
	define('HARD_PATH', $_SERVER['DOCUMENT_ROOT'] . '/adrol');	
	define('HARD_PATH_UPLOAD', HARD_PATH . '/public_assets/uploads');	
} 
else
{	
	define('HTTP_PATH', 'https://adrollubricants.com');	
	define('HTTP_PATH_ASSET', HTTP_PATH.'/public_assets');
	define('HTTP_PATH_ASSET_UPLOAD', HTTP_PATH.'/public_assets/uploads');
	define('HTTP_PATH_ADMIN', HTTP_PATH.'/webcmspanel');
	define('HARD_PATH', $_SERVER['DOCUMENT_ROOT'].'');	
	define('HARD_PATH_UPLOAD', HARD_PATH . '/public_assets/uploads');
	error_reporting(0);
}

##############################DO NOT TOUCH BELOW THIS LINE##############################
ob_start();
@session_start();
?>
