<?php
$PAGE_TITLE = "Blogs";
include_once("include/meta-header.php");
include_once("include/header.php");
$whereCls = "status = 'A' AND isDeleted = 'N' AND postType = 'B'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_posts', $whereCls, '', '', 'createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
 ?>
 <div class="divider5"></div>
<section class="journey-us-panel pb-5">
	<div class="container">
		<div class="text-center text-black py-5 mb-0 font-bebas-neue ft-4">Blog Segment</div>
		<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4">
 <?php
	  		$numOfRows = count($rcdInfoArr);
			for ($i = 0; $i < $numOfRows; $i++)
			{
				$imgPath = HTTP_PATH_ASSET_UPLOAD.'/post_imgs/'.$rcdInfoArr[$i]['postThumb']; 
?>		   
				<div class="col hover-class">
					<div class="card shadow-sm bg-radi">
						<img class="bd-placeholder-img bg-radi card-img-top" width="100%" height="225" src="<?php echo $imgPath?>">
						<div class="card-body">
							<p class="header font-bebas-neue fw-bold fs-5 mb-0"><?php echo $rcdInfoArr[$i]['postTitle']?></p>
							<small class="text-muted"><i class="fa fa-clock-o"></i> <?php echo getTimeStamp($rcdInfoArr[$i]['createdOn'], 'M d, D')?></small>
							<small class="text-muted float-end"><i class="fa fa-user-o"></i> <?php echo $rcdInfoArr[$i]['autherName']?></small>
							<p class="card-text mt-3"><?php echo limitPageContent($rcdInfoArr[$i]['postDesc'], 100, 'N')['slashedTxt']?></p>
							<div class="d-flex justify-content-between align-items-center">
								<div class="btn-group">
									<a type="button" href="blog/<?php echo $rcdInfoArr[$i]['slug']?>" class="btn btn-sm btn-danger bg-radi px-4">View More</a>
								</div>

							</div>
						</div>
					</div>
				</div>
<?php
			}
?>			

		</div>
	</div>
</section>
<?php
	}
?>
<?php
    include_once("include/footer.php");
?> 