<?php
$PAGE_TITLE = "Thank You";
include_once("include/meta-header.php");
include_once("include/header.php");

$_POST = trimFormValue(0, $_POST);		
$formToken = @$_POST['formToken'];	

if ($formToken != '' && (@$_POST['fname'] || @$_POST['cName']) && ($_POST['email'] || $_POST['cEmail']))
{
	$msg = 'thankyou';
	$_SESSION['FORM_TOKEN'] = '';

	if ($_POST['frmType'] == 'connect')
	{
		$body = "Dear Admin,<br><br><b>User Enquiry Details:</b><br>";
		$body .= "<br><b>Name:</b> ".$_POST['cName'];
		$body .= "<br><b>State:</b> ".$_POST['cState'];
		$body .= "<br><b>City/District:</b> ".$_POST['cCity'];
		$body .= "<br><b>Mobile No.:</b> ".$_POST['cPhone'];
		$body .= "<br><b>Alternet Mobile No.:</b> ".$_POST['cAltPhone'];
		$body .= "<br><b>Address:</b> ".$_POST['cAddress'];
		$body .= "<br><b>Email:</b> ".$_POST['cEmail'];	
		$body .= "<br><b>Enquiry Type:</b> ".$_POST['cEnquiryType'];		
		$body .= "<br><b>Remarks:</b> ".nl2br($_POST['cRemarks']);
		$etype = "Enquiry for Connect";
		$arrExcludedFrmKeys = array();
		$arrAllowFrmKeys = array('cCity', 'cstate', 'cPhone', 'cAltPhone', 'cEmail', 'cAddress', 'cEnquiryType', 'cRemarks');		
		$dataArr = prepareKeyValueForDBQuery(0, $_POST, $arrAllowFrmKeys, $arrExcludedFrmKeys);
				
		$objDBQuery->addRecord(0, $dataArr, 'tbl_connects');
	}
	else
	{
		$body = "Dear Admin,<br><br><b>User Enquiry Details:</b><br>";
		$body .= "<br><b>First Name:</b> ".$_POST['fname'];
		$body .= "<br><b>Last Name:</b> ".$_POST['lname'];
		$body .= "<br><b>Email:</b> ".$_POST['email'];
		$body .= "<br><b>Phone:</b> ".$_POST['phone'];
		$body .= "<br><b>Message:</b> ".nl2br($_POST['message']);
		$etype = "Enquiry";
	}

	sendEmail(INFO_EMAIL, USER_PANEL_TITLE.' - '.$etype, $body, FROM_NAME, NO_REPLY_EMAIL, 'HTML');
}
else
{
	$msg = '401';
}

$arrHttpsErrCode = array('thankyou' => '<img src="https://adrollubricants.com/public_assets/images/tick.gif" class="img-fluid"> <br> Thank you for getting in touch! <br> We appreciate you contacting us about inquiry.', '401' => 'Unauthorized access'); 

?> 
<div class="devider6"></div> 
<section>
	<div class="container">
		<div class="row align-items-center justify-content-center pt-5 hts-100">
			<div class="error-set text-center">		
			<div class="text-center fs-2"><?php echo $arrHttpsErrCode[$msg]?></div>
			
			</div>
		</div>
	</div>
</section>
