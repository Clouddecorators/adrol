<?php
$PAGE_TITLE = "Connects";
include_once("include/meta-header.php");
include_once("include/header.php");

$err = $_GET['err'];
$arrHttpsErrCode = array('400' => 'Bad Request', '401' => 'Unauthorized', '403' => 'Forbidden', '404' => 'Page Not Found', '405' => 'Method Not Allowed', '500' => 'Internal Server Error'); 
?>
<div class="devider6"></div>
<section style="background: url(<?php echo HTTP_PATH_ASSET?>/images/oops.gif) no-repeat; background-size: cover; background-position:center;height: 100vh; ">
	<div class="container">
		<div class="row align-items-center justify-content-center pt-5 hts-100">
			<div class="error-set">
			<div class="text-white text-center" style="font-size: 10rem;
    font-weight: 600; line-height: 1em;"><?php echo $err?></div>
			<div class="text-white text-center fs-2"><?php echo $arrHttpsErrCode[$err]?></div>
			<div class="text-white text-center">
				Oops.... The link you clicked may be broken or the page may have been removed. We're Sorry.
			</div>
			</div>
		</div>
	</div>
</section>
<?php
    include_once("include/footer.php");
?> 