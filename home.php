<?php
$PAGE_TITLE = "Home";
include_once("include/meta-header.php");
include_once("include/header.php");

$whereCls = "status = 'A' AND isDeleted = 'N'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_pages', $whereCls, '', '', 'pageOrder ASC, createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
	$numOfRows = count($rcdInfoArr);
	for ($i = 0; $i < $numOfRows; $i++)
	{
		if ($i % 2 == 0)
		{ 
			$titleCls = "title-set ms-4";
			$titleCls2 = "font-set ms-4";
			$subTitleCls = "pipe5";
			$divCls2 = "col-sm-12 position-relative";
			$divCls = "row ht-100 justify-content-start align-items-center";
			$anchorCls = "fa fa-angle-right  animate__animated animate__fadeIn animate__infinite";
		}
		else
		{
			$titleCls = "title-set me-4";
			$titleCls2 = "font-set me-4";
			$subTitleCls = "pipe3";
			$divCls2 = "col-sm-12 text-end";
			$divCls = "row ht-100 justify-content-end align-items-center";
			$anchorCls = "fa fa-angle-right  animate__animated animate__fadeIn animate__infinit";
		}
		$imgPath = HTTP_PATH_ASSET_UPLOAD.'/page_images/'.$rcdInfoArr[$i]['bannerImg']; 
		$prdMobViewImg = $rcdInfoArr[$i]['bannerMobViewImg'];
		$prdTabViewImg = $rcdInfoArr[$i]['bannerTabViewImg'];
		if ($prdMobViewImg != '')
		{
			$moviewImgPath = HTTP_PATH_ASSET_UPLOAD.'/page_images/'.$prdMobViewImg; 
		}
		else 
		{
			$moviewImgPath = $imgPath; 
		}

		if ($prdTabViewImg != '')
		{
			$tabiewImgPath = HTTP_PATH_ASSET_UPLOAD.'/page_images/'.$prdTabViewImg; 
		}
		else 
		{
			$tabiewImgPath = $imgPath; 
		}
		list($title1, $title2) = explode(' ', $rcdInfoArr[$i]['title'], 2);
 
  ?> 	             
     <section class="position-relative">
         <div class="d-none d-md-none d-lg-block d-xl-block d-xxl-block" style="background: url(<?php echo $imgPath?>) no-repeat; background-size: cover; background-position: bottom, left;">
	        <a class="text-decoration-none" href="<?php echo HTTP_PATH?>/page/<?php echo $rcdInfoArr[$i]['pageAbbr']?>">
        
		<div class="container-fluid">
            <div class="<?php echo $divCls?>">
                <div class="<?php echo $divCls2?>">
                    <p class="<?php echo $titleCls?>"><?php echo $title1?></p>
                    <p class="<?php echo $titleCls2?>"><?php echo $title2?></p>

                    <div class="<?php echo $subTitleCls?>"><?php echo $rcdInfoArr[$i]['pageTxt']?></div>
                </div>
            </div>
        </div>
        </a>
        <a class="enter-button" href="<?php echo HTTP_PATH?>/page/<?php echo $rcdInfoArr[$i]['pageAbbr']?>">
            View Details &nbsp; <i class="<?php echo $anchorCls?>"></i>
        </a>
        </div>
        
        <!--For Tab-->
        <div class="d-none d-md-block d-lg-none d-xl-none d-xxl-none" style="background: url(<?php echo $tabiewImgPath?>) no-repeat; background-size: cover; background-position: bottom, left;">
	        <a class="text-decoration-none" href="<?php echo HTTP_PATH?>/page/<?php echo $rcdInfoArr[$i]['pageAbbr']?>">
        
		<div class="container-fluid">
            <div class="<?php echo $divCls?>">
                <div class="<?php echo $divCls2?>">
                    <p class="<?php echo $titleCls?>"><?php echo $title1?></p>
                    <p class="<?php echo $titleCls2?>"><?php echo $title2?></p>

                    <div class="<?php echo $subTitleCls?>"><?php echo $rcdInfoArr[$i]['pageTxt']?></div>
                </div>
            </div>
        </div>
        </a>
        <a class="enter-button" href="<?php echo HTTP_PATH?>/page/<?php echo $rcdInfoArr[$i]['pageAbbr']?>">
            View Details &nbsp; <i class="<?php echo $anchorCls?>"></i>
        </a>
        </div>        
           
	     <!--For mobile-->
	     <div class="d-block d-md-none d-lg-none d-xl-none d-xxl-none"
        style="background: url(<?php echo $moviewImgPath?>) no-repeat; background-size: cover; background-position: center;">
        <div class="container-fluid">
            <div class="row ht-100 justify-content-start">
                <div class="col-sm-12" style="position: absolute; bottom: 188px;">
<?php
				$arrTitle = explode(' ', $rcdInfoArr[$i]['title']);
				$sCls = 'title-set ms-3';				
				foreach ($arrTitle as $sTitle)
				{					
?>
                    <p class="<?php echo $sCls?>"><?php echo $sTitle?></p>                    
<?php
				    $sCls = 'font-set ms-3'; 
				}
?>
                    <div class="pipe5"><?php echo $rcdInfoArr[$i]['pageTxt']?></div>                    
                </div>
            </div>
        </div>
        <a class="enter-button" href="<?php echo HTTP_PATH?>/page/<?php echo $rcdInfoArr[$i]['pageAbbr']?>">
            View Details &nbsp; <i class="fa fa-angle-right  animate__animated animate__fadeIn animate__infinite"></i>
        </a>
    </div>
    </section>
    
   


    
<?php
	}
}

?>

 <section>
    <div class="container py-5">
    <div class="row">
    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4  mb-4">
    <div class="card border-0 shadow">
      <img src="<?php echo HTTP_PATH_ASSET?>/images/vision.jpg" alt="" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">OUR VISION</h5>
        <p class="card-text">Our Vision is to become a leading contributor in INDIAN economy, emerging as leading player in Lubricant Industry. We strive to become a consistent and a major shareholder in lubricants industry in Automotive and Industrial lubes and Greases.</p>
      </div>
     </div>
    </div>
    
    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4  mb-4">
    <div class="card border-0 shadow">
      <img src="<?php echo HTTP_PATH_ASSET?>/images/mission.jpg" alt="" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">OUR MISSION</h5>
        <p class="card-text">Our Mission is to be a dynamic, profitable and consistent growth oriented company through  leading the market, maximizing excellence in quality and service to ensure attractive returns to channel partner, to reward employees according to their ability and performance.</p>
      </div>
     </div>
    </div>
    
    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4  mb-4">
    <div class="card border-0 shadow">
      <img src="<?php echo HTTP_PATH_ASSET?>/images/quality.jpg" alt="" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">QUALITY POLICY</h5>
        <p class="card-text">We believe in best quality management and strive to ensure quality standards. We aim at continuous improvement through skills and modernization of our products and systems to the entire satisfaction of our customers.</p>
      </div>
     </div>
    </div>
  
  </div>
</div>
    </section>
    
 <?php
    include_once("include/footer.php");
?>    