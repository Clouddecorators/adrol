<?php
include_once("inc.php");
$caseName = $_GET['caseName'];
$uid = $_GET['uid'];
$reFrm = @$_GET['reFrm'];
$ajaxRequestUrl = HTTP_PATH.'/ajax-call.php';
if ($reFrm == 'ad') $cls = "class='selectpicker' data-size='6' data-live-search='true";
else $cls = "class='form-select' data-live-search='true' aria-label='Default select example' required";

if ($caseName == 'getManufacturerList')
{
	$infoArr = $objDBQuery->getRecord(0, array('bid', 'vbrand_name'), 'tbl_vehicle_brand', array('vehicle_type_id' => $uid), '', '', 'vbrand_name', 'ASC');
	echo makeDropDownFromDB('bid_FK', $infoArr, 'bid', 'vbrand_name', '', $cls, "onchange=\"setDropDownValue(this.value, 'modelId', 'getModelList', '$ajaxRequestUrl');\"", '', 'Select Manufacturer');
}
else if ($caseName == 'getModelList')
{
	$infoArr = $objDBQuery->getRecord(0, array('mid', 'model_name', 'brand_id'), 'tbl_vehicle_model', array('brand_id' => $uid), '', '', 'model_name', 'ASC');
	echo makeDropDownFromDB('mid_FK', $infoArr, 'mid', 'model_name', '', $cls, '', '', '', 'Select Model');
}
else if ($caseName == 'getManufacturerListM')
{
	$infoArr = $objDBQuery->getRecord(0, array('bid', 'vbrand_name'), 'tbl_vehicle_brand', "vehicle_type_id IN($uid)", '', '', 'vbrand_name', 'ASC');
	echo makeMultiSelectDownFromDB('bid_FK[]', $infoArr, 'bid', 'vbrand_name', array(), "class='selectpicker' data-size='6' data-live-search='true'", '', "onchange=\"setDropDownValue(this.id, 'modelId', 'getModelListM', '$ajaxRequestUrl');\"", '');
}
else if ($caseName == 'getModelListM')
{
	$infoArr = $objDBQuery->getRecord(0, array('mid', 'model_name', 'brand_id'), 'tbl_vehicle_model', "brand_id IN($uid)", '', '', 'model_name', 'ASC');
	echo makeMultiSelectDownFromDB('mid_FK[]', $infoArr, 'mid', 'model_name', array(), "class='selectpicker' data-size='6' data-live-search='true'", '', "", '');
}

?>