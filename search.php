<?php
$slug = 'search';
$whereClsGlobal = '';
if ($slug == 'search') 
{
	$vtid = $_POST['vtid'];
	$bid = $_POST['bid_FK'];
	$mid = $_POST['mid_FK'];
	
	$whereClsGlobal = " FIND_IN_SET($vtid, p.vtid_FK) AND FIND_IN_SET($bid, p.bid_FK) AND FIND_IN_SET($mid, p.mid_FK)";
}
else
{
	$whereClsGlobal = "s.slug = '$slug'";
}

$PAGE_TITLE = ucwords(str_replace('-', ' ', $slug));

include_once("include/meta-header.php");
include_once("include/header.php");
$whereCls = " $whereClsGlobal AND s.segmentCode = p.segmentCode_RK AND p.status = 'A' AND p.prdType = 'P' AND p.isDeleted = 'N' AND s.isDeleted = 'N'";
$rcdInfoF = '';
?>
<div class="divider6"></div>
<?php
$rcdInfoArr = $objDBQuery->getRecord(0, "*, s.slug AS pSlug", 'tbl_products p, tbl_segments s', $whereCls, '', '', 'p.createdOn', 'DESC');	
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
	$rcdInfoF = 1;
	$numOfRow = count($rcdInfoArr);
	for ($j = 0; $j < $numOfRow; $j++)
	{
		$prdFor = $rcdInfoArr[$j]['prdFor']; 
		$prdPdf = $rcdInfoArr[$j]['prdPdf']; 	 

		$pdfPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$prdPdf; 
		$imgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[$j]['prdDeatilPageImg']; 

		if ($prdFor == 'G') $iconPath = HTTP_PATH_ASSET_UPLOAD.'/segment_imgs/'.$rcdInfoArr[$j]['icon']; 
		else if ($prdFor == 'ECG') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/gce.png';
		else if ($prdFor == 'GC') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/gear-clutch.png';
		else if ($prdFor == 'C') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/clutch.png';
		else if ($prdFor == 'GE') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/gear.png';
		else if ($prdFor == 'E') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/engine.png';
?>

		<section class="glide-us-panel position-relative d-block">
			<a href="<?php echo HTTP_PATH?>/products/<?php echo $rcdInfoArr[$j]['pSlug']?>" class=" float-start ms-2 mt-3" aria-label="Close"><i class="fa fa-times fs-4 text-muted" alt="Close"></i></a>
			
			<div class="text-end icon-right-set">
<?php
			if ($prdFor != '')
			{
?>
				<img src="<?php echo $iconPath?>" class="img-fluid">
<?php
			}
?>
			</div>
			<div class="container">
				<div class="row justify-content-center align-items-center hts-100 py-5">
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6">
						<div class="pipe-set-about">BENEFIT OF THE PRODUCT</div>
						<div class="ft-20 mt-4">
							<p class=" font-bebas-neue fs-4 fw-bold text-danger"><?php echo strip_tags($rcdInfoArr[$j]['title'])?></p>
							<p><?php echo nl2br($rcdInfoArr[$j]['description'])?></p>
							
							<div class="mb-3">
<?php
							if ($prdPdf != '')
							{
?>
								<a class="btn button-rounded" href="<?php echo $pdfPath?>" target="_blank">Product Description</a>
<?php
							}
?>
							<!--<a class="btn back-button" href="">Back</a>-->
							</div>
					 </div>
			</div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 text-center p-5 position-relative">
<?php
				$imgWidth = '';
				if ($prdFor == 'G')
				{
					$imgWidth = "width='207px'";
?>
                <div class="circle d-none d-md-block d-lg-block d-xl-block d-xxl-block"><img src="<?php echo HTTP_PATH_ASSET?>/images/seg_detail_page_icons/circle.png" class="img-fluid" ></div>
<?php
				}
?>
                <img src="<?php echo $imgPath?>" class="img-fluid" <?php echo $imgWidth?>>
            </div>
        </div>
    </div>
    <div class="dark-grey col-sm-12 p-0">
        <div class="d-flex flex-wrap bd-highlight">
            <div class="bg-light-secondary text-white p-3 fw-bold">SKUs Available</div>
 <?php
			$qtyArr = strDataExplode($rcdInfoArr[$j]['qty']);
			
			$tQty = count($qtyArr);
			for ($i = 0; $i < $tQty; $i++)
			{
				$strPipe = '<div class="py-3 fw-bold">I</div>'; 
				if ($i == $tQty -1) $strPipe = '';
?>
				<div class="py-3 fw-bold px-4"><?php echo $qtyArr[$i]?></div><?php echo $strPipe?>
				   
<?php
			}
?>
        </div>
    </div>
</section>


<?php
	}
}

$whereCls = "$whereClsGlobal AND s.segmentCode = p.segmentCode_RK AND p.status = 'A' AND p.prdType = 'S' AND p.isDeleted = 'N' AND s.isDeleted = 'N'";
$rcdInfoArr = $objDBQuery->getRecord(0, '*, p.slug AS pSlug', 'tbl_products p, tbl_segments s', $whereCls, '', '', 'p.createdOn', 'DESC');	
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{	
?>

<section class="bg-secondary-grey">
    <div class="container-fluid">
        <div class="row row-flex py-5 gy-4">
<?php
		$numOfRows = count($rcdInfoArr);
		for ($i = 0; $i < $numOfRows; $i++)
		{
			$imgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[$i]['prdImg']; 

			$strTitle = $rcdInfoArr[$i]['title'];
			@list($t1, $t2, $t3) = explode(' ', $strTitle, 2);
			$prdPdf = $rcdInfoArr[$i]['prdPdf']; 

			$pdfPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$prdPdf;
?>		
            
			<div class="col-lg-6 col-md-12 col-sm-12">
                <div class="bg-secondary-grey-still p-5 overflow-hidden bdr-radius h-100">
                    <div class="row align-items-center justify-content-xxl-start justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-sm-center">
                        <div class="col-md-6">
                            <p class="fw-bold ft-36 mb-0"><?php echo $t1?></p>
<?php
							if (@$t2) echo "<p class='fw-bold ft-48'>$t2</p>";                            
							if (@$t3) echo "<p class='fw-bold ft-48'>$t3</p>";   
?>						
                            <p class="fs-5 font-bebas-neue fw-bold"><?php echo $rcdInfoArr[$i]['prdsku']?></p>
<?php
				if ($prdPdf != '')
				{
?>				
                    <a class="btn button-rounded my-3" href="<?php echo $pdfPath?>" target="_blank">Product Description</a>
<?php
				}
?>
                        </div>
                        <div
                            class="col-md-6 position-relative text-center">
                            <img src="<?php echo $imgPath?>" width="250">
                        </div>
                    </div>
                </div>
            </div>
<?php
		}
?>
		</div>
    </div>
</section>
<?php
}
if (empty($rcdInfoArr) && $rcdInfoF == '') 
{
?>			

  <div class="container">
      <div class="row ht-100 justify-content-center align-items-center">
          <div class="col-md-12 text-center">
              <img src="<?php echo HTTP_PATH_ASSET?>/images/no_result.gif
" class="img-fluid">
      <div class="font-bebas-neue fs-4 fw-bold">Sorry! No Search Result Found.</div> 
      <small class="text-secondary">We are sorry what you were looking for.  Please try another way. </small>
      </div>
      </div>
      </div>      
<?php
}
?>


<?php
 include_once("include/footer.php");
?> 