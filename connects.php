<?php
$PAGE_TITLE = "Connects";
include_once("include/meta-header.php");
include_once("include/header.php");
?>
<div class="divider5"></div>
    <section class="journey-us-panel pb-5">
        <div class="container">
            <div class="intro">
                <div class="text-center font-bebas-neue text-black ft-48 py-5">FAQ & Information
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="accordion accordion-flush" id="accordionFlushFaq">
  <?php
                    $whereCls = "status = 'A' AND isDeleted = 'N'";
                    $rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_faqs', $whereCls, '', '', 'faqOrder ASC, createdOn', 'DESC');
                    if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
                    {
                        $numOfRows = count($rcdInfoArr);
                        for ($i = 0; $i < $numOfRows; $i++)
                        {
                            $accordionId = 'flush_heading_'.$i;

  ?>                      
                            <div class="accordion-item">
                                <div class="accordion-header" id="<?php echo $accordionId?>">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse<?php echo $accordionId?>" aria-expanded="false"
                                        aria-controls="<?php echo $accordionId?>"><?php echo $rcdInfoArr[$i]['title']?>
                                    </button>
                                </div>
                                <div id="collapse<?php echo $accordionId?>" class="accordion-collapse collapse"
                                    aria-labelledby="<?php echo $accordionId?>" data-bs-parent="#accordionFlushFaq">
                                    <div class="accordion-body"><?php echo $rcdInfoArr[$i]['description']?></div>
                                </div>
                            </div> 

 <?php
                        }
                   }
 ?>                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="bg-gallery">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 pt-5">

                        <div class=" text-white ft-36 pt-3 pb-4">Advantages to Join Adrol
                        </div>
                        <ul class="text-white font-bebas-neue oil-li">
                            <li class="pb-3 pl-3"> High quality lubricant manufacturing plant with qualified in
                                house R&amp;D team.</li>
                            <li class="pb-3 pl-3">Entire range of Automotive &amp; Industrial Products as per the market
                                requirement.</li>
                            <li class="pb-3 pl-3">Sales and Marketing team with in depth knowledge of the lubricant
                                market
                                for business
                                support.</li>
                            <li class="pb-3 pl-3">Quality products at competitive pricing.</li>
                            <li class="pb-3 pl-3">We give time to time training of products, viscosity, grade,
                                application
                                etc. we give
                                training of CRM software with features of stock, account, customer management etc with
                                analysis.</li>
                        </ul>
                    </div>
                     <div class="col-md-6 pt-5">
                        <div class=" text-white ft-36 pb-4">Why To Join Adrol</div>
                        <p class="text-white font-bebas-neue">
                            "Gargo International" manufacturers of lubricants under the brand name of "Adrol
                            Lubricants". The world class performance of product drives through resources of best
                            Research and development along with experience of excellence, providing best process and
                            manpower to become a Global leader.
                        </p>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
     <section style="background: #e1b50e;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo HTTP_PATH_ASSET?>/images/map-min.jpg" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
<?php
    include_once("include/footer.php");
?> 