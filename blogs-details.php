<?php
$slug = $_GET['slug'];
$metaFrm = "PD";
$PAGE_TITLE = ucwords(str_replace('-', ' ', $slug));
include_once("include/meta-header.php");
include_once("include/header.php");
$whereCls = "status = 'A' AND isDeleted = 'N' AND postType = 'B' AND slug = '$slug'";
$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_posts', $whereCls, '', '', 'createdOn', 'DESC');
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
	$imgPath = HTTP_PATH_ASSET_UPLOAD.'/post_imgs/'.$rcdInfoArr[0]['postBanner']; 
?>
<div class="divider5"></div>
    <section class="journey-us-panel pb-5">
        <div class="container">
            <div class="text-center text-black py-5 mb-0 font-bebas-neue ft-4">Blog Details</div>
            <div class="row justify-content-center">
                <div class="col-sm-12 col-lg-10">
                    <div class="card shadow-sm bg-radi">
                        <img class="bd-placeholder-img bg-radi card-img-top" width="100%" height=
                        "320" src="<?php echo $imgPath?>">
                        <div class="card-body">
                            <p class="header font-bebas-neue fw-bold fs-4 text-danger"><?php echo $rcdInfoArr[0]['postTitle']?></p>
                            <p class="card-text"><?php echo nl2br($rcdInfoArr[0]['postDesc'])?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
}
include_once("include/footer.php");
?> 