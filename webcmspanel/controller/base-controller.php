<?php
include_once('../../web-config.php');

include_once('../../libs/db_classes/DBQuery.php');
$objDBQuery = new DBQuery();

include_once('../../libs/functions/common.php');
include_once('../../libs/functions/form-validation.php');

if (isset($_POST['postAction']))  $accessCase = $_POST['postAction'];
else if (isset($_GET['getAction'])) $accessCase = $_GET['getAction'];

// IF FORM TOKEN IS NOT VALID THEN RETURN ON DEFAULT CASE
if (!empty($_POST['formToken']) && $_POST['formToken'] != $_SESSION['prepareToken'])
{
	$accessCase = '';
}
