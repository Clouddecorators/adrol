<?php
include_once('base-controller.php'); 

$msgTxt = 'meta tag';
$tblName = 'tbl_meta_tags';
$dbFld4checkDulicate = 'title';
$enckeyDBFldName = 'metaTagCode';

$formRedirectUrl = '../add-edit-meta-tag.php?'.$_SESSION['SESSION_QRY_STRING_FOR_MTAG'];
$viewRedirectUrl = '../view-all-meta-tags.php?'.$_SESSION['SESSION_QRY_STRING_FOR_MTAG'];

switch ($accessCase) 
{
	case 'addAction':		
		$_POST = trimFormValue(0, $_POST);
		$title = addslashes($_POST['title']);
		$headerRedirectUrl = $formRedirectUrl;
		$arrExcludedFrmKeys = array($enckeyDBFldName);		
		$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'], $arrExcludedFrmKeys);
		
		if ($objDBQuery->getRecordCount(0, $tblName, " isDeleted = 'N' AND $dbFld4checkDulicate = '".strtolower($dataArr[$dbFld4checkDulicate])."'"))
		{
			viewState($dataArr, 1);
			$_SESSION['messageSession'] = "Same  $msgTxt name exists, please try with another.";	
		}		
		else if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';	
			//$dataArr['pageAbbr'] = makeSlugURL($title);		
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);				

			$dataArr[$enckeyDBFldName] = randomMD5();
			if ($objDBQuery->addRecord(0, $dataArr, $tblName))
			{				
				$msg = "New $msgTxt has been added successfully.";				
				$_SESSION['msgTrue'] = 1;
				$headerRedirectUrl = $viewRedirectUrl;				
			}
			else $msg = "Record does not add.";	
			$_SESSION['messageSession'] = $msg;	
		}
		else
		{
			viewState($dataArr, 1);
		}
		break;

	case 'updateAction':
		$_POST = trimFormValue(0, $_POST);
		$enkey = $_POST['enkey'];
		$title = addslashes($_POST['title']);

		$headerRedirectUrl = $formRedirectUrl.'&enkey='.$enkey; 
		
		 if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';						
			$arrExcludedFrmKeys = array($enckeyDBFldName);		
			$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'], $arrExcludedFrmKeys);					
			
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);			

			if ($objDBQuery->updateRecord(0, $dataArr, $tblName, array($enckeyDBFldName => $enkey)))
			{				
				$_SESSION['msgTrue'] = 1;				
				$headerRedirectUrl = $viewRedirectUrl;
				$msg = ucfirst($msgTxt)." detail has been updated successfully.";	
			}
			else $msg = "Data does not update";	
			$_SESSION['messageSession'] = $msg;	
		}	
		break;

	// Don't remove this case
	default: 
		$_SESSION['messageSession'] = 'Access case does not found.';
		$headerRedirectUrl = '../';
		break;
}

unset($objDBQuery);

if (isset($_SESSION['formValidation'])) unset($_SESSION['formValidation']);

if (isset($headerRedirectUrl)) headerRedirect($headerRedirectUrl);
