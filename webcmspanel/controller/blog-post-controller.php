<?php
include_once('base-controller.php'); 

$msgTxt = 'blog post';
$tblName = 'tbl_posts';
$dbFld4checkDulicate = 'postTitle';
$enckeyDBFldName = 'postCode';
$assetDirName = 'post_imgs';

$formRedirectUrl = '../add-edit-blog-post.php?'.$_SESSION['SESSION_QRY_STRING_FOR_POST'];
$viewRedirectUrl = '../view-all-blog-posts.php?'.$_SESSION['SESSION_QRY_STRING_FOR_POST'];

switch ($accessCase) 
{
	case 'deleteRecordAction':	
		$_POST = trimFormValue(0, $_POST);
		$enckey = $_POST['enckey'];
		$headerRedirectUrl = $viewRedirectUrl;
		
		if (!$enckey) $msg = "Please enter all required fields.";		
		else if (!$objDBQuery->getRecordCount(0, $tblName, array($enckeyDBFldName => $enckey))) $msg = "Record does not match with our db record.";  
		else if ($enckey)
		{
			$objDBQuery->deleteRecord(0, $tblName, array($enckeyDBFldName => $enckey));
			$_SESSION['msgTrue'] = 1;
			$msg = "Record has been deleted successfully.";
		}	
		$_SESSION['messageSession'] = $msg;
		break;

	case 'changeRecordStatus':		
		$_POST = trimFormValue(0, $_POST);
		$enckey = $_POST['enckey'];
		$headerRedirectUrl = $viewRedirectUrl;
		
		if (!$enckey) $msg = "Please enter all required fields.";		
		else if (!$objDBQuery->getRecordCount(0, $tblName, array($enckeyDBFldName =>$enckey))) $msg = "Record does not match with our db record."; 
		else if ($enckey)
		{
			$infoArr = $objDBQuery->getRecord(0, array($enckeyDBFldName, 'status'), $tblName, array($enckeyDBFldName => $enckey));	
			$status = $infoArr[0]['status'];
			
			if (strtolower($status) == strtolower("A")) 
			{
				$updatedId = $objDBQuery->updateRecord(0, array('status' => 'I'), $tblName, array($enckeyDBFldName => $enckey));
			}
			else if (strtolower($status) == strtolower("I"))
			{
				$updatedId = $objDBQuery->updateRecord(0, array('status' => 'A'), $tblName, array($enckeyDBFldName => $enckey));
			}

			if ($updatedId)
			{
				$msg = "Status has been changed succussfully.";
				$_SESSION['msgTrue'] = 1;				
			}
			else $msg = "Status does not change in our db record.";
		}	
		$_SESSION['messageSession'] = $msg;
		break;
	
	case 'addAction':		
		$_POST = trimFormValue(0, $_POST);
		$title = addslashes($_POST['postTitle']);
		$imgKey = "postThumb";
		$imgKey2 = "postBanner";
		$headerRedirectUrl = $formRedirectUrl;
		$arrExcludedFrmKeys = array($enckeyDBFldName);		
		$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'], $arrExcludedFrmKeys);
		
		if ($objDBQuery->getRecordCount(0, $tblName, " isDeleted = 'N' AND $dbFld4checkDulicate = '".strtolower($dataArr[$dbFld4checkDulicate])."'"))
		{
			viewState($dataArr, 1);
			$_SESSION['messageSession'] = "Same  $msgTxt name exists, please try with another.";	
		}
		else if (allowedFIleExten($imgKey))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
			viewState($dataArr, 1);			
		}		
		else if (allowedFIleExten($imgKey2))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
			viewState($dataArr, 1);			
		}		
		else if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';
			$fileName =  fileUpload(0, $imgKey, $assetDirName);
			if ($fileName) $dataArr[$imgKey] = $fileName;
			
			$fileName =  fileUpload(0, $imgKey2, $assetDirName);
			if ($fileName) $dataArr[$imgKey2] = $fileName;
			
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);	
			$dataArr[$enckeyDBFldName] = randomMD5();
			$dataArr['slug'] = makeSlugURL($dataArr['postTitle']);
			if ($objDBQuery->addRecord(0, $dataArr, $tblName))
			{				
				$msg = "New $msgTxt has been added successfully.";				
				$_SESSION['msgTrue'] = 1;
				$headerRedirectUrl = $viewRedirectUrl;				
			}
			else $msg = "Record does not add.";	
			$_SESSION['messageSession'] = $msg;	
		}
		else
		{
			viewState($dataArr, 1);
		}
		break;

	case 'updateAction':
		$_POST = trimFormValue(0, $_POST);
		$enkey = $_POST['enkey'];
		$imgKey = "postThumb";
		$imgKey2 = "postBanner";
		$title = addslashes($_POST['postTitle']);

		$headerRedirectUrl = $formRedirectUrl.'&enkey='.$enkey; 
		
		if ($objDBQuery->getRecordCount(0, $tblName, " isDeleted = 'N' AND $enckeyDBFldName != '$enkey' AND $dbFld4checkDulicate = '".strtolower($_POST[$dbFld4checkDulicate])."'"))
		{			
			$_SESSION['messageSession'] = "Same  $msgTxt name exists, please try with another.";
		}
		else if (allowedFIleExten($imgKey))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
		}	
		else if (allowedFIleExten($imgKey2))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
		}	
		else if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';						
			$arrExcludedFrmKeys = array($enckeyDBFldName);		
			$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'], $arrExcludedFrmKeys);
			
			$infoArr = $objDBQuery->getRecord(0, array($imgKey, $imgKey2), $tblName, array($enckeyDBFldName => $enkey));
			$fileName =  fileUpload(0, $imgKey, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$imgKey]) 
			{
				$dataArr[$imgKey] = $fileName;				
				unlinkFile(0, $infoArr[0][$imgKey], $assetDirName);
			}		

			$fileName =  fileUpload(0, $imgKey2, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$imgKey2]) 
			{
				$dataArr[$imgKey2] = $fileName;				
				unlinkFile(0, $infoArr[0][$imgKey2], $assetDirName);
			}		
			
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);
			$dataArr['slug'] = makeSlugURL($dataArr['postTitle']);
			if ($objDBQuery->updateRecord(0, $dataArr, $tblName, array($enckeyDBFldName => $enkey)))
			{				
				$_SESSION['msgTrue'] = 1;				
				$headerRedirectUrl = $viewRedirectUrl;
				$msg = ucfirst($msgTxt)." detail has been updated successfully.";	
			}
			else $msg = "Data does not update";	
			$_SESSION['messageSession'] = $msg;	
		}	
		break;

	// Don't remove this case
	default: 
		$_SESSION['messageSession'] = 'Access case does not found.';
		$headerRedirectUrl = '../';
		break;
}

unset($objDBQuery);

if (isset($_SESSION['formValidation'])) unset($_SESSION['formValidation']);

if (isset($headerRedirectUrl)) headerRedirect($headerRedirectUrl);
