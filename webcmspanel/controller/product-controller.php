<?php
include_once('base-controller.php'); 

$msgTxt = 'product';
$tblName = 'tbl_products';
$dbFld4checkDulicate = 'title';
$enckeyDBFldName = 'prdCode';
$assetDirName = 'product_imgs';
$imgKey = "prdImg";
$imgKey2 = "prdDeatilPageImg";
$imgKey3 = "prdMobViewImg";
$imgKey4 = "prdTabViewImg";
$pdfKey = "prdPdf";

$formRedirectUrl = '../add-edit-product.php?'.$_SESSION['SESSION_QRY_STRING_FOR_PRODUCT'];
$viewRedirectUrl = '../view-all-products.php?'.$_SESSION['SESSION_QRY_STRING_FOR_PRODUCT'];

switch ($accessCase) 
{
	case 'deleteRecordAction':	
		$_POST = trimFormValue(0, $_POST);
		$enckey = $_POST['enckey'];
		$headerRedirectUrl = $viewRedirectUrl;
		
		if (!$enckey) $msg = "Please enter all required fields.";		
		else if (!$objDBQuery->getRecordCount(0, $tblName, array($enckeyDBFldName => $enckey))) $msg = "Record does not match with our db record.";  
		else if ($enckey)
		{
			$objDBQuery->deleteRecord(0, $tblName, array($enckeyDBFldName => $enckey));
			$_SESSION['msgTrue'] = 1;
			$msg = "Record has been deleted successfully.";
		}	
		$_SESSION['messageSession'] = $msg;
		break;

	case 'changeRecordStatus':		
		$_POST = trimFormValue(0, $_POST);
		$enckey = $_POST['enckey'];
		$headerRedirectUrl = $viewRedirectUrl;
		
		if (!$enckey) $msg = "Please enter all required fields.";		
		else if (!$objDBQuery->getRecordCount(0, $tblName, array($enckeyDBFldName =>$enckey))) $msg = "Record does not match with our db record."; 
		else if ($enckey)
		{
			$infoArr = $objDBQuery->getRecord(0, array($enckeyDBFldName, 'status'), $tblName, array($enckeyDBFldName => $enckey));	
			$status = $infoArr[0]['status'];
			
			if (strtolower($status) == strtolower("A")) 
			{
				$updatedId = $objDBQuery->updateRecord(0, array('status' => 'I'), $tblName, array($enckeyDBFldName => $enckey));
			}
			else if (strtolower($status) == strtolower("I"))
			{
				$updatedId = $objDBQuery->updateRecord(0, array('status' => 'A'), $tblName, array($enckeyDBFldName => $enckey));
			}

			if ($updatedId)
			{
				$msg = "Status has been changed succussfully.";
				$_SESSION['msgTrue'] = 1;				
			}
			else $msg = "Status does not change in our db record.";
		}	
		$_SESSION['messageSession'] = $msg;
		break;
	
	case 'addAction':		
		$_POST = trimFormValue(0, $_POST, 'Y');		
		$headerRedirectUrl = $formRedirectUrl;
		$arrExcludedFrmKeys = array($enckeyDBFldName);		
		$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'], $arrExcludedFrmKeys);
		
		if ($objDBQuery->getRecordCount(0, $tblName, " isDeleted = 'N' AND $dbFld4checkDulicate = '".strtolower($dataArr[$dbFld4checkDulicate])."'"))
		{
			viewState($dataArr, 1);
			$_SESSION['messageSession'] = "Same  $msgTxt name exists, please try with another.";	
		}
		else if (allowedFIleExten($imgKey))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
			viewState($dataArr, 1);			
		}		
		else if (allowedFIleExten($imgKey2))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
			viewState($dataArr, 1);			
		}	
		else if (allowedFIleExten($imgKey3))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
			viewState($dataArr, 1);			
		}	
		else if (allowedFIleExten($imgKey4))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
			viewState($dataArr, 1);			
		}	
		else if (allowedFIleExten($pdfKey, array('pdf')))
		{
			$_SESSION['messageSession'] = "Please upload pdf";	
			viewState($dataArr, 1);			
		}		
		else if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';
			$fileName =  fileUpload(0, $imgKey, $assetDirName);
			if ($fileName) $dataArr[$imgKey] = $fileName;
			
			$fileName =  fileUpload(0, $imgKey2, $assetDirName);
			if ($fileName) $dataArr[$imgKey2] = $fileName;
			
			$fileName =  fileUpload(0, $imgKey3, $assetDirName);
			if ($fileName) $dataArr[$imgKey3] = $fileName;
			
			$fileName =  fileUpload(0, $imgKey4, $assetDirName);
			if ($fileName) $dataArr[$imgKey4] = $fileName;

			$fileName =  fileUpload(0, $pdfKey, $assetDirName);
			if ($fileName) $dataArr[$pdfKey] = $fileName;
			
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);	
			$dataArr[$enckeyDBFldName] = randomMD5();
			$dataArr['slug'] = makeSlugURL($dataArr['title']);
			if ($objDBQuery->addRecord(0, $dataArr, $tblName))
			{				
				$msg = "New $msgTxt has been added successfully.";				
				$_SESSION['msgTrue'] = 1;
				$headerRedirectUrl = $viewRedirectUrl;				
			}
			else $msg = "Record does not add.";	
			$_SESSION['messageSession'] = $msg;	
		}
		else
		{
			viewState($dataArr, 1);
		}
		break;

	case 'updateAction':
		$_POST = trimFormValue(0, $_POST, 'Y');
		$enkey = $_POST['enkey'];		

		$headerRedirectUrl = $formRedirectUrl.'&enkey='.$enkey; 
		
		if ($objDBQuery->getRecordCount(0, $tblName, " isDeleted = 'N' AND $enckeyDBFldName != '$enkey' AND $dbFld4checkDulicate = '".strtolower($_POST[$dbFld4checkDulicate])."'"))
		{			
			$_SESSION['messageSession'] = "Same  $msgTxt name exists, please try with another.";
		}
		else if (allowedFIleExten($imgKey))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
		}	
		else if (allowedFIleExten($imgKey2))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
		}	
		else if (allowedFIleExten($imgKey3))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
		}	
		else if (allowedFIleExten($imgKey4))
		{
			$_SESSION['messageSession'] = "Please upload poster format png or jpg";	
		}	
		else if (allowedFIleExten($pdfKey, array('pdf')))
		{
			$_SESSION['messageSession'] = "Please upload pdf";	
			viewState($dataArr, 1);			
		}
		else if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';						
			$arrExcludedFrmKeys = array($enckeyDBFldName);		
			$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'], $arrExcludedFrmKeys);
			
			$infoArr = $objDBQuery->getRecord(0, array($imgKey, $imgKey2, $imgKey3, $imgKey4, $pdfKey), $tblName, array($enckeyDBFldName => $enkey));
			$fileName =  fileUpload(0, $imgKey, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$imgKey]) 
			{
				$dataArr[$imgKey] = $fileName;				
				unlinkFile(0, $infoArr[0][$imgKey], $assetDirName);
			}		

			$fileName =  fileUpload(0, $imgKey2, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$imgKey2]) 
			{
				$dataArr[$imgKey2] = $fileName;				
				unlinkFile(0, $infoArr[0][$imgKey2], $assetDirName);
			}

			$fileName =  fileUpload(0, $imgKey3, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$imgKey3]) 
			{
				$dataArr[$imgKey3] = $fileName;				
				unlinkFile(0, $infoArr[0][$imgKey3], $assetDirName);
			}
			
			$fileName =  fileUpload(0, $imgKey4, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$imgKey4]) 
			{
				$dataArr[$imgKey4] = $fileName;				
				unlinkFile(0, $infoArr[0][$imgKey4], $assetDirName);
			}
			
			$fileName =  fileUpload(0, $pdfKey, $assetDirName);
			if ($fileName && $fileName != $infoArr[0][$pdfKey]) 
			{
				$dataArr[$pdfKey] = $fileName;				
				unlinkFile(0, $infoArr[0][$pdfKey], $assetDirName);
			}		
			
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);
			$dataArr['slug'] = makeSlugURL($dataArr['title']);
			if ($objDBQuery->updateRecord(0, $dataArr, $tblName, array($enckeyDBFldName => $enkey)))
			{				
				$_SESSION['msgTrue'] = 1;				
				$headerRedirectUrl = $viewRedirectUrl;
				$msg = ucfirst($msgTxt)." detail has been updated successfully.";	
			}
			else $msg = "Data does not update";	
			$_SESSION['messageSession'] = $msg;	
		}	
		break;

	// Don't remove this case
	default: 
		$_SESSION['messageSession'] = 'Access case does not found.';
		$headerRedirectUrl = '../';
		break;
}

unset($objDBQuery);

if (isset($_SESSION['formValidation'])) unset($_SESSION['formValidation']);

if (isset($headerRedirectUrl)) headerRedirect($headerRedirectUrl);
