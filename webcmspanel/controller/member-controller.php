<?php
include_once('base-controller.php');

switch ($accessCase) 
{	
	case 'memberRegistration':	
		$tblName = 'tbl_users';		
		$_POST = trimFormValue(0, $_POST);		
		$headerRedirectUrl = "../member-registration.php";
		$arrExcludedFrmKeys = array('userCode', 'phone1Code', 'whatsappCode', 'phoneCode');		
		$dataArr = prepareKeyValueForDBQuery(0, $_POST, $_SESSION['ARR_ALLOW_FORM_KEYS'], $arrExcludedFrmKeys);
		
		if ($objDBQuery->getRecordCount(0, $tblName, "lower(email) = '".strtolower($dataArr['email'])."'"))
		{
			viewState($dataArr, 1);
			$_SESSION['messageSession'] = 'Same email id exists, please try with another.';	
		}			
		else if (!validateForm($_SESSION['formValidation']))
		{
			$msg = '';			
			$password = makeRandNo8Digit();
			$hashedPassword = generatePassword($password);					
			$dataArr['userCode'] = randomMD5();
			$dataArr['password'] = $hashedPassword;
			$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);	

			if ($objDBQuery->addRecord(0, $dataArr, $tblName))
			{							
				$_SESSION['msgTrue'] = 1;	
				$email = $dataArr['email'];			
				$msg = "Your account has been created and PASSWORD has been sent to <span style='color:blue;'>$email</span>.";	
				makeEmailStructure('addUserAccount', $dataArr['email'], $dataArr['fullName'], $dataArr['email'], $password);				

				$headerRedirectUrl = "../index.php";

				$arrMsgKeyVal["[FORM_EMAIL]"] = $email;				
				traceActivity(0, 'registrationCompleted', $arrMsgKeyVal, $dataArr['userCode']);
			}
			else
			{
				viewState($dataArr, 1);
				$msg = "Your data does not add.";	
			} 
			$_SESSION['messageSession'] = $msg;	
		}
		else
		{
			viewState($dataArr, 1);
		}
		break;

	case 'changePassword':
			$_POST = trimFormValue(0, $_POST);				
			$headerRedirectUrl = '../change-password.php';

			if (!validateForm($_SESSION['formValidation']))
			{
				$msg = '';
				$oldPassword = $_POST['oldPassword'];
				$hashedPassword = generatePassword($oldPassword);	
				$userCode = $_POST['userCode'];
				if (!$objDBQuery->getRecordCount(0, 'tbl_users', "password = '$hashedPassword' AND userCode = '$userCode'"))
				{
					$msg = "Sorry, entered password does not match with our record.";					
				}
				else if ($userCode)
				{			
					$frmKeyExcludeArr = array('submitBtn', 'formToken', 'postAction', 'userCode', 'username', 'oldPassword', 'cpassword', 'password');
					//$dataArr = prepareKeyValue4Msql(1, $_POST, $frmKeyExcludeArr);
					$newPassword = $_POST['password'];
					$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);
					$dataArr['password'] = generatePassword($newPassword);
					$oldEmail = $objDBQuery->getRecord(0, array('email'), 'tbl_users', array('userCode' => $userCode))[0]['email'];
					if ($objDBQuery->updateRecord(0, $dataArr, 'tbl_users', array('userCode' => $userCode)))
					{				
						$userAccInfoArr = $objDBQuery->getRecord(0, array('userCode', 'email', 'password', 'fullName', 'accountType', 'lastLoginIpAddress', 'lastLoginOn', 'accountStatus'), 'tbl_users', array('userCode' => $userCode));				
						$_SESSION['userDetails'] = $userAccInfoArr[0];						

						if ($newPassword != $oldPassword)
						{
							makeEmailStructure('changePassword', $userAccInfoArr[0]['email'], $userAccInfoArr[0]['fullName'], $userAccInfoArr[0]['email'], $newPassword);
						}					
						
						$arrMsgKeyVal["[SESSION_EMAIL]"] = $_SESSION['userDetails']['email'];				
				        traceActivity(0, 'changePassword', $arrMsgKeyVal);
						$msg = "Your password has been updated successfully.";				
						$_SESSION['msgTrue'] = 1;
					}
					else $msg = "Data does not update";
				}
				$_SESSION['messageSession'] = $msg;	
			}	
			break;

	case 'updateYourProfile':
			$_POST = trimFormValue(0, $_POST);				
			$headerRedirectUrl = '../profile.php';

			if (!validateForm($_SESSION['formValidation']))
			{
				$msg = '';
				$email = strtolower($_POST['email']);
				$phone = $_POST['phone'];
				$userCode = $_POST['userCode'];
				if ($objDBQuery->getRecordCount(0, 'tbl_users', "LOWER(email) = '$email' AND userCode != '$userCode'"))
				{
					$msg = "Same email id exists, please try with another.";
				}
				else if ($userCode)
				{			
					$frmKeyExcludeArr = array('submitBtn', 'formToken', 'postAction', 'userCode', 'username');
					$dataArr = prepareKeyValue4Msql(0, $_POST, $frmKeyExcludeArr);
					
					$oldEmail = $objDBQuery->getRecord(0, array('email'), 'tbl_users', array('userCode' => $userCode))[0]['email'];
					$dataArr['updatedOn'] = date(LONG_MYSQL_DATE_FORMAT);
					if ($objDBQuery->updateRecord(0, $dataArr, 'tbl_users', array('userCode' => $userCode)))
					{				
						$userAccInfoArr = $objDBQuery->getRecord(0, array('userCode', 'email', 'password', 'fullName', 'accountType', 'lastLoginIpAddress', 'lastLoginOn', 'accountStatus'), 'tbl_users', array('userCode' => $userCode));				
						$_SESSION['userDetails'] = $userAccInfoArr[0];
						/*
						if (strtolower($oldEmail) != $email)
						{							
							makeEmailStructure('updateYourEmail', $oldEmail, $userAccInfoArr[0]['fname'], $userAccInfoArr[0]['username']);					
						}*/
	
						$arrMsgKeyVal["[SESSION_EMAIL]"] = $_SESSION['userDetails']['email'];				
				        traceActivity(0, 'updateYourProfile', $arrMsgKeyVal);
						$msg = "Your profile information has been updated successfully.";				
						$_SESSION['msgTrue'] = 1;
					}
					else $msg = "Data does not update";
				}
				$_SESSION['messageSession'] = $msg;	
			}	
			break;

	case 'forgotPassword':
			$_POST = trimFormValue(0, $_POST);			
			$username = strtolower($_POST['username']); 
			$headerRedirectUrl = '../forgot-password.php';

			if (!validateForm($_SESSION['formValidation']))
			{
				if ($objDBQuery->getRecordCount(0, 'tbl_users', "LOWER(email) = '$username'"))
				{
					$accountInfo = $objDBQuery->getRecord(0, array('userCode', 'fullName','email'), 'tbl_users', "LOWER(email) = '$username'");	
					
					$password = makeRandNo8Digit();
					$encPassword = generatePassword($password);
					$updatedId = $objDBQuery->updateRecord(0, array('password' => $encPassword), 'tbl_users',array('userCode' => $accountInfo[0]['userCode']));
					if ($updatedId) 
					{
						$email = $accountInfo[0]['email'];
						makeEmailStructure('changePassword', $email, $accountInfo[0]['fullName'], $email, 
							$password);
						
						$arrMsgKeyVal["[FORM_EMAIL]"] = $email;				
						traceActivity(0, 'forgotPassword', $arrMsgKeyVal);
						$msg = "New password has been sent to your registered email id '$email'.";						
						$_SESSION['msgTrue'] = 1;
					}
					else $msg = 'There is a error. Please try again.';				
				} 
				else $msg = 'Sorry, entered email id does not match with our record.';
				$_SESSION['messageSession'] = $msg;
			}
			break;

	case 'userLoginAuthentication':	
		$_POST = trimFormValue(0, $_POST);
		$username = $objDBQuery->escapeSpecialCharForSql($_POST['email']);
		$password = $objDBQuery->escapeSpecialCharForSql($_POST['password']);
		$headerRedirectUrl = '../';

		if (!validateForm($_SESSION['formValidation']))
		{
			$checkLoginInfo = $objDBQuery->getRecord(0, array('password'), 'tbl_users', "email = '$username' AND accountStatus != 'D'");
			$hashedPassword = generatePassword($password);	

			if (empty($checkLoginInfo)) $_SESSION['messageSession'] = 'Invalid Email Id';
			else if ($checkLoginInfo[0]['password'] != $hashedPassword) $_SESSION['messageSession'] = 'Invalid Password';	
			else if (!empty($checkLoginInfo)) 
			{
				$accountDetails = $objDBQuery->getRecord(0, array('userCode', 'email', 'password', 'fullName', 'accountType', 'lastLoginIpAddress', 'lastLoginOn', 'accountStatus'), 'tbl_users', array('email' => $username, 'password' => $hashedPassword));	
				
				if (empty($accountDetails)) $_SESSION['messageSession'] = 'A technical Problem';
				else if ($accountDetails[0]['accountStatus'] == 'A')
				{
					$_SESSION['userDetails'] = $accountDetails[0];
					$objDBQuery->updateRecord(0, array('lastLoginIpAddress' => getRealIpAddr(), 'lastLoginOn'=> date(LONG_MYSQL_DATE_FORMAT)), 'tbl_users',array('userCode' => $accountDetails[0]['userCode']));
					
					$arrMsgKeyVal["[FORM_EMAIL]"] = $username;				
					traceActivity(0, 'login', $arrMsgKeyVal);
					$headerRedirectUrl = getLandingPage('Y');
				}
				else if ($accountDetails[0]['accountStatus'] == 'I') $_SESSION['messageSession'] = 'Your account has been suspended, please contact with the website admin.';
				else $_SESSION['messageSession'] = "Condition does not match."; 
			}
		}
		break;

	// Don't remove this case
	default: 
		$_SESSION['messageSession'] = SESSION_EXPIRED_MSG;
		$headerRedirectUrl = '../';

		break;
}

unset($objDBQuery);

if (isset($_SESSION['formValidation'])) unset($_SESSION['formValidation']);

if (isset($headerRedirectUrl)) headerRedirect($headerRedirectUrl);
