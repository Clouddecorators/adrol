<?php
	$SUBTITLE = "Fogot Password";
	include("includes/meta-header.php");
	$valParamArray = array("username" => array("type" => "email", "msg" => "Email Id"));

	$_SESSION['formValidation'] = $valParamArray;
	include_once("includes/check-frontend-auth.php");
?>
<!-- End of meta header -->
<div class="login_banner">
<div class="center-block w-xxl w-auto-xs login-box login_box_width">
	<!-- Start of sign form -->
	<div class="box-color r box-shadow-z1 text-color mt content_gapbox">
		<!-- Start of logo -->
		<div class="logo-bottom-margin">
			<div class="centerinbox">
				<a class="navbar-img-large" href="index.php"><img src="images/admin_logo.png" alt="<?php echo removeStr(ADMIN_PANEL_TITLE); ?>" > 
				</a>
			</div>
			<div class="sign-text">Fogot Password</div>
		</div>
		<!-- End of logo -->
		
		
		<div class="login-text" style='display:none;'>Fogot Password</div>
		<?php
			include_once('includes/flash-msg.php'); 
		?>
		<form name="login-form" method="post" action="controller/member-controller.php" onSubmit='return validation(1, <?php echo json_encode($valParamArray); ?>);'>
			<!-- Start of password -->
			<div class="md-form-group float-label">
				<input type="text" name="username" id="username" maxlength="100"  class="md-input" value="">
				<label>Email Id:<span class="cla_star">*</span></label>
				<span id='span_username' class='form_error'><?php showErrorMessage('username'); ?></span>
			</div>
			<!-- Start of sign in button -->
			<button type="submit" class="btn btn-warning btn-block mg-gap">Submit</button>
			<input type="hidden" name="postAction" value="forgotPassword">					
			<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
		</form>
		<div class="border-line"></div>
		<div class="p-v-lg text-center">
			<div class="gapinline_center"><a href="index.php" class="forgot-link">Back to Login</a>
			</div>
		</div>
	</div>
	<!-- End of sign form -->
</div>
<?php
include_once("includes/frontend-footer.php");
?>
</body>