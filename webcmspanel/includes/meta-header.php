<?php
$CUR_PAGE_NAME = basename($_SERVER['SCRIPT_NAME']);

include_once('../web-config.php');
include_once('../libs/db_classes/DBQuery.php');

$objDBQuery = new DBQuery();

include_once('../libs/functions/common.php');
include_once('../libs/functions/form-validation.php');

$_SESSION['prepareToken'] = randomMD5();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo ADMIN_PANEL_TITLE?><?php echo @$SUBTITLE?></title>
	<link rel='shortcut icon' type='image/x-icon' href='images/faviconi.ico'>
    <link rel="stylesheet" href="<?php echo HTTP_PATH_ADMIN?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo HTTP_PATH_ADMIN?>/css/bootstrap-select.css">
	<link rel="stylesheet" href="<?php echo HTTP_PATH_ADMIN?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo HTTP_PATH_ADMIN?>/css/style.css?time=<?php echo date('U')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
</head>
<body>