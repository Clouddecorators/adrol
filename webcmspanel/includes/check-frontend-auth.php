<?php
$objDBQuery = new DBQuery();

if (isset( $_SESSION[ 'userDetails' ] ) && $objDBQuery->getRecordCount( 0, 'tbl_users', array( 'email' => $_SESSION[ 'userDetails' ][ 'email' ], 'accountStatus' => 'A' ) ) ) {
	headerRedirect(getLandingPage());
}
?>