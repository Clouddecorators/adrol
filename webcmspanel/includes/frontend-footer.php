<div class="app-footer">
    <div class="s text-xs">
        <div class="copyright_center_login">
            <span class="copyright_text">&nbsp;<span class="footer-text"><?=COPY_RIGHT_INFO?></span></span>
        </div>
    </div>
</div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/form-validation.js?t=<?php echo date('U')?>"></script>
<script src="includes/js/common.js?t=<?php echo date('U')?>"></script>