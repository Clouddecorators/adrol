<?php
include( "includes/meta-header.php" );
$ACCOUNT_TYPE_LBL = '';
$ACCOUNT_TYPE = $_SESSION['userDetails']['accountType'];
if ($ACCOUNT_TYPE == 'S')
{
	$ACCOUNT_TYPE_LBL = ' ('.$ARR_ACCOUNT_TYPES[$ACCOUNT_TYPE].')';
	include_once( "includes/left-menubar.php" );
} 
else 
{
	//$ACCOUNT_TYPE_LBL = ' ('.$ARR_ACCOUNT_TYPES[$ACCOUNT_TYPE].')';
	include_once( "includes/left-menubar.php" );	
} 


// SECURE PAGE NAME
$securePageArray = array( 'index.php' );

// CHECK LOGIN USER'S REQUEST IS VALID OR NOT
if (isset( $_SESSION['userDetails']))
{
	$whereClause = array(
		'email' => $_SESSION['userDetails']['email'],
		'password' => $_SESSION['userDetails']['password'],
		'accountType' => $_SESSION['userDetails']['accountType'],
		'accountStatus' => $_SESSION['userDetails']['accountStatus']
	);

	// CHECK USER'S DETAILS ARE VALID OR NOT
	if (!$objDBQuery->getRecordCount(0, 'tbl_users', $whereClause)) headerRedirect('logout.php?logout=2');
}
else headerRedirect( 'index.php' );

// HERE USER ACCESSES SCRIPT NAME

$adminUserAccessArr = array('view-all-streams.php', 'view-all-shows.php','view-all-menus.php', 'view-all-categories.php', 'add-edit-app.php', 'add-edit-stream.php', 'add-edit-show.php','add-edit-menu.php', 'add-edit-category.php', 'profile.php', 'change-password.php');

if ($ACCOUNT_TYPE != 'S' && !in_array(basename($_SERVER['SCRIPT_NAME']), $adminUserAccessArr))
{
	headerRedirect('logout.php?logout=2');
}




$welcomeName = '';
if ( isset($_SESSION['userDetails']))
	{
		$welcomeName = ucfirst( $_SESSION['userDetails' ]['fullName']);
		$welcomeName = "$welcomeName{$ACCOUNT_TYPE_LBL}";

	}

//$excludeMenuOnPageArr = array( 'sign-up.php' );
?>
<!-- Start of main content -->
<div class="app-content box-shadow-z0" role="main">
	<!-- Start of header menu -->
	<div class="app-header box-shadow navbar-md">
		<div class="navbar">
			<!-- Open side - Naviation on mobile -->
			<a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up">
				<span class="material-icons  menu_icon">menu</span>
			</a>
		

			<!-- Page title - Bind to $state's title -->
			<div class="navbar-item pull-left h5" id="pageTitle"></div>

			<!-- Start of navbar right -->
				<ul class="nav navbar-nav pull-right">
				<li class="nav-item p-t p-b" style='display:none;'>
					<a class="btn btn-sm btn-primary btn_site" href="<?php echo HTTP_PATH?>" onclick="window.open('<?php echo HTTP_PATH?>');" target="_new" title="Website">
                    	<i class="material-icons">&#xe895;</i> Website
					</a>
				</li>
				<li class="nav-item p-t p-b"><a class="btn admin_color">Welcome, <span class="black_admin"><?php echo 
				$welcomeName?>!</span></a></li>
				<li class="nav-item dropdown">
					<a class="nav-link clear navblink" data-toggle="dropdown" href="index.php">
						<span class="material-icons  menu_icon">account_circle</span>
					</a>
				
					<div class="dropdown-menu pull-right dropdown-menu-scale">
						<a class="dropdown-item" href="profile.php"><span>Profile Setting</span></a>
						<a class="dropdown-item" href="logout.php">Logout</a>
					</div>
				</li>
			</ul>
			<!-- End of navbar right -->
		</div>
	</div>
	<!-- End of header menu -->