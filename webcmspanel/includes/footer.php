<div class="app-footer">
	<div class="p-a text-xs">
		<div class="copyright_center">
			<span class="copyright_text">&nbsp;
				<span class="footer-text"><?=COPY_RIGHT_INFO?></span>
			</span>
		</div>
		<div class="pull-right back_to_top"><a id="back-to-top" href="#" title="Back to top"><i class="fa fa-long-arrow-up p-x-sm"></i></a></div>
	</div>
</div>
<!-- Start of Script box -->
<script src="<?php echo HTTP_PATH_ADMIN?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo HTTP_PATH_ADMIN?>/js/custom-form-validation.js?time=<?php echo date('U')?>"></script>
<script src="<?php echo HTTP_PATH_ADMIN?>/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTTP_PATH_ADMIN?>/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo HTTP_PATH_ADMIN?>/js/custom-common.js?time=<?php echo date('U')?>"></script>
<script>
	
/** Script for left menu navbar **/
$(".nav > li > a").on('click', function(e){
   $(this).parent().addClass('active').siblings().removeClass('active');
    e.preventDefault();
});
	
/*Scroll to top when arrow up clicked BEGIN*/
if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}
/*Scroll to top when arrow up clicked END*/

/** Script for tooltip **/  	
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});	

</script>
<?php
	if ($CUR_PAGE_NAME == 'add-edit-apartment.php')
	{
?>
<!-- Script for time picker -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/css/datepicker3.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/js/bootstrap-datepicker.js"></script>
<script src="js/moment-with-locales.js"></script>
<script type="text/javascript">

$(function () {
	var date = new Date();
	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());	
	//$('#datePicker0').datepicker();	
   var paymentsRcd = <?php echo $paymentsRcd?>;
   for (i = 0; i < paymentsRcd; i++)
   {
        $('#datePicker'+i).datepicker({format: "dd-mm-yyyy", autoclose: true, endDate: "today"});

   }
});

</script>
<?php
	}
?>
</body>
</html>