<?php
	$SUBTITLE = 'Manage Mata Tags';
	include("includes/header.php");
	//checkPageAccessPermission($appCode);

	$keyword = getValPostORGet('keyword', 'B');
	$status = getValPostORGet('status', 'B');	

    $whereCls = "";
	if ($keyword) $whereCls .= " AND (title LIKE '%$keyword%')";
	if ($status) $whereCls .= " AND status = '$status'";

	$enckeyDBFldName = 'metaTagCode';
	$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_meta_tags', $whereCls, '', '', 'updatedOn', 'DESC');	 
	$_SESSION['SESSION_QRY_STRING_FOR_MTAG'] = "keyword=$keyword&status=$status";
?>
<!-- Start of content -->
<div class="app-body">
	<div class="padding">
		<?php include_once('includes/flash-msg.php'); ?>
        <div class="box">
			<!-- Start of header area-->
			
            <div class="box-header dker">
                <h3>Manage Mata Tags For Static Pages</h3>
					<a href="add-edit-meta-tag.php" class="view-all"><i class="material-icons">&#xe02e;</i>Add New Mata Tag</a>
            </div>
			<!-- End of header area-->
			<!-- Start of search panel -->
			<div class="search-panel">
				<form class="form-inline" method="post" action='<?php echo $CUR_PAGE_NAME?>'>
					<div class="form-group">
						<label for="keyword">Keyword</label>
						<input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo $keyword?>">						
					</div>
					<div class="form-group" style="display:none;">
						<label for="search">Status</label>
<?php
						makeDropDown('status', array_keys($STATUS), array_values($STATUS), $status, "class='selectpicker' data-size='4'", '', '');
?>	
					</div>
					<span class="button_box">
						<button type="submit" class="btn btn-sm blue_btn"><i class="fa fa-search "></i>&nbsp;Search</button>
						<button type="reset" class="btn btn-sm yellow_btn" onClick="javascript:navigate2('<?php echo $CUR_PAGE_NAME?>');"><i class="fa fa-repeat "></i>&nbsp;Reset</button>
					</span>
				</form>
			</div>
			
			<!-- End of search panel -->
			<!-- Start of table responsive -->
            <div class="table-responsive">
				<table class="table table-striped testimonial_table">
					<thead>
					<tr>
						<th class="fixedwidthforSrNo">Sr. No.</th>
						<th class="width880">Page Name</th>
						<th class="width880">Title</th>
						<th>Description</th>
						<th class="text-center fixedwidthforaction">Action</th>
					</tr>
					</thead>
					<tbody>
<?php
					if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
					{
						$numOfRows = count($rcdInfoArr);	
						for ($i = 0; $i < $numOfRows; $i++)
						{?>
							 <tr>
								<td><?=$i+1?>.</td>
								<td><?php echo $rcdInfoArr[$i]['pageAbbr']?></td>
								<td><?php echo $rcdInfoArr[$i]['title']?></td>								
								<td><div class="description_fullbox"><?php echo $rcdInfoArr[$i]['metaDescription'];?></div></td>
								<td class="text-center">								
									<!----------------------Here EDIT Form Start------------------>
									<form action="add-edit-meta-tag.php" method="post" class="form-inline">							
										<button type="submit" class="btn btn-sm btn-success"><small><i class="fa fa-pencil"></i>&nbsp;Edit</small></button>
										<input type="hidden" name="enkey" value="<?php echo $rcdInfoArr[$i][$enckeyDBFldName]?>">										
									</form>
								</td>
							 </tr>
<?php
						}
				}
				else
				{
?>
						<tr><td colspan="10"><span class="no_rdc_found_msg">No Record Found</span></td></tr>				
<?php
				}
				include_once('includes/popup/popup-model-confirmation.php');
?>	
                    </tbody>
                </table>
			</div>
            <!-- End of table responsive --> 
		</div>
	</div>
</div>
<!-- End of content -->
<!-- Start of footer -->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
<!-- Start of main content -->