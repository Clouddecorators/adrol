<!-- Start of left menu bar-->
<div id="aside" class="app-aside modal fade md nav-expand">
	<div class="left navside white dk">
		<div class="navbar navbar-md no-radius">
			<!-- brand -->
			<a class="navbar-brand" href="index.php">
				<img src="images/admin_logo.png" alt="<?php echo removeStr(ADMIN_PANEL_TITLE); ?>">
			</a>
			<!-- / brand -->
		</div>
		<div flex class="hide-scroll">
			<nav class="scroll nav-active-info">
				<ul class="nav">
					<li class='active' <?php if (in_array($CUR_PAGE_NAME, array('view-all-registered-users.php', 'website-setup.php'))) { echo "class='active'";} ?>>
						<a style="cursor:auto;">							
							<span class="nav-text"><i class="material-icons">public</i>&nbsp;Manage Website</span>
						</a>
						<ul class="nav-sub">

<?php
							if ($ACCOUNT_TYPE == 'S')
							{
?>								
								<li <?php if ($CUR_PAGE_NAME == 'view-all-pages.php') { echo "class='active'";} ?>><a href="view-all-pages.php" onclick="location.href='view-all-pages.php'"><span class="nav-text"><i class="material-icons">pages</i>&nbsp;Manage Pages</span></a></li>
								<li <?php if ($CUR_PAGE_NAME == 'website-setup.php') { echo "class='active'";} ?>><a href="website-setup.php" onclick="location.href='website-setup.php'"><span class="nav-text"><i class="material-icons">public</i>&nbsp;Edit Website Setup</span></a></li>
								<li <?php if ($CUR_PAGE_NAME == 'view-all-blog-posts.php') { echo "class='active'";} ?>><a href="view-all-blog-posts.php" onclick="location.href='view-all-blog-posts.php'"><span class="nav-text"><i class="material-icons">content_copy</i>&nbsp;Manage Blog Posts</span></a></li>
								<li <?php if ($CUR_PAGE_NAME == 'view-all-segments.php') { echo "class='active'";} ?>><a href="view-all-segments.php" onclick="location.href='view-all-segments.php'"><span class="nav-text"><i class="material-icons">content_copy</i>&nbsp;Manage Segments</span></a></li>								
								<li <?php if ($CUR_PAGE_NAME == 'view-all-news-media.php') { echo "class='active'";} ?>><a href="view-all-news-media.php" onclick="location.href='view-all-news-media.php'"><span class="nav-text"><i class="material-icons">feed</i>&nbsp;Manage News & Media</span></a></li>							
								<li <?php if ($CUR_PAGE_NAME == 'view-all-event-galleries.php') { echo "class='active'";} ?>><a href="view-all-event-galleries.php" onclick="location.href='view-all-event-galleries.php'"><span class="nav-text"><i class="material-icons">collections</i>&nbsp;Manage Gallery / Sliders</span></a></li>								
								<li <?php if ($CUR_PAGE_NAME == 'view-all-faqs.php') { echo "class='active'";} ?>><a href="view-all-faqs.php" onclick="location.href='view-all-faqs.php'"><span class="nav-text"><i class="material-icons">quiz</i>&nbsp;Manage FAQ & Information</span></a></li>	

<?php
							}
?>

						</ul>
					</li>

				</ul>
			</nav>
		</div>
	</div>
</div>
<!-- End of content -->
