<?php	
	$SUBTITLE = 'Edit Website Setup';
	include("includes/header.php");

	$arrDBFld = array('webId_PK', 'infoEmail', 'facebookUrl', 'twitterUrl', 'linkedinUrl', 'instagramUrl', 'youtubeUrl', 'address', 'headerEnquiry', 'footerTxt', 'footerEnquiry', 'appNoreplyEmail', 'appFromEmailTxt', 'appEmailSignature', 'status', 'updatedOn', 'distributors', 'happyCustomer', 'awards', 'statesCovered');	
	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_webs', '1', 0, 1, 'webId_PK', 'ASC');
	$enkey = $infoArr[0]['webId_PK'];
	if ($enkey)
	{
		$btnTxt = 'Submit';
		$postAction = 'updateAction';
		$awsomeIcon = "fa fa-plus";
		$pageTitleTxt = "Edit Website Setup Detail";

		$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_webs', array('webId_PK' => $enkey));	
		$appBgImage = $infoArr[0]['appBgImage']; 
		//$detailPageBGImage = $infoArr[0]['detailPageBGImage']; 
		
		//$backBtnURL = "view-all-apps.php?".@$_SESSION['SESSION_QRY_STRING'];
		$idDisabled = 'readonly';
		if ($_SESSION['userDetails']['accountType'] != 'S')  $idDisabled = 'readonly';
	}
	else
	{
		$btnTxt = 'Submit';
		$postAction = 'addAction';
		$awsomeIcon = "fa fa-plus";
		$pageTitleTxt = "Add New App";
	
		foreach ($arrDBFld AS $dbFldName)
		{
			$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
			unset($_SESSION['session_'.$dbFldName]);
		}
		
		//$backBtnURL = "view-all-apps.php".$_SESSION['SESSION_QRY_STRING'];
	}
	$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;

	$valParamArray = array(
					"infoEmail" => 
					array(
						"type" => "text",
						"msg" => "Info Email"	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "255 char."),
					),
					"facebookUrl" => 
					array(
						"type" => "text",
						"msg" => "Facebook Url"	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "255 char."),
					),
					"twitterUrl" => 
					array(
						"type" => "text",
						"msg" => "Twitter Url"	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "255 char."),
					),
					"linkedinUrl" => 
					array(
						"type" => "text",
						"msg" => "Linkedin Url"	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "255 char."),
					),
					"instagramUrl" => 
					array(
						"type" => "text",
						"msg" => "Instagram Url"	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "255 char."),
					),
					"youtubeUrl" => 
					array(
						"type" => "text",
						"msg" => "Youtube Url"	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "255 char."),
					),
					"headerEnquiry" => 
					array(
						"type" => "text",
						"msg" => "Header Enquiry No."	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "50 char."),
					),
					"footerEnquiry" => 
					array(
						"type" => "text",
						"msg" => "Footer Enquiry No."	,
						"min" => array("length" => 1, "msg" => "1 char."),
						"max" => array("length" => 255, "msg" => "50 char."),
					),
				);

	$_SESSION['formValidation'] = $valParamArray;
?>
<!-- Start of content -->
<div class="app-body" >	
      <div class="padding">
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>
        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker" style="display:none1;">
                <h3><?=$pageTitleTxt?></h3>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-app-form" enctype="multipart/form-data" method="post" action="controller/website-setup-controller.php" onSubmit='return validation(1, <?php echo json_encode($valParamArray); ?>);'>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Info Email:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control " type="text" name="infoEmail" id="infoEmail" maxlength="255" value="<?=$infoArr[0]['infoEmail']?>">
							<span id='span_infoEmail' class='form_error'><?php showErrorMessage('infoEmail'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Address</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="address" id="address" rows="2" cols="5"><?=$infoArr[0]['address']?></textarea>
							<span id='span_address' class='form_error'><?php showErrorMessage('address'); ?></span>							
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Footer Text</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="footerTxt" id="footerTxt" rows="3" cols="5"><?=$infoArr[0]['footerTxt']?></textarea>
							<span id='span_footerTxt' class='form_error'><?php showErrorMessage('footerTxt'); ?></span>							
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Facebook Url:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="facebookUrl" id="facebookUrl" maxlength="255" value="<?=$infoArr[0]['facebookUrl']?>">
							<span id='span_facebookUrl' class='form_error'><?php showErrorMessage('facebookUrl'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Twitter Url:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="twitterUrl" id="twitterUrl" maxlength="255" value="<?=$infoArr[0]['twitterUrl']?>">
							<span id='span_twitterUrl' class='form_error'><?php showErrorMessage('twitterUrl'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Instagram Url:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="instagramUrl" id="instagramUrl" maxlength="255" value="<?=$infoArr[0]['instagramUrl']?>">
							<span id='span_instagramUrl' class='form_error'><?php showErrorMessage('instagramUrl'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Linkedin Url:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="linkedinUrl" id="linkedinUrl" maxlength="255" value="<?=$infoArr[0]['linkedinUrl']?>">
							<span id='span_linkedinUrl' class='form_error'><?php showErrorMessage('linkedinUrl'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Youtube Url:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="youtubeUrl" id="youtubeUrl" maxlength="255" value="<?=$infoArr[0]['youtubeUrl']?>">
							<span id='span_youtubeUrl' class='form_error'><?php showErrorMessage('youtubeUrl'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Header Enquiry No:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="headerEnquiry" id="headerEnquiry" maxlength="50" value="<?=$infoArr[0]['headerEnquiry']?>">
							<span id='span_headerEnquiry' class='form_error'><?php showErrorMessage('headerEnquiry'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Footer Enquiry No:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="footerEnquiry" id="footerEnquiry" maxlength="50" value="<?=$infoArr[0]['footerEnquiry']?>">
							<span id='span_footerEnquiry' class='form_error'><?php showErrorMessage('footerEnquiry'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Total Distributors:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="number" name="distributors" id="distributors" maxlength="15" value="<?=$infoArr[0]['distributors']?>">
							<span id='span_distributors' class='form_error'><?php showErrorMessage('distributors'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Total Happy Customer:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="number" name="happyCustomer" id="happyCustomer" maxlength="15" value="<?=$infoArr[0]['happyCustomer']?>">
							<span id='span_happyCustomer' class='form_error'><?php showErrorMessage('happyCustomer'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Total Awards:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="number" name="awards" id="awards" maxlength="15" value="<?=$infoArr[0]['awards']?>">
							<span id='span_awards' class='form_error'><?php showErrorMessage('awards'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Total States Covered:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="number" name="statesCovered" id="statesCovered" maxlength="15" value="<?=$infoArr[0]['statesCovered']?>">
							<span id='span_statesCovered' class='form_error'><?php showErrorMessage('statesCovered'); ?></span>
						</div>
					</div>
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if (!$enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}

?>

							<input type="hidden" name="postAction" value="<?=$postAction?>">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
