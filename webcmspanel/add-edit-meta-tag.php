<?php	
$SUBTITLE = 'Manage Mata Tag';
include("includes/header.php");	

$enkey = getValPostORGet('enkey', 'B');
$arrDBFld = array('title', 'metaTagCode', 'metaDescription', 'metaKeywords', 'pageAbbr');	
$enckeyDBFldName = 'metaTagCode';
$backBtnURL = "view-all-meta-tags.php?".$_SESSION['SESSION_QRY_STRING_FOR_MTAG'];

if ($enkey)
{
	$btnTxt = 'Submit';
	$postAction = 'updateAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Edit Mata Tag Detail";

	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_meta_tags', array($enckeyDBFldName => $enkey), '', '', 'updatedOn', 'DESC');		
}
else
{
	$btnTxt = 'Submit';
	$postAction = 'addAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Add New Mata Tag";

	foreach ($arrDBFld AS $dbFldName)
	{
		$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
		unset($_SESSION['session_'.$dbFldName]);
	}
	$bannerImg = '';		
}

$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;
$arrParamForValidation = array("title" => array("type" => "text", "msg" => "Title"),
 'metaDescription' => array("type" => "textarea", "msg" => "Meta Description"),
 'metaKeywords' => array("type" => "textarea", "msg" => "Meta Keywords"),
 'pageAbbr' => array("type" => "text", "msg" => "Page Name"),
);

$_SESSION['formValidation'] = $arrParamForValidation;
?>
<!-- Start of content -->
<div class="app-body" >
      <div class="padding">
	  <script src="<?php echo HTTP_PATH?>/js/tinymce/js/tinymce/tinymce.min.js"></script>
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>

        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker">
                <h3><?=$pageTitleTxt?></h3><a href="<?php echo $backBtnURL?>" class="view-all"><i class="material-icons">&#xe02f;</i>View All Mata Tags</a>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-plan-form" enctype="multipart/form-data" method="post" action="controller/meta-tag-controller.php" onSubmit='return validation(1, <?php echo json_encode($arrParamForValidation); ?>);'>
                <div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Page Name:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="pageAbbr" id="pageAbbr" maxlength="500" value="<?=$infoArr[0]['pageAbbr']?>">
							<span id='span_pageAbbr' class='form_error'><?php showErrorMessage('pageAbbr'); ?></span>
						</div>
				</div>					
                <div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Page Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="title" id="title" maxlength="500" value="<?=$infoArr[0]['title']?>">
							<span id='span_title' class='form_error'><?php showErrorMessage('title'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Meta Description:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="metaDescription" id="metaDescription" rows="8" cols="50"><?=$infoArr[0]['metaDescription']?></textarea>
							<span id='span_metaDescription' class='form_error'><?php showErrorMessage('metaDescription'); ?></span>
							<script>
								tinymce.init({
									selector:'#metaDescription',
									 height: 350,
									 menubar:false,										 
									 plugins: ["code lists advlist preview"],
									 forced_root_block : '', // Needed for 3.x
									 readonly :0,
									 toolbar: 'code | undo redo | styleselect | bold | italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent',										
								});
							</script>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Meta Keywords:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="metaKeywords" id="metaKeywords" rows="8" cols="50"><?=$infoArr[0]['metaDescription']?></textarea>
							<span id='span_metaKeywords' class='form_error'><?php showErrorMessage('metaKeywords'); ?></span>
							<script>
								tinymce.init({
									selector:'#metaKeywords',
									 height: 350,
									 menubar:false,										 
									 plugins: ["code lists advlist preview"],
									 forced_root_block : '', // Needed for 3.x
									 readonly :0,
									 toolbar: 'code | undo redo | styleselect | bold | italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent',										
								});
							</script>
						</div>
					</div>                    
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if ($enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}
?>
						    <input type="hidden" name="postAction" value="<?=$postAction?>">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<script>
/** Upload image button **/
document.getElementById("uploadBtn").onchange = function ()
{
    document.getElementById("uploadFile").value = this.value.replace(/^C:\\\\/i, '');
};
</script>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
