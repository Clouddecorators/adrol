<?php	
$SUBTITLE = 'Manage Pages';
include("includes/header.php");	

$enkey = getValPostORGet('enkey', 'B');
$arrDBFld = array('pageCode', 'title', 'pageTxt', 'pageImg', 'bannerImg','bannerMobViewImg', 'bannerTabViewImg', 'description','status');	
$enckeyDBFldName = 'pageCode';
$backBtnURL = "view-all-pages.php?".$_SESSION['SESSION_QRY_STRING_FOR_PAGE'];

if ($enkey)
{
	$btnTxt = 'Submit';
	$postAction = 'updateAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Edit Page Detail";

	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_pages', array($enckeyDBFldName => $enkey));
	$bannerImg = $infoArr[0]['bannerImg'];
	$bannerMobViewImg = $infoArr[0]['bannerMobViewImg'];
	$bannerTabViewImg = $infoArr[0]['bannerTabViewImg'];
	$pageImg = $infoArr[0]['pageImg'];

	$pageInfoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_pages', array('pageCode' => $infoArr[0]['pageCode']), '', '', '', 'ASC');
}
else
{
	$btnTxt = 'Submit';
	$postAction = 'addAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Add New Page";

	foreach ($arrDBFld AS $dbFldName)
	{
		$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
		unset($_SESSION['session_'.$dbFldName]);
	}
	$pageImg = '';		
	$bannerImg = '';		
	$bannerMobViewImg = '';		
	$bannerTabViewImg = '';		
}

$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;

$arrParamForValidation = array("title" => array("type" => "text", "msg" => "Title"), 'description' => array("type" => "textarea", "msg" => "Description"));

$_SESSION['formValidation'] = $arrParamForValidation;
?>
<!-- Start of content -->
<div class="app-body" >
      <div class="padding">
	  <script src="<?php echo HTTP_PATH?>/js/tinymce/js/tinymce/tinymce.min.js"></script>
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>

        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker">
                <h3><?=$pageTitleTxt?></h3><a href="<?php echo $backBtnURL?>" class="view-all"><i class="material-icons">&#xe02f;</i>View All Pages</a>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-plan-form" enctype="multipart/form-data" method="post" action="controller/page-controller.php" onSubmit='return validation(1, <?php echo json_encode($arrParamForValidation); ?>);'>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="title" id="title" maxlength="255" value="<?=$infoArr[0]['title']?>">
							<span id='span_title' class='form_error'><?php showErrorMessage('title'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Sub Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="pageTxt" id="pageTxt" maxlength="255" value="<?=$infoArr[0]['pageTxt']?>">
							<span id='span_pageTxt' class='form_error'><?php showErrorMessage('pageTxt'); ?></span>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Description:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="description" id="description" rows="8" cols="50"><?=$infoArr[0]['description']?></textarea>
							<span id='span_description' class='form_error'><?php showErrorMessage('description'); ?></span>
							<script>
								tinymce.init({
									selector:'#description',
									 height: 350,
									 menubar:false,										 
									 plugins: ["code lists advlist preview"],
									 forced_root_block : '', // Needed for 3.x
									 readonly :0,
									 toolbar: 'code | undo redo | styleselect | bold | italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent',										
								});
							</script>
						</div>
					</div>
					<!-- Start of banner -->
					<div class="form-group row">
						<label for="photo_file" class="col-sm-12 col-md-2 form-control-label">Home Page Poster:</label>
						<div class="col-sm-12 col-md-10">
							<!-- Start of photo box -->
<?php
							if ($bannerImg != '')
							{
?>
								<div class="photo_box">
									<div class="banner_img_box">
										<img src="<?=HTTP_PATH_ASSET.'/uploads/page_images/'.$bannerImg?>" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo HTTP_PATH_ASSET?>/images/default_page_banner.jpg';">
									</div>
								</div>
<?php
							}
?>
							<!-- End of photo box -->
							<div id="undo" class="col-sm-4 p-a-xs" style="display:none"><a> Undo Delete</a></div>
							<!-- Start of upload file -->
							<input id="photo_delete" name="photo_delete" type="hidden" value="0" class="has-value">
							<input id="uploadFile" placeholder="Choose File" class="form-control input_smallbox" type="text" id="photo_filess">
							<div class="fileUpload btn btn-sm btn-success">
								<span><i class="fa fa-upload"></i> Upload</span>
								<input id="uploadBtn" type="file" name="bannerImg" class="upload form-control">
							</div>
							<!-- End of upload file -->
							<small class="text-muted text-image"><i class="fa fa-question-circle-o"></i>&nbsp;Extensions: png, jpg or jpeg, Dimension: (1920x1080)</small>
						 </div>
					</div>
					<!-- Start of banner -->
					<div class="form-group row">
						<label for="photo_file" class="col-sm-12 col-md-2 form-control-label">Home Page Mob View Poster:</label>
						<div class="col-sm-12 col-md-10">
							<!-- Start of photo box -->
<?php
							if ($bannerMobViewImg != '')
							{
?>
								<div class="photo_box">
									<div class="banner_img_box">
										<img src="<?=HTTP_PATH_ASSET.'/uploads/page_images/'.$bannerMobViewImg?>" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo HTTP_PATH_ASSET?>/images/default_page_banner.jpg';">
									</div>
								</div>
<?php
							}
?>
							<!-- End of photo box -->
							<div id="undo" class="col-sm-4 p-a-xs" style="display:none"><a> Undo Delete</a></div>
							<!-- Start of upload file -->
							<input id="photo_delete" name="photo_delete" type="hidden" value="0" class="has-value">
							<input id="uploadFile3" placeholder="Choose File" class="form-control input_smallbox" type="text" id="photo_filess">
							<div class="fileUpload btn btn-sm btn-success">
								<span><i class="fa fa-upload"></i> Upload</span>
								<input id="uploadBtn3" type="file" name="bannerMobViewImg" class="upload form-control">
							</div>
							<!-- End of upload file -->
							<small class="text-muted text-image"><i class="fa fa-question-circle-o"></i>&nbsp;Extensions: png, jpg or jpeg, Dimension: (375 x 812)</small>
						 </div>
					</div>
					<!-- Start of banner -->
					<div class="form-group row">
						<label for="photo_file" class="col-sm-12 col-md-2 form-control-label">Home Page Tab View Poster:</label>
						<div class="col-sm-12 col-md-10">
							<!-- Start of photo box -->
<?php
							if ($bannerTabViewImg != '')
							{
?>
								<div class="photo_box">
									<div class="banner_img_box">
										<img src="<?=HTTP_PATH_ASSET.'/uploads/page_images/'.$bannerTabViewImg?>" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo HTTP_PATH_ASSET?>/images/default_page_banner.jpg';">
									</div>
								</div>
<?php
							}
?>
							<!-- End of photo box -->
							<div id="undo" class="col-sm-4 p-a-xs" style="display:none"><a> Undo Delete</a></div>
							<!-- Start of upload file -->
							<input id="photo_delete" name="photo_delete" type="hidden" value="0" class="has-value">
							<input id="uploadFile4" placeholder="Choose File" class="form-control input_smallbox" type="text" id="photo_filess">
							<div class="fileUpload btn btn-sm btn-success">
								<span><i class="fa fa-upload"></i> Upload</span>
								<input id="uploadBtn4" type="file" name="bannerTabViewImg" class="upload form-control">
							</div>
							<!-- End of upload file -->
							<small class="text-muted text-image"><i class="fa fa-question-circle-o"></i>&nbsp;Extensions: png, jpg or jpeg, Dimension: (768x 1024)</small>
						 </div>
					</div>
					<!-- Start of banner -->
					<div class="form-group row">
						<label for="photo_file" class="col-sm-12 col-md-2 form-control-label">Main Page Poster:</label>
						<div class="col-sm-12 col-md-10">
							<!-- Start of photo box -->
<?php
							if ($pageImg != '')
							{
?>
								<div class="photo_box">
									<div class="banner_img_box">
										<img src="<?=HTTP_PATH_ASSET.'/uploads/page_images/'.$pageImg?>" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo HTTP_PATH_ASSET?>/images/default_page_banner.jpg';">
									</div>
								</div>
<?php
							}
?>
							<!-- End of photo box -->
							<div id="undo" class="col-sm-4 p-a-xs" style="display:none"><a> Undo Delete</a></div>
							<!-- Start of upload file -->
							<input id="photo_delete" name="photo_delete" type="hidden" value="0" class="has-value">
							<input id="uploadFile2" placeholder="Choose File" class="form-control input_smallbox" type="text" id="photo_filess">
							<div class="fileUpload btn btn-sm btn-success">
								<span><i class="fa fa-upload"></i> Upload</span>
								<input id="uploadBtn2" type="file" name="pageImg" class="upload form-control">
							</div>
							<!-- End of upload file -->
							<small class="text-muted text-image"><i class="fa fa-question-circle-o"></i>&nbsp;Extensions: png, jpg or jpeg, Dimension: (500x500)</small>
						 </div>
					</div>
					<div class="form-group row" style="display: none;">
						<label class="col-sm-12 col-md-2 form-control-label">Order:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="pageOrder" id="pageOrder" maxlength="255" value="<?=$infoArr[0]['pageOrder']?>">
							<span id='span_pageOrder' class='form_error'><?php showErrorMessage('pageOrder'); ?></span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Status:</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('status', array_keys($STATUS), array_values($STATUS), $infoArr[0]['status'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if ($enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}
?>
						    <input type="hidden" name="postAction" value="<?=$postAction?>">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<script>
/** Upload image button **/
document.getElementById("uploadBtn").onchange = function ()
{
    document.getElementById("uploadFile").value = this.value.replace(/^C:\\\\/i, '');
};
/** Upload image button **/
document.getElementById("uploadBtn2").onchange = function ()
{
    document.getElementById("uploadFile2").value = this.value.replace(/^C:\\\\/i, '');
};
document.getElementById("uploadBtn3").onchange = function ()
{
    document.getElementById("uploadFile3").value = this.value.replace(/^C:\\\\/i, '');
};
document.getElementById("uploadBtn4").onchange = function ()
{
    document.getElementById("uploadFile4").value = this.value.replace(/^C:\\\\/i, '');
};
</script>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
