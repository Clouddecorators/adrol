<?php	
$SUBTITLE = 'Manage Gallery & Sliders';
include("includes/header.php");	

$enkey = getValPostORGet('enkey', 'B');
$enckeyDBFldName = 'imgCode';
$arrDBFld = array($enckeyDBFldName, 'title', 'videoUrl', 'img','imgType','status');	

$backBtnURL = "view-all-event-galleries.php?".$_SESSION['SESSION_QRY_STRING_FOR_IMG'];

if ($enkey)
{
	$btnTxt = 'Submit';
	$postAction = 'updateAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Edit Video/Image Gallery, Testimonial & Slider Detail";

	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_imgs', array($enckeyDBFldName => $enkey));
	$img = $infoArr[0]['img'];	
}
else
{
	$btnTxt = 'Submit';
	$postAction = 'addAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Add New Video/Image Gallery, Testimonial & Slider";

	foreach ($arrDBFld AS $dbFldName)
	{
		$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
		unset($_SESSION['session_'.$dbFldName]);
	}
	$img = '';		
}

$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;

$arrParamForValidation = array("title" => array("type" => "text", "msg" => "Title"));

$_SESSION['formValidation'] = $arrParamForValidation;
?>
<!-- Start of content -->
<div class="app-body" >
      <div class="padding">
	  <script src="<?php echo HTTP_PATH?>/js/tinymce/js/tinymce/tinymce.min.js"></script>
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>

        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker">
                <h3><?=$pageTitleTxt?></h3><a href="<?php echo $backBtnURL?>" class="view-all"><i class="material-icons">&#xe02f;</i>View All Gallery & Sliders</a>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-plan-form" enctype="multipart/form-data" method="post" action="controller/event-gallery-controller.php" onSubmit='return validation(1, <?php echo json_encode($arrParamForValidation); ?>);'>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="title" id="title" maxlength="255" value="<?=$infoArr[0]['title']?>">
							<span id='span_title' class='form_error'><?php showErrorMessage('title'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Youtube URL:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="videoUrl" id="videoUrl" maxlength="500" value="<?=$infoArr[0]['videoUrl']?>">
							<span id='span_videoUrl' class='form_error'><?php showErrorMessage('videoUrl'); ?></span>
						</div>
					</div>					
					<!-- Start of banner -->
					<div class="form-group row">
						<label for="photo_file" class="col-sm-12 col-md-2 form-control-label">Poster:</label>
						<div class="col-sm-12 col-md-10">
							<!-- Start of photo box -->
<?php
							if ($img != '')
							{
?>
								<div class="photo_box">
									<div class="banner_img_box bannerfullbox">
										<img src="<?=HTTP_PATH_ASSET_UPLOAD.'/imgs/'.$img?>" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo HTTP_PATH_ADMIN?>/images/default_poster.jpg';">
									</div>
								</div>
<?php
							}
?>
							<!-- End of photo box -->
							<div id="undo" class="col-sm-4 p-a-xs" style="display:none"><a> Undo Delete</a></div>
							<!-- Start of upload file -->
							<input id="photo_delete" name="photo_delete" type="hidden" value="0" class="has-value">
							<input id="uploadFile" placeholder="Choose File" class="form-control input_smallbox" type="text" id="photo_filess">
							<div class="fileUpload btn btn-sm btn-success">
								<span><i class="fa fa-upload"></i> Upload</span>
								<input id="uploadBtn" type="file" name="img" class="upload form-control">
							</div>
							<!-- End of upload file -->
							<small class="text-muted text-image"><i class="fa fa-question-circle-o"></i>&nbsp;Extensions: png, jpg or jpeg, Dimension: (1920x330)</small>
						 </div>
					</div>
					<div class="form-group row" style="display: none;">
						<label class="col-sm-12 col-md-2 form-control-label">Order:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="imgOrder" id="imgOrder" maxlength="6" value="<?=$infoArr[0]['imgOrder']?>">
							<span id='span_imgOrder' class='form_error'><?php showErrorMessage('imgOrder'); ?></span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Type:</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('imgType', array_keys($ARR_IMG_TYPES), array_values($ARR_IMG_TYPES), $infoArr[0]['imgType'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>					
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Status:</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('status', array_keys($STATUS), array_values($STATUS), $infoArr[0]['status'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if ($enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}
?>
						    <input type="hidden" name="postAction" value="<?=$postAction?>">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<script>
/** Upload image button **/
document.getElementById("uploadBtn").onchange = function ()
{
    document.getElementById("uploadFile").value = this.value.replace(/^C:\\\\/i, '');
};
</script>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
