<?php	
$SUBTITLE = 'Manage Categories';
include("includes/header.php");	

$enkey = getValPostORGet('enkey', 'B');
$arrDBFld = array('catCode', 'catName','isShowInFooter','menuCode_RK', 'status');	
$backBtnURL = "view-all-categories.php?".$_SESSION['SESSION_QRY_STRING_FOR_CAT'];

if ($enkey)
{
	$btnTxt = 'Submit';
	$postAction = 'updateAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Edit Category Detail";

	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_categories', array('catCode' => $enkey));
}
else
{
	$btnTxt = 'Submit';
	$postAction = 'addAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Add New Category";

	foreach ($arrDBFld AS $dbFldName)
	{
		$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
		unset($_SESSION['session_'.$dbFldName]);
	}		
}

$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;

$arrParamForValidation = array("catName" => array("type" => "text", "msg" => "Category Name"), "menuCode_RK" =>array("type" => "dropDown", "msg" => "Menu Name"));

$_SESSION['formValidation'] = $arrParamForValidation;
?>
<!-- Start of content -->
<div class="app-body" >
	<script src="<?php echo HTTP_PATH?>/js/tinymce/js/tinymce/tinymce.min.js"></script>
      <div class="padding">
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>

        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker">
                <h3><?=$pageTitleTxt?></h3><a href="<?php echo $backBtnURL?>" class="view-all"><i class="material-icons">&#xe02f;</i>View All Categories</a>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-menu-form" enctype="multipart/form-data" method="post" action="controller/category-controller.php" onSubmit='return validation(1, <?php echo json_encode($arrParamForValidation); ?>);'>
				<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Menu Name:</label>
						<div class="col-sm-12 col-md-10">
<?php			
						$menusInfoArr = $objDBQuery->getRecord(0, array('menuCode', 'menuName'), 'tbl_menus', array('isDeleted' => 'N'), '', '', 'menuOrder ASC, createdOn', 'ASC');
						makeDropDownFromDB('menuCode_RK', $menusInfoArr, 'menuCode', 'menuName', $infoArr[0]['menuCode_RK'], "class='selectpicker' data-size='5'", '', '');
?>	
						<span id='span_menuCode_RK' class='form_error'><?php showErrorMessage('menuCode_RK'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Category Name:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="catName" id="catName" maxlength="255" value="<?=$infoArr[0]['catName']?>">
							<span id='span_catName' class='form_error'><?php showErrorMessage('catName'); ?></span>
						</div>
					</div>
					<div class="form-group row" >
						<label class="col-sm-12 col-md-2 form-control-label">Is Show in Footer?</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('isShowInFooter', array_keys($DEFAULT_DROPDOWN_SELECTION), array_values($DEFAULT_DROPDOWN_SELECTION), $infoArr[0]['isShowInFooter'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>					
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Status:</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('status', array_keys($STATUS), array_values($STATUS), $infoArr[0]['status'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if ($enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}
?>
						    <input type="hidden" name="postAction" value="<?=$postAction?>">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
