<?php	
	$SUBTITLE = 'User Profile';
	include("includes/header.php");
?>
<!-- End of header -->

<!-- Start of content -->
<div class="app-body" >
<?php
	$userInfoArr = $objDBQuery->getRecord(0, array('userCode', 'email', 'fullName', 'phone'), 'tbl_users', array('userCode' => $_SESSION['userDetails']['userCode']));
	$valParamArray = array(
					"fullName" => array(
						"type" => "text",
						"msg" => "Name"	
					),						
					"phone" =>array(
						"type" => "text",
						"msg" => "Phone",
						"regex" => array("pattern" => PHONE_VALIADTION_REGEX, "msg" => PHONE_VALIADTION_MSG),
					),				
				);

	$_SESSION['formValidation'] = $valParamArray;
?>
	
	<div class="padding">
		<?php include_once('includes/flash-msg.php'); ?>
		<?php include_once('includes/profile-setting-menu.php'); ?>
			<!-- Start of tab content -->
			<div class="tab-content clear b-t">
			<div class="tab-pane active" id="user_profile">
				<div class="box-body">
						<form name="update_your_profule-form" method="post" action="controller/member-controller.php" onSubmit='return validation(1, <?php echo json_encode($valParamArray); ?>);'>
						<div class="form-group row">
							<label for="title_ar" class="col-md-2 form-control-label">Email Id:<span class="cla_star">*</span></label>
							<div class="col-md-10">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="material-icons nav_icon">mail_outline</i>
									</span>
								</div>
								<input class="form-control" type="text" disabled value="<?php echo $userInfoArr[0]['email']?>">	
							</div>								
							</div>							
						</div>
						<div class="form-group row">
							<label for="fullName" class="col-md-2 form-control-label">Name:<span class="cla_star">*</span></label>
							<div class="col-md-10">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons nav_icon">person</i>
										</span>
									</div>
									<input class="form-control" type="text" name='fullName' id='fullName' maxlength="100" value="<?php echo $userInfoArr[0]['fullName']?>">
								</div>
								<span id='span_fullName' class='form_error'><?php showErrorMessage('fullName'); ?></span>
							</div>							
						</div>																													
						<div class="form-group row">
							<label for="fullName" class="col-md-2 form-control-label">Phone:<span class="cla_star">*</span></label>
							<div class="col-md-10">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons nav_icon">phone</i>
										</span>
									</div>
									<input class="form-control" type="text" name='phone' id='phone' maxlength="20" value="<?php echo $userInfoArr[0]['phone']?>">
									</div>
								<span id='span_phone' class='form_error'><?php showErrorMessage('phone'); ?></span>
							</div>							
						</div>							
														
						<div class="form-groups row">
							<div class="col-md-offset-2 col-md-10 btn_space_web">
								<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>								
								<input type="hidden" name="postAction" value="updateYourProfile">					
								<input type="hidden" name="userCode" value="<?php echo $userInfoArr[0]['userCode']?>">					
								<input type="hidden" id="formToken" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
							</div>
						</div>
						</div>			
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- End of content -->
<!-- Start of footer -->
<?php 
	include("includes/footer.php");
?>
<!-- End of footer-->
</div>
<!-- Start of main content -->