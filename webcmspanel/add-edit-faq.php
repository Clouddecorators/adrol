<?php	
$SUBTITLE = 'Manage FAQ & Information ';
include("includes/header.php");	

$enkey = getValPostORGet('enkey', 'B');
$arrDBFld = array('faqCode', 'title', 'description','status');	
$enckeyDBFldName = 'faqCode';
$backBtnURL = "view-all-faqs.php?".$_SESSION['SESSION_QRY_STRING_FOR_FAQ'];

if ($enkey)
{
	$btnTxt = 'Submit';
	$postAction = 'updateAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Edit FAQ Detail";

	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_faqs', array($enckeyDBFldName => $enkey), '', '', 'createdOn', 'DESC');		
}
else
{
	$btnTxt = 'Submit';
	$postAction = 'addAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Add New FAQ";

	foreach ($arrDBFld AS $dbFldName)
	{
		$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
		unset($_SESSION['session_'.$dbFldName]);
	}
	$bannerImg = '';		
}

$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;

$arrParamForValidation = array("title" => array("type" => "text", "msg" => "Title"), 'description' => array("type" => "textarea", "msg" => "Description"));

$_SESSION['formValidation'] = $arrParamForValidation;
?>
<!-- Start of content -->
<div class="app-body" >
      <div class="padding">
	  <script src="<?php echo HTTP_PATH?>/js/tinymce/js/tinymce/tinymce.min.js"></script>
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>

        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker">
                <h3><?=$pageTitleTxt?></h3><a href="<?php echo $backBtnURL?>" class="view-all"><i class="material-icons">&#xe02f;</i>View All FAQS</a>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-plan-form" enctype="multipart/form-data" method="post" action="controller/faq-controller.php" onSubmit='return validation(1, <?php echo json_encode($arrParamForValidation); ?>);'>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="title" id="title" maxlength="500" value="<?=$infoArr[0]['title']?>">
							<span id='span_title' class='form_error'><?php showErrorMessage('title'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Description:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="description" id="description" rows="8" cols="50"><?=$infoArr[0]['description']?></textarea>
							<span id='span_description' class='form_error'><?php showErrorMessage('description'); ?></span>
							<script>
								tinymce.init({
									selector:'#description',
									 height: 350,
									 menubar:false,										 
									 plugins: ["code lists advlist preview"],
									 forced_root_block : '', // Needed for 3.x
									 readonly :0,
									 toolbar: 'code | undo redo | styleselect | bold | italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent',										
								});
							</script>
						</div>
					</div>
					<div class="form-group row" style="display: none;">
						<label class="col-sm-12 col-md-2 form-control-label">Order:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="pageOrder" id="pageOrder" maxlength="255" value="<?=$infoArr[0]['pageOrder']?>">
							<span id='span_pageOrder' class='form_error'><?php showErrorMessage('pageOrder'); ?></span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Status:</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('status', array_keys($STATUS), array_values($STATUS), $infoArr[0]['status'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if ($enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}
?>
						    <input type="hidden" name="postAction" value="<?=$postAction?>">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<script>
/** Upload image button **/
document.getElementById("uploadBtn").onchange = function ()
{
    document.getElementById("uploadFile").value = this.value.replace(/^C:\\\\/i, '');
};
</script>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
