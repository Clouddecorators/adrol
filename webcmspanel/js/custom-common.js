function setPopupContent(frmId, title, msg)
{	
	// Here set content to popup
	$("#confirmation_modal_title").html(title);	
	$("#confirmation_modal_msg").html(msg);
	//$("#popBtnTxtDlteReset").html($('#popupBtnTxt'+cntVal).val());	
	$("#confirmation_modal_frmId").val(frmId);	
}

function frmSubmit(frmId)
{
	document.getElementById(frmId).submit(); 
}

function popupSubmtBtnAction()
{
	var frmId = $("#confirmation_modal_frmId").val();
	frmSubmit(frmId);	
}

function resetFormErrorMsg()
{	
	$(".form_error").text('');

}

function resetFormValErrorMsg(frmId)
{	
	document.getElementById(frmId).reset();
	$(".form_error").text('');	
}

function copyVal(fldVal, copy2Id)
{
	//alert(fldVal)
	$("#"+copy2Id).val(fldVal);
}

function copyValmore2Input(fldVal, fld1, fld2)
{
	//alert(fldVal)
	$("#"+fld1).val(fldVal);
	$("#"+fld2).val(fldVal);
}

function errorMsgSet(strVal)
{	
	if (strVal != '')
    {
        var strObj = JSON.parse(strVal);
        $.each(strObj, function(keys, values)
        {	
            $('#span_' + keys).text(values);
        });
    }
}

function isJson(str)
{
    try 
    {
        JSON.parse(str);
    }
    catch (e) 
     {
        return false;
    }
    return true;
}

function saveTinyMCETxt()
{
    tinyMCE.triggerSave();  
}

function navigate2(pageLink)
{	
	window.location.href = pageLink;
}

function setDropDownValue(curId, dropDownIdTo, caseName, requestURl)
{	
	var uid = $('#'+curId).val();	
	var dropDownIdTo = dropDownIdTo;
	$.ajax(
	{
		url: requestURl+"?uid="+uid+"&caseName="+caseName+"&reFrm=ad",
	})
	.done(function(data)
	{			
		$("#"+dropDownIdTo).html(data);	
		$('.selectpicker').selectpicker('refresh');
	});
}
