<?php
	$SUBTITLE = 'Manage Segments';
	include("includes/header.php");	

	$keyword = getValPostORGet('keyword', 'B');
	$status = getValPostORGet('status', 'B');
	
	$whereCls = "isDeleted = 'N'";
	if ($keyword) $whereCls .= " AND (postTitle LIKE '%$keyword%')";
	if ($status) $whereCls .= " AND status = '$status'";	

	$enckeyDBFldName = 'segmentCode';
	$rcdInfoArr = $objDBQuery->getRecord(0, array('*'), 'tbl_segments', $whereCls, '', '', 'createdOn', 'DESC');	 
	$_SESSION['SESSION_QRY_STRING_FOR_SEGMENT'] = "keyword=$keyword&status=$status";
?>
<!-- Start of content -->
<div class="app-body">
	<div class="padding">
		<?php include_once('includes/flash-msg.php'); ?>
        <div class="box">
			<!-- Start of header area-->
			
            <div class="box-header dker">
                <h3>Manage Segments</h3>
					<a href="add-edit-segment.php" class="view-all"><i class="material-icons">&#xe02e;</i>Add New Segment</a>
            </div>
			<!-- End of header area-->
			<!-- Start of search panel -->
			<div class="search-panel">
				<form class="form-inline" method="post" action='<?php echo $CUR_PAGE_NAME?>'>
					<div class="form-group">
						<label for="keyword">Keyword</label>
						<input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo $keyword?>">						
					</div>
					<div class="form-group">
						<label for="search">Status</label>
<?php
						makeDropDown('status', array_keys($STATUS), array_values($STATUS), $status, "class='selectpicker' data-size='4'", '', '');
?>	
					</div>					
					<span class="button_box">
						<button type="submit" class="btn btn-sm blue_btn"><i class="fa fa-search "></i>&nbsp;Search</button>
						<button type="reset" class="btn btn-sm yellow_btn" onClick="javascript:navigate2('<?php echo $CUR_PAGE_NAME?>');"><i class="fa fa-repeat "></i>&nbsp;Reset</button>
					</span>
				</form>
			</div>
			
			<!-- End of search panel -->
			<!-- Start of table responsive -->
            <div class="table-responsive">
				<table class="table table-striped testimonial_table">
					<thead>
					<tr>
						<th class="fixedwidthforSrNo">Sr. No.</th>
						<th class="width880">Title</th>
						<th class="client_img_center">Poster</th>
						<th class="text-center" style="display: none;">Order</th>
						<th class="text-center">Status</th>
						<th class="text-center fixedwidthforaction">Action</th>
					</tr>
					</thead>
					<tbody>
<?php
					if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
					{
						$numOfRows = count($rcdInfoArr);	
						for ($i = 0; $i < $numOfRows; $i++)
						{
							$status = $rcdInfoArr[$i]['status'];
							$statusTxt = $STATUS[$status];
							if ($status == 'A') $statusCls = 'text-success';
							else $statusCls = 'text-danger';

							$frmId4ActionDelete = "deleteFrmId_".$i;
							$frmId4ActionStatus = "statusFrmId_".$i;

							$bannerImg = $rcdInfoArr[$i]['postThumb'];
							if ($bannerImg !='') $bannerImgPath = HTTP_PATH_ASSET_UPLOAD."/segment_imgs/".$bannerImg;
							else $bannerImgPath = HTTP_PATH."/images/default_poster.jpg";
?>
							 <tr>
								<td><?=$i+1?>.</td>
								<td><?php echo $rcdInfoArr[$i]['postTitle']?></td>								
								<td class="client_img_center">
									<span class="table_img"><img src="<?php echo $bannerImgPath?>" alt="Stream Poster"></span>
									<span class="table_text" style='display:none'>Banner</span>
								</td>
								<td class="text-center">
									<!----------e------------Here CHANGE STATUS Form Start------------------>
									<form action="controller/segment-controller.php" method="post" class="form-inline" id="<?php echo $frmId4ActionStatus?>">	
										<btn class="button_status" type="button" data-toggle="modal" data-target="#confirmation_modal" ui-toggle-class="bounce" ui-target="#animate" onClick="javascript:setPopupContent('<?php echo $frmId4ActionStatus?>', ' for Change Status', 'Are you sure you want to change this status?');"><i class="fa fa-circle <?php echo $statusCls?> inline" title="<?php echo $statusTxt?>"></i></btn>
										<input type="hidden" name="enckey" value="<?php echo $rcdInfoArr[$i][$enckeyDBFldName]?>">										
										<input type="hidden" name="postAction" value="changeRecordStatus">
										<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
									</form>	
								</td>
								<td class="text-center">								
									<!----------------------Here EDIT Form Start------------------>
									<form action="add-edit-segment.php" method="post" class="form-inline">							
										<button type="submit" class="btn btn-sm btn-success"><small><i class="fa fa-pencil"></i>&nbsp;Edit</small></button>
										<input type="hidden" name="enkey" value="<?php echo $rcdInfoArr[$i][$enckeyDBFldName]?>">										
									</form>
									<!----------------------Here DELETE Form Start------------------>
									 <form action="controller/segment-controller.php" method="post" class="form-inline" id="<?php echo $frmId4ActionDelete?>">		
										<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#confirmation_modal" ui-toggle-class="bounce" ui-target="#animate" onClick="javascript:setPopupContent('<?php echo $frmId4ActionDelete?>', ' for Deletion', 'Do you want to delete this Blog Post details?');">
											<small><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</small>
										</button>
										<input type="hidden" name="enckey" value="<?php echo $rcdInfoArr[$i][$enckeyDBFldName]?>">
										<input type="hidden" name="postAction" value="deleteRecordAction">
										<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
									</form>
								</td>
							 </tr>
<?php
						}
				}
				else
				{
?>
						<tr><td colspan="10"><span class="no_rdc_found_msg">No Record Found</span></td></tr>				
<?php
				}
				include_once('includes/popup/popup-model-confirmation.php');
?>	
                    </tbody>
                </table>
			</div>
            <!-- End of table responsive --> 
		</div>
	</div>
</div>
<!-- End of content -->
<!-- Start of footer -->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
<!-- Start of main content -->