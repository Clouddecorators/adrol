<?php
$SUBTITLE = "Login";
include( "includes/meta-header.php" );
$valParamArray = array( "email" =>
	array(
		"type" => "email",
		"msg" => "Email Id",
	),
	"password" =>
	array(
		"type" => "text",
		"msg" => "Password",
		//"regex" => array("pattern" => "[]", "msg" => "Maximum length should be 15 char."),
	)
);

$_SESSION[ 'formValidation' ] = $valParamArray;
include_once("includes/check-frontend-auth.php");
?>
<!-- End of meta header -->

<!-- Start of login page -->
<div class="login_banner">
<div class="center-block w-xxl w-auto-xs login-box login_box_width">
	<!-- Start of sign form -->
	<div class="box-color r box-shadow-z1 text-color mt">
		<!-- Start of logo -->
		<div class="logo-bottom-margin">
			<div class="centerinbox">
				<a class="navbar-img-large" href="javascript:void(0);" style='cursor: auto;'><img src="images/admin_logo.png" alt="<?php echo removeStr(ADMIN_PANEL_TITLE); ?>" > 
				</a>
			</div>
			<div class="sign-text">Administrator Login Panel</div>
		</div>
		<!-- End of logo -->
		
		<?php
		if ( @$_GET[ 'logout' ] == 1 ) {
			$_SESSION[ 'msgTrue' ] = 1;
			$_SESSION[ 'messageSession' ] = 'You have logged out from the system successfully.';
		} else if ( @$_GET[ 'logout' ] == 2 )$_SESSION[ 'messageSession' ] = 'Sorry, you have accessed an unauthorised web page.';
		include_once( 'includes/flash-msg.php' );
		?>
		<div class="login-text" style='display:none;'>Log In</div>
		<form name="login-form" method="post" action="controller/member-controller.php" onSubmit='return validation(1, <?php echo json_encode($valParamArray); ?>);'>
			<!-- Start of email -->
			<div class="md-form-group float-label">
				<input type="text" name="email" id="email" class="md-input" maxlength="100">
				<label for="email">Email Id:<span class="cla_star">*</span></label>
				<span id='span_email' class='form_error'>
					<?php showErrorMessage('email'); ?>
				</span>
			</div>
			<!-- Start of password -->
			<div class="md-form-group float-label">
				<input type="password" name="password" id="password" class="md-input" maxlength="32">
				<label for="password">Password:<span class="cla_star">*</span></label>
				<span id='span_password' class='form_error'>
					<?php showErrorMessage('password'); ?>
				</span>
				<input type="hidden" name="postAction" value="userLoginAuthentication">
				<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
			</div>
			<!-- Start of checkin -->
			<div class="m-gap" style='display:none;'>
				<label class="md-check"><input type="checkbox"><i class="primary"></i> Keep me signed in</label>
			</div>
			<!-- Start of sign in button -->
			<button type="submit" class="btn btn-warning btn_fullblock p-x-md">Login</button>
			<div class="gap_forget"><a href="forgot-password.php" class="forgot-link">Forgot Password?</a>
			</div>
			<div class="border-line border_topgap"></div>
		</form>
	</div>
	<!-- End of sign form -->
</div>
<!-- End of login page -->
<?php
include_once("includes/frontend-footer.php");
?>
</body>
</html>