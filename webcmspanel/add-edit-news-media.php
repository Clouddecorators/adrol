<?php	
$SUBTITLE = 'Manage News & Media';
include("includes/header.php");	

$enkey = getValPostORGet('enkey', 'B');
$enckeyDBFldName = 'postCode';
$arrDBFld = array($enckeyDBFldName, 'postTitle', 'postDesc', 'postLink', 'postType', 'postThumb', 'status', 'metaTitle', 'metaDescription', 'metaKeywords');	

$backBtnURL = "view-all-news-media.php?".$_SESSION['SESSION_QRY_STRING_FOR_POST'];

if ($enkey)
{
	$btnTxt = 'Submit';
	$postAction = 'updateAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Edit News & Media Detail";

	$infoArr = $objDBQuery->getRecord(0, $arrDBFld, 'tbl_posts', array($enckeyDBFldName => $enkey));
	$postThumb = $infoArr[0]['postThumb'];	
}
else
{
	$btnTxt = 'Submit';
	$postAction = 'addAction';
	$awsomeIcon = "fa fa-plus";
	$pageTitleTxt = "Add New News & Media";

	foreach ($arrDBFld AS $dbFldName)
	{
		$infoArr[0][$dbFldName] = @$_SESSION['session_'.$dbFldName];
		unset($_SESSION['session_'.$dbFldName]);
	}
	$postThumb = '';		
}

$_SESSION['ARR_ALLOW_FORM_KEYS_FOR_DB'] = $arrDBFld;

$arrParamForValidation = array("postTitle" => array("type" => "text", "msg" => "Title"), "postLink" => array("type" => "text", "msg" => "Link"), 'postDesc' => array("type" => "textarea", "msg" => "Description"));

$_SESSION['formValidation'] = $arrParamForValidation;
?>
<!-- Start of content -->
<div class="app-body" >
      <div class="padding">
		<!-- Start of box-->
		<?php include_once('includes/flash-msg.php'); ?>

        <div class="box add_edit_form">
			<!-- Start of box header -->					
            <div class="box-header dker">
                <h3><?=$pageTitleTxt?></h3><a href="<?php echo $backBtnURL?>" class="view-all"><i class="material-icons">&#xe02f;</i>View All News & Media</a>
            </div>
			<!-- End of box header -->
			<!-- Start of box body -->
            <div class="box-body">
				<form class="form-box" name="add-edit-plan-form" enctype="multipart/form-data" method="post" action="controller/news-media-controller.php" onSubmit='return validation(1, <?php echo json_encode($arrParamForValidation); ?>);'>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="postTitle" id="postTitle" maxlength="255" value="<?=$infoArr[0]['postTitle']?>">
							<span id='span_postTitle' class='form_error'><?php showErrorMessage('postTitle'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Link:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="postLink" id="postLink" maxlength="500" value="<?=$infoArr[0]['postLink']?>">
							<span id='span_postLink' class='form_error'><?php showErrorMessage('postLink'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label"><span class="cla_star">*</span>Description:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="postDesc" id="postDesc" rows="8" cols="50"><?=$infoArr[0]['postDesc']?></textarea>
							<span id='span_postDesc' class='form_error'><?php showErrorMessage('postDesc'); ?></span>							
						</div>
					</div>					
					<!-- Start of banner -->
					<div class="form-group row">
						<label for="photo_file" class="col-sm-12 col-md-2 form-control-label">Poster:</label>
						<div class="col-sm-12 col-md-10">
							<!-- Start of photo box -->
<?php
							if ($postThumb != '')
							{
?>
								<div class="photo_box">
									<div class="banner_img_box bannerfullbox">
										<img src="<?=HTTP_PATH_ASSET_UPLOAD.'/post_imgs/'.$postThumb?>" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo HTTP_PATH_ADMIN?>/images/default_poster.jpg';">
									</div>
								</div>
<?php
							}
?>
							<!-- End of photo box -->
							<div id="undo" class="col-sm-4 p-a-xs" style="display:none"><a> Undo Delete</a></div>
							<!-- Start of upload file -->
							<input id="photo_delete" name="photo_delete" type="hidden" value="0" class="has-value">
							<input id="uploadFile" placeholder="Choose File" class="form-control input_smallbox" type="text" id="photo_filess">
							<div class="fileUpload btn btn-sm btn-success">
								<span><i class="fa fa-upload"></i> Upload</span>
								<input id="uploadBtn" type="file" name="postThumb" class="upload form-control">
							</div>
							<!-- End of upload file -->
							<small class="text-muted text-image"><i class="fa fa-question-circle-o"></i>&nbsp;Extensions: png, jpg or jpeg, Dimension: (680x382)</small>
						 </div>
					</div>
					<div class="form-group row" style="display: none;">
						<label class="col-sm-12 col-md-2 form-control-label">Order:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control input_smallbox" type="text" name="imgOrder" id="imgOrder" maxlength="6" value="<?=$infoArr[0]['imgOrder']?>">
							<span id='span_imgOrder' class='form_error'><?php showErrorMessage('imgOrder'); ?></span>
						</div>
					</div>						
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Status:</label>
						<div class="col-sm-12 col-md-10">
<?php			
							makeDropDown('status', array_keys($STATUS), array_values($STATUS), $infoArr[0]['status'], "class='selectpicker' data-size='4'", '', '', 'Y');
?>	
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Meta Title:</label>
						<div class="col-sm-12 col-md-10">
							<input class="form-control" type="text" name="metaTitle" id="metaTitle" maxlength="500" value="<?=$infoArr[0]['metaTitle']?>">
							<span id='span_metaTitle' class='form_error'><?php showErrorMessage('metaTitle'); ?></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Meta Description:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="metaDescription" id="metaDescription" rows="8" cols="50"><?=$infoArr[0]['metaDescription']?></textarea>
							<span id='span_metaDescription' class='form_error'><?php showErrorMessage('metaDescription'); ?></span>
							<script>
								tinymce.init({
									selector:'#metaDescription',
									 height: 100,
									 menubar:false,										 
									 plugins: ["code lists advlist preview"],
									 forced_root_block : '', // Needed for 3.x
									 readonly :0,
									 toolbar: 'code | undo redo | styleselect | bold | italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent',										
								});
							</script>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-12 col-md-2 form-control-label">Meta Keywords:</label>
						<div class="col-sm-12 col-md-10">
							<textarea class="form-control" type="text" name="metaKeywords" id="metaKeywords" rows="8" cols="50"><?=$infoArr[0]['metaDescription']?></textarea>
							<span id='span_metaKeywords' class='form_error'><?php showErrorMessage('metaKeywords'); ?></span>
							<script>
								tinymce.init({
									selector:'#metaKeywords',
									 height: 100,
									 menubar:false,										 
									 plugins: ["code lists advlist preview"],
									 forced_root_block : '', // Needed for 3.x
									 readonly :0,
									 toolbar: 'code | undo redo | styleselect | bold | italic | alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent',										
								});
							</script>
						</div>
					</div> 					
					<!-- Start of button -->
					<div class="form-groups row">
						<div class="col-sm-12 col-md-offset-2 col-md-10 btn_space_gap">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane-o"></i>&nbsp;Submit</button>
<?php
							if ($enkey)
							{
?>
								<a href="<?php echo $backBtnURL?>" class="back_btn_link"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;Back</button></a>
<?php
							}
?>
						    <input type="hidden" name="postAction" value="<?=$postAction?>">	
						    <input type="hidden" name="postType" value="M">	
							<input type="hidden" name="enkey" value="<?=$enkey?>">	
							<input type="hidden" name="formToken" value="<?php echo $_SESSION['prepareToken']; ?>">
						</div>
					</div>
                </form>
            </div>
			<!-- End of box body-->
        </div>
		<!-- End of box-->
    </div>
</div>
<script>
/** Upload image button **/
document.getElementById("uploadBtn").onchange = function ()
{
    document.getElementById("uploadFile").value = this.value.replace(/^C:\\\\/i, '');
};
</script>
<!-- End of content -->
<!-- Start of footer-->
<?php 
	include("includes/footer.php")
?>
<!-- End of footer-->
</div>
