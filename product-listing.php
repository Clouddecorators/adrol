<?php
$slug = $_GET['slug'];
$metaFrm = "P";
$PAGE_TITLE = ucwords(str_replace('-', ' ', $slug));

include_once("include/meta-header.php");
include_once("include/header.php");
$whereCls = "s.slug = '$slug' AND s.segmentCode = p.segmentCode_RK AND p.status = 'A' AND p.prdType = 'P' AND p.isDeleted = 'N' AND s.isDeleted = 'N'";
$rcdInfoArr = $objDBQuery->getRecord(0, "*, p.slug AS pSlug, p.segmentName", 'tbl_products p, tbl_segments s', $whereCls, '', '', 'p.createdOn', 'DESC');	
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
	$numOfRows = count($rcdInfoArr);
	for ($i = 0; $i < $numOfRows; $i++)
	{
		$imgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[$i]['prdImg']; 
		$prdMobViewImg = $rcdInfoArr[$i]['prdMobViewImg'];
		$prdTabViewImg = $rcdInfoArr[$i]['prdTabViewImg'];
		if ($prdMobViewImg != '')
		{
			$moviewImgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$prdMobViewImg; 
		}
		else 
		{
			$moviewImgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[$i]['prdImg']; 
		}

		if ($prdTabViewImg != '')
		{
			$tabviewImgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$prdTabViewImg; 
		}
		else 
		{
			$tabviewImgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[$i]['prdImg']; 
		}
		
		if ($i % 2 == 0)
		{
			$itemDivClass = 'justify-content-start';
			$itemDiv2Class = 'position-relative';
			$itemP1Class = 'ms-2';
			$segDivClass = 'pipe5';
		}
		else
		{
			$itemDivClass = 'justify-content-end';
			$itemDiv2Class = 'text-end';
			$itemP1Class = 'me-2';
			$segDivClass = 'pipe3 fe-bold';
		}
		$strTitle = $rcdInfoArr[$i]['title'];
		$segmentName = $rcdInfoArr[$i]['segmentName'];
		if ($segmentName == '') $segmentName = $rcdInfoArr[$i]['postTitle'];
		$t3 = '';
		list($t1, $t2, $t3) = explode(' ', $strTitle, 3);
		$segmentName = strtoupper(str_replace(array('oil', 'Oil'), array(' ', ' '), $segmentName))
	
?>	
	<section class="position-relative">
	     <div class="d-none d-md-none d-lg-block d-xl-block d-xxl-block" style="background: url(<?php echo $imgPath?>) no-repeat; background-size: cover; background-position:center; ">
	         <a style="text-decoration:none;" href="<?php echo HTTP_PATH?>/product/<?php echo $rcdInfoArr[$i]['pSlug']?>">
		<div class="container-fluid">
			<div class="row ht-100 <?php echo $itemDivClass?> align-items-center">
				<div class="col-sm-12 <?php echo $itemDiv2Class?>">
					<p class="title-set <?php echo $itemP1Class?>"><?php echo $t1?></p>
<?php
					if (@$t2) echo "<p class='font-set $itemP1Class'>$t2</p>";
					if (@$t3) echo "<p class='font-set $itemP1Class'>$t3</p>";
					?>
					<p class="text-white fs-5 font-bebas-neue fw-bold mb-2 <?php echo $itemP1Class?>"><?php echo $rcdInfoArr[$i]['prdsku']?></p>
					<div class="<?php echo $segDivClass?>"><?php echo $segmentName?> SEGMENT</div>
				</div>
			</div>
		</div>
		</a>
		<a class="enter-button" href="<?php echo HTTP_PATH?>/product/<?php echo $rcdInfoArr[$i]['pSlug']?>">View Details &nbsp; <i class="fa fa-angle-right  animate__animated animate__fadeIn animate__infinite"></i>
		</a>
	     </div>
	     
	     <!--For Tab-->
	      <div class="d-none d-md-block d-lg-none d-xl-none d-xxl-none" style="background: url(<?php echo $tabviewImgPath?>) no-repeat; background-size: cover; background-position:center; ">
	         <a style="text-decoration:none;" href="<?php echo HTTP_PATH?>/product/<?php echo $rcdInfoArr[$i]['pSlug']?>">
		<div class="container-fluid">
			<div class="row ht-100 <?php echo $itemDivClass?> align-items-center">
				<div class="col-sm-12 <?php echo $itemDiv2Class?>">
					<p class="title-set <?php echo $itemP1Class?>"><?php echo $t1?></p>
<?php
					if (@$t2) echo "<p class='font-set $itemP1Class'>$t2</p>";
					if (@$t3) echo "<p class='font-set $itemP1Class'>$t3</p>";
					?>
					<p class="text-white fs-5 font-bebas-neue fw-bold mb-0  <?php echo $itemP1Class?>"><?php echo $rcdInfoArr[$i]['prdsku']?></p>
					<div class="<?php echo $segDivClass?>"><?php echo $segmentName?> SEGMENT</div>
				</div>
			</div>
		</div>
		</a>
		<a class="enter-button" href="<?php echo HTTP_PATH?>/product/<?php echo $rcdInfoArr[$i]['pSlug']?>">View Details &nbsp; <i class="fa fa-angle-right  animate__animated animate__fadeIn animate__infinite"></i>
		</a>
	     </div>
	     
	     <!--For mobile-->
	     <div class="d-block d-md-none d-lg-none d-xl-none d-xxl-none"
        style="background: url(<?php echo $moviewImgPath?>) no-repeat; background-size: cover; background-position: center;">
        <div class="container-fluid">
            <div class="row ht-100 justify-content-start">
                <div class="col-sm-12" style="position: absolute; bottom: 188px;">
<?php
				$arrTitle = explode(' ', strip_tags($strTitle),2);
				$sCls = 'title-set ms-2';				
				foreach ($arrTitle as $sTitle)
				{					
?>
                    <p class="<?php echo $sCls?>"><?php echo $sTitle?></p>                    
<?php
				    $sCls = 'font-set ms-2'; 
				}
?>
                    <div class="pipe5"><?php echo $segmentName?> SEGMENT</div>
                    <p class="text-white fs-5 font-bebas-neue fw-bold ms-2 mb-0"><?php echo $rcdInfoArr[$i]['prdsku']?></p>
                </div>
            </div>
        </div>
        <a class="enter-button" href="<?php echo HTTP_PATH?>/product/<?php echo $rcdInfoArr[$i]['pSlug']?>">
            View Details &nbsp; <i class="fa fa-angle-right  animate__animated animate__fadeIn animate__infinite"></i>
        </a>
    </div>
	     
	    
	</section>
<?php
	}
}

$whereCls = "s.slug = '$slug' AND s.segmentCode = p.segmentCode_RK AND p.status = 'A' AND p.prdType = 'S' AND p.isDeleted = 'N' AND s.isDeleted = 'N'";
$rcdInfoArr = $objDBQuery->getRecord(0, '*, p.slug AS pSlug', 'tbl_products p, tbl_segments s', $whereCls, '', '', 'p.createdOn', 'DESC');	
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{	
?>

<section class="bg-secondary-grey">
    <div class="container-fluid">
        <div class="row row-flex py-5 gy-4">
<?php
		$numOfRows = count($rcdInfoArr);
		for ($i = 0; $i < $numOfRows; $i++)
		{
			$imgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[$i]['prdImg']; 

			$strTitle = $rcdInfoArr[$i]['title'];
			list($t1, $t2, $t3) = explode(' ', $strTitle, 2);
			$prdPdf = $rcdInfoArr[$i]['prdPdf']; 

			$pdfPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$prdPdf;
?>		
            
			<div class="col-lg-6 col-md-12 col-sm-12">
                <div class="bg-secondary-grey-still p-5 overflow-hidden bdr-radius h-100">
                    <div class="row align-items-center justify-content-xxl-start justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-sm-center h-100"> 
                        <div class="col-md-6">
                            <p class="fw-bold ft-36 mb-0"><?php echo $t1?></p>
<?php
							if (@$t2) echo "<p class='fw-bold ft-48'>$t2</p>";                            
							if (@$t3) echo "<p class='fw-bold ft-48'>$t3</p>";   
?>						
                            <p class="fs-5 font-bebas-neue fw-bold"><?php echo $rcdInfoArr[$i]['prdsku']?></p>
<?php
				if ($prdPdf != '')
				{
?>				
                    <a class="btn button-rounded my-3" href="<?php echo $pdfPath?>" target="_blank">Product Description</a>
<?php
				}
?>
                        </div>
                        <div
                            class="col-md-6 position-relative text-center">
                            <img src="<?php echo $imgPath?>" width="250">
                        </div>
                    </div>
                </div>
            </div>
<?php
		}
}
?>			

        </div>
    </div>
</section>

<?php
 include_once("include/footer.php");
?> 