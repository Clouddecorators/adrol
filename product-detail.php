<?php
$slug = $_GET['slug'];
$PAGE_TITLE = ucwords(str_replace('-', ' ', $slug));
$metaFrm = "PB";
include_once("include/meta-header.php");
include_once("include/header.php");
$whereCls = "p.slug = '$slug' AND s.segmentCode = p.segmentCode_RK AND p.status = 'A' AND p.prdType = 'P' AND p.isDeleted = 'N' AND s.isDeleted = 'N'";
?>
<div class="divider6"></div>
<?php
$rcdInfoArr = $objDBQuery->getRecord(0, "*, s.slug AS pSlug", 'tbl_products p, tbl_segments s', $whereCls, '', '', 'p.createdOn', 'DESC');	
if (is_array($rcdInfoArr) && !empty($rcdInfoArr))
{
	$prdFor = $rcdInfoArr[0]['prdFor']; 
	$prdPdf = $rcdInfoArr[0]['prdPdf']; 	 

	$pdfPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$prdPdf; 
	$imgPath = HTTP_PATH_ASSET_UPLOAD.'/product_imgs/'.$rcdInfoArr[0]['prdDeatilPageImg']; 

	if ($prdFor == 'G') $iconPath = HTTP_PATH_ASSET_UPLOAD.'/segment_imgs/'.$rcdInfoArr[0]['icon']; 
	else if ($prdFor == 'ECG') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/gce.png';
	else if ($prdFor == 'GC') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/gear-clutch.png';
	else if ($prdFor == 'C') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/clutch.png';
	else if ($prdFor == 'GE') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/gear.png';
	else if ($prdFor == 'E') $iconPath = HTTP_PATH_ASSET.'/images/seg_detail_page_icons/engine.png';
?>

<section class="glide-us-panel position-relative d-block">
	<!--<a href="<?php echo HTTP_PATH?>/products/<?php echo $rcdInfoArr[0]['pSlug']?>" class=" float-start ms-2 mt-3" aria-label="Close"><i class="fa fa-times fs-4 text-muted" alt="Close"></i></a>-->
	<a href="javascript:void(0);" onclick="javascript:history.back()" class=" float-start ms-2 mt-3" aria-label="Close"><i class="fa fa-times fs-4 text-muted" alt="Close"></i></a>
	
    <div class="text-end icon-right-set">
<?php
		if ($prdFor != '')
		{
?>
			<img src="<?php echo $iconPath?>" class="img-fluid">
<?php
		}
?>
	</div>
    <div class="container">
        <div class="row justify-content-center align-items-center hts-100 py-5">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6">
                <div class="pipe-set-about">BENEFIT OF THE PRODUCT</div>
                <div class="ft-20 mt-4">
                    <p class=" font-bebas-neue fs-4 fw-bold text-danger"><?php echo strip_tags($rcdInfoArr[0]['title'])?></p>
                    <p><?php echo nl2br($rcdInfoArr[0]['description'])?></p>
                    
                    <div class="mb-3">
<?php
					if ($prdPdf != '')
					{
?>
                        <a class="btn button-rounded" href="<?php echo $pdfPath?>" target="_blank">
                            Product Description
                        </a>
<?php
					}
?>
                        <!--<a class="btn back-button" href="">Back</a>-->
                    </div>

                </div>

            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 text-center p-5 position-relative">
<?php
				$imgWidth = '';
				if ($prdFor == 'G')
				{
					$imgWidth = "width='207px'";
?>
                <div class="circle d-none d-md-block d-lg-block d-xl-block d-xxl-block"><img src="<?php echo HTTP_PATH_ASSET?>/images/seg_detail_page_icons/circle.png" class="img-fluid" ></div>
<?php
				}
?>
                <img src="<?php echo $imgPath?>" class="img-fluid" <?php echo $imgWidth?>>
            </div>
        </div>
    </div>
    <div class="dark-grey col-sm-12 p-0">
        <div class="d-flex flex-wrap bd-highlight">
            <div class="bg-light-secondary text-white p-3 fw-bold">SKUs Available</div>
 <?php
			$qtyArr = strDataExplode($rcdInfoArr[0]['qty']);
			
			$tQty = count($qtyArr);
			for ($i = 0; $i < $tQty; $i++)
			{
				$strPipe = '<div class="py-3 fw-bold">I</div>'; 
				if ($i == $tQty -1) $strPipe = '';
?>
				<div class="py-3 fw-bold px-4"><?php echo $qtyArr[$i]?></div><?php echo $strPipe?>
				   
<?php
			}
?>
        </div>
    </div>
</section>

<?php
}
 include_once("include/footer.php");
?> 