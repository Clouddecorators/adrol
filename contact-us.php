<?php
$PAGE_TITLE = "Contact Us";
include_once("include/meta-header.php");
include_once("include/header.php");
?>
 <div class="divider5"></div>
    <section class="photo-gallery pb-5">
        <!-- Topic Cards -->
        <div id="">
            <div class="container">
                <div class="text-center text-black pt-5 mb-0 font-bebas-neue ft-4"></div> 
                
                <div class="row justify-content-center align-items-center">
                    <div class="bg-white col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 p-5 shadow" style="border-radius: 32px;">
                    <div class="row justify-content-center align-items-center">
                    <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3 col-xxl-3 text-center d-none d-md-block d-lg-block d-xl-block d-xxl-block" style="border-right: 1px solid #dedada;">
                          
                        <ul class="list-unstyled">
                            <li class="mb-3">
                                <i class="fa fa-map-marker fa-2x text-danger" area-hidden="true"></i>
                                <p class="mb-0 text-dark fw-bold">Address</p>
                                <small class="text-secondary">536, Punjabi Basti, 3rd Floor, Main Rohtak Road,<br> Nangloi, Delhi - 110041</small>
                            </li>
                            <li class="mb-3">
                                <i class="fa fa-phone fa-2x text-danger" area-hidden="true"></i>
                                <p class="mb-0 text-dark fw-bold">Let's Talk</p>
                                <small class="text-secondary">+91 910-003-2000 <br>+91 828-500-5500</small>
                            </li>
                            
                            <li class="mb-3">
                                <i class="fa fa-envelope fa-2x text-danger" area-hidden="true"></i>
                                <p class="mb-0 text-dark fw-bold">General Enquiry</p>
                                <small class="text-secondary">
                                    <a href="mailto:info@adrollubricants.com" class="text-dark">info@adrollubricants.com</a>
                                </small>
                            </li>
                        </ul>
                        
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 col-xl-9 col-xxl-9">
                         <form action="thankyou" method="post" onClick="frmValidation();">
                        <div class="col-sm-12" id="contactform">
                            <p class="text-danger fs-4 fw-bold">Connect with us</p>
                            <div class="row form-design">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="First Name" id="name" name="fname" value="" required maxlength="100">
                                    </div>
                                </div>
                                 <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Last Name" id="name" name="lname" value=""  maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="email" class="form-control" placeholder="Your Email Id" id="email" name="email" value="" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
<?php
    $formToken = date('U');
    $_SESSION['FORM_TOKEN'] = $formToken;
?>
                                    <div class="form-group mb-3">
                                     <input type="number" class="form-control" placeholder="Your Phone Number" id="phone" name="phone" value="" required maxlength="10" minlength="10">
                                        <input type="hidden" name="formToken" value="<?php echo $formToken?>">
                                        <input type="hidden" name="frmType" value="conn">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="comment" placeholder="Message:" required name="message"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <div class="getintouchBtn">
									<button type="sumbit" class="btn btn-primary btn-block">
											Send message
                                            <img src="<?php echo HTTP_PATH_ASSET?>/img/right-arrow_withour_border_white.svg" alt="">
									</button> </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </form>
                    </div>
                    
                    
                    </div>
                </div>
                </div>
                
                
            </div>
        </div>

    </section>

    <section class="photo-gallery photo-gallery2 pb-5">
        <!-- Topic Cards -->
        <div id="">
            <div class="container">
                <div class="text-center text-black pt-5 mb-0 font-bebas-neue ft-4"></div> 
                <div class="row justify-content-center align-items-center">
                    <div class="bg-white col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 p-5 shadow" style="border-radius: 32px;">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3 col-xxl-3">
                            <p class="text-danger fs-4 fw-bold text-center">Connect with Us</p>
                            <img src="<?php echo HTTP_PATH_ASSET?>/images/hand.png" class="img-fluid">
                        </div>
                    
                    <div class="col-sm-12 col-md-8 col-lg-9 col-xl-9 col-xxl-9">
                         <form action="thankyou" method="post" onclick="frmValidation();">
                        <div class="col-sm-12" id="contactform">
                           
                            <div class="row form-design">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Name" id="cName" name="cName" value="" required maxlength="150">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="City/District" id="cCity" name="cCity" required value="" maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="State" id="cState" name="cState" required value="" maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="number" class="form-control" placeholder="Mobile Number" id="cPhone" name="cPhone" value="" required maxlength="12">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="number" class="form-control" placeholder="Alternet Mobile Number" id="cAltPhone" name="cAltPhone" value="" >
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="email" class="form-control" placeholder="Your Email Id" id="cEmail" name="cEmail" value="" required="" maxlength="150">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Address" id="cAddress" name="cAddress" value="" required="" maxlength="250">                                       
                                        <input type="hidden" name="formToken" value="<?php echo $formToken?>">
                                        <input type="hidden" name="frmType" value="connect">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <select class="form-select form-control" aria-label="Default select example" id="cEnquiryType" name="cEnquiryType">
                                        <option selected>Enquiry Type</option>
                                        <option value="Distributor">Distributor</option>
                                        <option value="Sub-Distributor/Dealer">Sub-Distributor/Dealer</option>
                                        <option value="Retailer">Retailer</option>
                                        <option value="Workshop">Workshop</option>
                                        <option value="Consumer">Consumer</option>
                                        <option value="Other">Other</option>

                                      </select>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="cRemarks" placeholder="Remarks:" required="" name="cRemarks"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <div class="getintouchBtn">
									<button type="sumbit" class="btn btn-primary btn-block">
											Submit
									</button> </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </form>
                    </div>
                    
                    
                    </div>
                </div>
                </div>
                
                
            </div>
        </div>

    </section>

<?php
 include_once("include/footer.php");
?> 
  <!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
</script>