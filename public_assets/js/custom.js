$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
 
      items : 2,
      itemsDesktop : [1199,2],
      itemsDesktopSmall : [979,2],
      itemsDesktopSmall : [767,1]
 
  });

  var owl = $("#owl-demo");
 
  owl.owlCarousel(); 
  // Custom Navigation Events
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  });
});


$(document).ready(function(){
    $(".about-us-panel").hide();
  $(".about-bgs").click(function(){
    $(".about-us-panel").toggle(1000);
  });

  $(".crox-panel").hide();
  $(".journey-bg").click(function(){
    $(".crox-panel").toggle(1000);
  });

  $(".director-panel").hide();
  $(".director-bg").click(function(){
    $(".director-panel").toggle(1000);
  });

  $(".elite-us-panel").hide();
  $(".elite-bg").click(function(){
    $(".elite-us-panel").toggle(1000);
  });

  $(".sprint-us-panel").hide();
  $(".sprint-bg").click(function(){
    $(".sprint-us-panel").toggle(1000);
  });

  $(".glide-us-panel").hide();
  $(".glide-bg").click(function(){
    $(".glide-us-panel").toggle(1000);
  });

  $(".passion-us-panel").hide();
  $(".passion-bg").click(function(){
    $(".passion-us-panel").toggle(1000);
  });

  $(".royal-us-panel").hide();
  $(".royal-bg").click(function(){
    $(".royal-us-panel").toggle(1000);
  });

  $(".car-glide-us-panel").hide();
  $(".car-glide-bg").click(function(){
    $(".car-glide-us-panel").toggle(1000);
  });

  $(".supreme-us-panel").hide();
  $(".supreme-bg").click(function(){
    $(".supreme-us-panel").toggle(1000);
  });

  $(".car-supreme-us-panel").hide();
  $(".car-supreme-bg").click(function(){
    $(".car-supreme-us-panel").toggle(1000);
  });

  $(".super-king-us-panel").hide();
  $(".super-king-bg").click(function(){
    $(".super-king-us-panel").toggle(1000);
  });
  $(".synthetic-us-panel").hide();
  $(".synthetic-bg").click(function(){
    $(".synthetic-us-panel").toggle(1000);
  });

  $(".scooty-us-panel").hide();
  $(".scooty-bg").click(function(){
    $(".scooty-us-panel").toggle(1000);
  });

  $(".car-us-panel").hide();
  $(".car-bg").click(function(){
    $(".car-us-panel").toggle(1000);
  });

  
  $(".threewheeler-us-panel").hide();
  $(".threewheeler-bg").click(function(){
    $(".threewheeler-us-panel").toggle(1000);
  });
  
  $(".magic-ace-us-panel").hide();
  $(".magic-ace-bg").click(function(){
    $(".magic-ace-us-panel").toggle(1000);
  });

  $(".power-us-panel").hide();
  $(".power-bg").click(function(){
    $(".power-us-panel").toggle(1000);
  });

  $(".geo-gas-us-panel").hide();
  $(".geo-gas-bg").click(function(){
    $(".geo-gas-us-panel").toggle(1000);
  });

  $(".truck-glide-us-panel").hide();
  $(".truck-glide-bg").click(function(){
    $(".truck-glide-us-panel").toggle(1000);
  });

  $(".truck-elite-supreme-us-panel").hide();
  $(".truck-elite-supreme-bg").click(function(){
    $(".truck-elite-supreme-us-panel").toggle(1000);
  });

  $(".truck-arbplus-us-panel").hide();
  $(".truck-arbplus-bg").click(function(){
    $(".truck-arbplus-us-panel").toggle(1000);
  });

  $(".truck-arbturbo-us-panel").hide();
  $(".truck-arbturbo-bg").click(function(){
    $(".truck-arbturbo-us-panel").toggle(1000);
  });

  $(".tractor-glide-us-panel").hide();
  $(".tractor-glide-bg").click(function(){
    $(".tractor-glide-us-panel").toggle(1000);
  });
 
  $(".adrol-kissan-us-panel").hide();
  $(".adrol-kissan-bg").click(function(){
    $(".adrol-kissan-us-panel").toggle(1000);
  });
  $(".adroll-kisaan-utto-us-panel").hide();
  $(".adroll-kisaan-utto-bg").click(function(){
    $(".adroll-kisaan-utto-us-panel").toggle(1000);
  });
  $(".adroll-pso-us-panel").hide();
  $(".adroll-pso-bg").click(function(){
    $(".adroll-pso-us-panel").toggle(1000);
  });
  $(".bearing-glide-us-panel").hide();
  $(".bearing-glide-bg").click(function(){
    $(".bearing-glide-us-panel").toggle(1000);
  });
  $(".red-gel-grease-us-panel").hide();
  $(".red-gel-grease-bg").click(function(){
    $(".red-gel-grease-us-panel").toggle(1000);
  });
  $(".adrol-long-run-us-panel").hide();
  $(".adrol-long-run-bg").click(function(){
    $(".adrol-long-run-us-panel").toggle(1000);
  });

  $(".adroll-ap3-us-panel").hide();
  $(".adroll-ap3-bg").click(function(){
    $(".adroll-ap3-us-panel").toggle(1000);
  });

  $(".glide-gear-oil-us-panel").hide();
  $(".glide-gear-oil-bg").click(function(){
    $(".glide-gear-oil-us-panel").toggle(1000);
  });

  $(".adrol-elite-premium-us-panel").hide();
  $(".adrol-elite-premium-bg").click(function(){
    $(".adrol-elite-premium-us-panel").toggle(1000);
  });
 
  $(".elite-gear-us-panel").hide();
  $(".elite-gear-bg").click(function(){
    $(".elite-gear-us-panel").toggle(1000);
  });

  $(".super-power-us-panel").hide();
  $(".super-power-bg").click(function(){
    $(".super-power-us-panel").toggle(1000);
  });
  
  $(".industrial-gear-us-panel").hide();
  $(".industrial-gear-bg").click(function(){
    $(".industrial-gear-us-panel").toggle(1000);
  });
  
  $(".industrial-grinding-us-panel").hide();
  $(".industrial-grinding-bg").click(function(){
    $(".industrial-grinding-us-panel").toggle(1000);
  });

  $(".adrol-gpmo-oil-us-panel").hide();
  $(".adrol-gpmo-oil-bg").click(function(){
    $(".adrol-gpmo-oil-us-panel").toggle(1000);
  });

  $(".adrol-rust-oil-us-panel").hide();
  $(".adrol-rust-oil-bg").click(function(){
    $(".adrol-rust-oil-us-panel").toggle(1000);
  });

  $(".adrol-cutting-oil-us-panel").hide();
  $(".adrol-cutting-oil-bg").click(function(){
    $(".adrol-cutting-oil-us-panel").toggle(1000);
  });

  $(".coolant-us-panel").hide();
  $(".coolant-bg").click(function(){
    $(".coolant-us-panel").toggle(1000);
  });

  $(".coolant-heavy-us-panel").hide();
  $(".coolant-heavy-bg").click(function(){
    $(".coolant-heavy-us-panel").toggle(1000);
  });

  $(".break-oil-us-panel").hide();
  $(".break-oil-bg").click(function(){
    $(".break-oil-us-panel").toggle(1000);
  });

  $(".x-pert-oil-us-panel").hide();
  $(".x-pert-oil-bg").click(function(){
    $(".x-pert-oil-us-panel").toggle(1000);
  });


  $('.enquiry-button, .close').click( function(e) {        
    e.preventDefault(); 
    e.stopPropagation(); 
    $('.getintouch').toggle();        
});    
$('.getintouch').click( function(e) {        
    e.stopPropagation();         
});    
$('body').click( function() {       
    $('.getintouch').hide();        
});    

});

function openNav() {
  document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.height = "0%";
}



// counter//
function inVisible(element) {
  //Checking if the element is
  //visible in the viewport
  var WindowTop = $(window).scrollTop();
  var WindowBottom = WindowTop + $(window).height();
  var ElementTop = element.offset().top;
  var ElementBottom = ElementTop + element.height();
  //animating the element if it is
  //visible in the viewport
  if ((ElementBottom <= WindowBottom) && ElementTop >= WindowTop)
    animate(element);
}

function animate(element) {
  //Animating the element if not animated before
  if (!element.hasClass('ms-animated')) {
    var maxval = element.data('max');
    var html = element.html();
    element.addClass("ms-animated");
    $({
      countNum: element.html()
    }).animate({
      countNum: maxval
    }, {
      //duration 5 seconds
      duration: 5000,
      easing: 'linear',
      step: function() {
        element.html(Math.floor(this.countNum) + html);
      },
      complete: function() {
        element.html(this.countNum + html);
      }
    });
  }

}

//When the document is ready
$(function() {
  //This is triggered when the
  //user scrolls the page
  $(window).scroll(function() {
    //Checking if each items to animate are 
    //visible in the viewport
    $("div[data-max]").each(function() {
      inVisible($(this));
    });
  })
});


$(function () {

  var mySwiper = new Swiper('.swiper-container', {
      // Optional parameters
      direction: 'horizontal',
      loop: true,

      // If we need pagination
      pagination: '.swiper-pagination',

      // Navigation arrows
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',

      // And if we need scrollbar
      // scrollbar: '.swiper-scrollbar',

      autoplay: 3000
  });

});

// Header Js
document.addEventListener("DOMContentLoaded", function () {
  window.addEventListener('scroll', function () {
      if (window.scrollY > 500) {
          document.getElementById('navbar_top').classList.add('fixed-top');
          document.getElementById('navbar-brands').classList.add('brands');
          // add padding top to show content behind navbar
          navbar_height = document.querySelector('.navbar').offsetHeight;
          document.body.style.paddingTop = navbar_height + 'px';
      } else {
          document.getElementById('navbar_top').classList.remove('fixed-top');
          document.getElementById('navbar-brands').classList.remove('brands');
          // remove padding top from body
          document.body.style.paddingTop = '0';
      }
  });
});

// headee js end 


//   var v = document.getElementsByTagName("video")[0];

//   v.addEventListener("canplay", function () {
//       mySwiper.startAutoplay();
//   }, true);

//   v.addEventListener("ended", function () {
//       mySwiper.stopAutoplay();
//   }, true);

// 'use strict';

// function debounce(func, wait, immediate) {

  // Debounce
  // http://davidwalsh.name/javascript-debounce-function

//   var timeout;
//   return function() {
//     var context = this,
//       args = arguments;
//     var later = function() {
//       timeout = null;
//       if (!immediate) {
//         func.apply(context, args);
//       }
//     };
//     var callNow = immediate && !timeout;
//     clearTimeout(timeout);
//     timeout = setTimeout(later, wait);
//     if (callNow) {
//       func.apply(context, args);
//     }
//   };
// }

// var htmlTag = document.getElementsByTagName('html')[0];
// var videoContainer = document.querySelector('#video-container');
// var videoElem = document.querySelector('#video-container video');

// var minW = 320; // Minimum video width allowed
// var vidWOrig; // Original video dimensions
// var vidHOrig;

// vidWOrig = videoElem.getAttribute('width');
// vidHOrig = videoElem.getAttribute('height');

// var videoCover = function() {

//   var winWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
//   var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

//   // Set the video viewport to the window size

//   videoContainer.style.width = winWidth + 'px';
//   videoContainer.style.height = winHeight + 'px';

//   // Use largest scale factor of horizontal/vertical
//   var scaleH = winWidth / vidWOrig;
//   var scaleV = winHeight / vidHOrig;
//   var scale = scaleH > scaleV ? scaleH : scaleV;

//   // Don't allow scaled width < minimum video width
//   if (scale * vidWOrig < minW) {
//     scale = minW / vidWOrig;
//   }

//   // Scale the video
//   var videoNewWidth = scale * vidWOrig;
//   var videoNewHeight = scale * vidHOrig;

//   videoElem.style.width = videoNewWidth + 'px';
//   videoElem.style.height = videoNewHeight + 'px';

//   // Center it by scrolling the video viewport
//   videoContainer.scrollLeft = (videoNewWidth - winWidth) / 2;
//   videoContainer.scrollTop = (videoNewHeight - winHeight) / 2;

// };

// if (htmlTag.classList.contains('no-touch')) {
//   videoCover();

//   // Adjust on resize
//   var updateVideo = debounce(function() {
//     videoCover();
//   }, 100);

//   window.addEventListener('resize', updateVideo);
// }



function setDropDownValue(curDropdownVal, dropDownIdTo, caseName, requestURl)
{	
	var uid = curDropdownVal;
	var dropDownIdTo = dropDownIdTo;
	$.ajax(
	{
		url: requestURl+"?uid="+uid+"&caseName="+caseName,
	})
	.done(function(data)
	{			
		$("#"+dropDownIdTo).html(data);			
	});
}
