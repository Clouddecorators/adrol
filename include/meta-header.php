<?php
$CUR_SCRIPT_NAME = basename($_SERVER['SCRIPT_NAME']);
include_once("inc.php");
$WEB_SETUP_INFO_ARR = $objDBQuery->getRecord(0, '*', 'tbl_webs', '1', 0, 1, 'webId_PK', 'ASC');
if (isset($metaFrm) && $metaFrm == "D")
{
  $metaInfoArr = $objDBQuery->getRecord(0, '*', 'tbl_webs', '1', 0, 1, 'webId_PK', 'ASC');
  
}
else if (isset($metaFrm) && $metaFrm == "PD")
{
  $metaInfoArr = $objDBQuery->getRecord(0, array('metaTitle', 'metaDescription', 'metaKeywords'), 'tbl_posts', array('slug' => strtolower($slug)));
  $pageTitle = $metaInfoArr[0]['metaTitle'];  
  $META_DESCRIPTION = $metaInfoArr[0]['metaDescription'];
  $META_KEYWORDS = $metaInfoArr[0]['metaKeywords'];  
}
else if (isset($metaFrm) && $metaFrm == "PB")
{
  $metaInfoArr = $objDBQuery->getRecord(0, array('metaTitle', 'metaDescription', 'metaKeywords'), 'tbl_products', array('slug' => strtolower($slug)));
  $pageTitle = $metaInfoArr[0]['metaTitle'];  
  $META_DESCRIPTION = $metaInfoArr[0]['metaDescription'];
  $META_KEYWORDS = $metaInfoArr[0]['metaKeywords'];  
}
else if (isset($metaFrm) && $metaFrm == "P")
{
  $metaInfoArr = $objDBQuery->getRecord(0, array('metaTitle', 'metaDescription', 'metaKeywords'), 'tbl_segments', array('slug' => strtolower($slug)));
  $pageTitle = $metaInfoArr[0]['metaTitle'];  
  $META_DESCRIPTION = $metaInfoArr[0]['metaDescription'];
  $META_KEYWORDS = $metaInfoArr[0]['metaKeywords'];  
}
else
{
  $metaInfoArr = $objDBQuery->getRecord(0, array('title', 'metaTagCode', 'metaDescription', 'metaKeywords'), 'tbl_meta_tags', array('pageAbbr' => strtolower($PAGE_TITLE)));
  $pageTitle = $metaInfoArr[0]['title'];  
  $META_DESCRIPTION = $metaInfoArr[0]['metaDescription'];
  $META_KEYWORDS = $metaInfoArr[0]['metaKeywords'];
}
if ($pageTitle == '') $PAGE_TITLE = $PAGE_TITLE.' | '.USER_PANEL_TITLE; 
else $PAGE_TITLE = $pageTitle; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title><?php echo htmlentities($PAGE_TITLE)?></title>  
  <meta name="google-site-verification" content="tesBciu6dVL9bSk2FiVQH9mHw1mWICiAAdz-EXpCMHk" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" /> 
  <meta name="description" content="<?php echo htmlentities($META_DESCRIPTION)?>">
  <meta name="keywords" content="<?php echo htmlentities($META_KEYWORDS)?>">
  <meta name="robots" content="index,follow">

  <!-- Bootstrap core CSS -->
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo HTTP_PATH_ASSET?>/images/faviconi.ico'>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
    crossorigin="anonymous" />
  <link type="text/css" rel="stylesheet" href="<?php echo HTTP_PATH_ASSET?>/css/custom-style.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
   <link rel='stylesheet' href='<?php echo HTTP_PATH_ASSET?>/css/owl.carousel.css'>
      <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">

  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap"
    rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@300;400;500;600;700&family=Lora:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">   
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2D270XL2VH"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2D270XL2VH');
</script>
</head>