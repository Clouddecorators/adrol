    <div class="fixed-top">
        <nav class="navbar navbar-expand navbar-dark top-bg d-none d-md-block d-lg-block d-xl-block d-xxl-block p-0"
            aria-label="Second navbar example">
            <div class="container-xl container-md-fluid">
                <a class="navbar-brand" href="<?php echo HTTP_PATH?>" style="width:113px;"> <img src="<?php echo HTTP_PATH_ASSET?>/images/logo.png" width="70"
                        class="img-fluid" style="visibility:hidden;" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarsExample02">
                    <ul class="navbar-nav me-auto nav-set" style="margin-top:-18px;">	
						<li class="nav-item">
                            <a class="nav-link text-white fw-normal" aria-current="page"> DISTRIBUTOR ENQUIRY <i class="fa fa-mobile" aria-hidden="true"></i>
 <?php echo $WEB_SETUP_INFO_ARR[0]['headerEnquiry']?> OR <i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $WEB_SETUP_INFO_ARR[0]['infoEmail']?></a>
                        </li>						
                    </ul>
                    <!--<form style="margin-top: -19px;" action="products">-->
                    <!--    <input class="form-control" type="text" placeholder="Search" aria-label="Search">-->
                    <!--</form>-->
                </div>
            </div>
        </nav>
        <nav id="navbar_top" class="navbar navbar-expand-lg navbar-dark bg-dark-bg p-0">
            <div class="container padd-14">
                <a id="navbar-brands" class="navbar-brand brand" href="<?php echo HTTP_PATH?>"> <img src="<?php echo HTTP_PATH_ASSET?>/images/logo.png" class="img-fluid" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main-menu">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 nav-set">
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'home.php') echo 'active'?>" aria-current="page" href="<?php echo HTTP_PATH?>/home">HOME</a>
                        </li>
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'products.php') echo 'active'?>" aria-current="" href="<?php echo HTTP_PATH?>/products">PRODUCTS</a>
                        </li>
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'connects.php') echo 'active'?>" aria-current="" href="<?php echo HTTP_PATH?>/connects">CONNECTS</a>
                        </li>
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'blogs.php') echo 'active'?>" aria-current="" href="<?php echo HTTP_PATH?>/blogs">BLOGS</a>
                        </li>
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'advertisement.php') echo 'active'?>" aria-current="" href="advertisement">ADVERTISING</a>
                        </li>
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'pr.php') echo 'active'?>" aria-current="" href="pr">Pr</a>
                        </li>                        
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'news-event.php') echo 'active'?>" href="<?php echo HTTP_PATH?>/news-event">NEWS & EVENTS</a>
                        </li>
                        <span class="line d-none d-md-none d-lg-block d-xl-block d-xxl-block"></span>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($CUR_SCRIPT_NAME == 'contact-us.php') echo 'active'?>" href="<?php echo HTTP_PATH?>/contact-us">CONTACT</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!-- fab menu -->
    <div class="fab">
        <div class="mainop">
            <img src="<?php echo HTTP_PATH_ASSET?>/images/cart.png" width="30px">
        </div>
        <div id="docs" class="minifab op1">
            <a href="https://amzn.to/3CJs4ze" target="_blank"><img class="minifabIcon" src="<?php echo HTTP_PATH_ASSET?>/images/amazon.png"></a>
        </div>
        <div id="sheets" class="minifab op2">
            <a href="https://bit.ly/3sgnUKd" target="_blank"><img class="minifabIcon" src="<?php echo HTTP_PATH_ASSET?>/images/flipkart.png"></a>
        </div>
    </div>

    <div id="user-login-top" class="enquiry-button">
        <a href="Javascript:void(0);">
            <img src="<?php echo HTTP_PATH_ASSET?>/images/oil.svg" class="animate__animated animate__flash animate__infinite animate__slower"
                width="70" height="70" style="border: 2px solid #ffffff; border-radius: 45px;">
        </a>
    </div>