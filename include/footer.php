<section class="bg-gallery">
        <div class="container-fluid text-center py-4">
            <div class="text-white py-5 mb-0 font-bebas-neue ft-4">WE SPREAD ALL OVER INDIA</div>
            <div class="row my-5">
                <div class="col-sm-3">
                    <i class="fa fa-cubes fs-1 text-white"></i>
                    <div class="text-white ft-61 font-bebas-neue" data-max="<?php echo $WEB_SETUP_INFO_ARR[0]['distributors']?>">+ </div>
                    <p class="text-white fs-3 font-bebas-neue">DISTRIBUTOR</p>
                </div>
                <div class="col-sm-3">
                    <i class="fa fa-smile-o fs-1 text-white"></i>
                    <div class="text-white ft-61 font-bebas-neue" data-max="<?php echo $WEB_SETUP_INFO_ARR[0]['happyCustomer']?>">+ </div>
                    <p class="text-white fs-3 font-bebas-neue">HAPPY CUSTOMER</p>
                </div>
                <div class="col-sm-3">
                    <i class="fa fa-trophy fs-1 text-white"></i>
                    <div class="text-white ft-61 font-bebas-neue" data-max="<?php echo $WEB_SETUP_INFO_ARR[0]['awards']?>">+ </div>
                    <p class="text-white fs-3 font-bebas-neue">AWARDS</p>
                </div>
                <div class="col-sm-3">
                    <i class="fa fa-building fs-1 text-white"></i>
                    <div class="text-white ft-61 font-bebas-neue" data-max="<?php echo $WEB_SETUP_INFO_ARR[0]['statesCovered']?>" id="test">+ </div>
                    <p class="text-white fs-3 font-bebas-neue">STATES COVERED</p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3500.2415246507476!2d77.06637091542999!3d28.682420888571304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d036b3fffffff%3A0xdb77a736e01e1a32!2sGargo%20International!5e0!3m2!1sen!2sin!4v1622878859354!5m2!1sen!2sin"
            width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </section>
    <footer class="p-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <img src="<?php echo HTTP_PATH_ASSET?>/images/logo.png" class="img-fluid mb-3" width="120">
                    <p class="text-white"><?php echo nl2br($WEB_SETUP_INFO_ARR[0]['footerTxt'])?></p>
                    <div class="heading font-bebas-neue">
                        FOLLOW US
                    </div>
                    <a href="<?php echo $WEB_SETUP_INFO_ARR[0]['facebookUrl']?>" target="_blank"><i class="fa fa-facebook-square fa-2x text-white me-3" aria-hidden="true"></i></a>
                    <a href="<?php echo $WEB_SETUP_INFO_ARR[0]['twitterUrl']?>" target="_blank"><i class=" fa fa-twitter-square fa-2x text-white me-3" aria-hidden="true"></i></a>
                    <a href="<?php echo $WEB_SETUP_INFO_ARR[0]['linkedinUrl']?>" target="_blank"><i class="fa fa-linkedin-square fa-2x text-white me-3" aria-hidden="true"></i></a>
                    <a href="<?php echo $WEB_SETUP_INFO_ARR[0]['instagramUrl']?>" target="_blank"><i class="fa fa-instagram fa-2x text-white me-3" aria-hidden="true"></i></a>
                    <a href="<?php echo $WEB_SETUP_INFO_ARR[0]['youtubeUrl']?>" target="_blank"><i class=" fa fa-youtube-square fa-2x text-white" aria-hidden=" true"></i></a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="heading font-bebas-neue">
                        WRITE US
                    </div>
                    <div class="d-flex align-items-center py-2">
                        <div class="flex-shrink-0">
                            <img src="<?php echo HTTP_PATH_ASSET?>/images/call.png" alt="..." width="34" height="34">
                        </div>
                        <div class="flex-grow-1 ms-3 text-white"><?php echo $WEB_SETUP_INFO_ARR[0]['footerEnquiry']?></div>
                    </div>
                    <div class="d-flex align-items-center py-2">
                        <div class="flex-shrink-0">
                            <img src="<?php echo HTTP_PATH_ASSET?>/images/email.png" alt="<?php echo $WEB_SETUP_INFO_ARR[0]['infoEmail']?>" width="34" height="34">
                        </div>
                        <div class="flex-grow-1 ms-3 text-white">
                            <a class="text-white" href="mailto:<?php echo $WEB_SETUP_INFO_ARR[0]['infoEmail']?>"><?php echo $WEB_SETUP_INFO_ARR[0]['infoEmail']?></a>
                        </div>
                    </div>
                    <div class=" d-flex align-items-center py-2">
                        <div class="flex-shrink-0">
                            <img src="<?php echo HTTP_PATH_ASSET?>/images/location'.png" alt="..." width="34" height="34">
                        </div>
                        <div class="flex-grow-1 ms-3 text-white"><?php echo nl2br($WEB_SETUP_INFO_ARR[0]['address'])?></div>
                    </div>


                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <form action="thankyou" method="post" onClick="frmValidation();">
                        <div class="col-sm-12 bg-white rounded-3 p-3" id="contactform">
                            <p class="text-danger fw-bold">Get In Touch</p>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Your Name" id="name" name="fname" value="" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group mb-3">
                                        <input type="email" class="form-control" placeholder="Your Email Id" id="email" name="email" value="" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
<?php
$formToken = date('U');
$_SESSION['FORM_TOKEN'] = $formToken;
?>
                                    <div class="form-group mb-3">
                                        <input type="number" class="form-control" placeholder="Your Phone Number" id="phone" name="phone" value="" required maxlength="10" minlength="10">
                                        <input type="hidden" name="formToken" value="<?php echo $formToken?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" id="comment" placeholder="Message:" required name="message"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <div class="getintouchBtn">
									<button type="sumbit" class="btn btn-primary btn-block">
											Send message
                                            <img src="<?php echo HTTP_PATH_ASSET?>/img/right-arrow_withour_border_white.svg" alt="">
									</button> </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>
    <div class="p-3" style="background: #252b31;">
    <div class="container">
      <div
        class="d-flex justify-content-sm-center justify-content-md-between justify-content-lg-between justify-content-xl-between justify-content-xxl-between">
        <div class="text-secondary">
          © <?php echo $cpYearInfo?> Adrol. All Rights Reserved.
        </div>
        <!--<div class="text-secondary">-->
        <!--  Site Design & Developed By <strong>Clouddecorators</strong>-->
        <!--</div>-->
      </div>
    </div>
  </div>
    <div id="user-login-wrapper" class="getintouch" style="display:none;">
        <div class="fs-5 fw-bold text-danger mb-3 font-bebas-neue">Find the Right Oil <span
                class="text-end float-end"><a class="close text-secondary fs-6" href="Javascript:void();"><i
                        class="fa fa-close fw-light"></i></a></span></div class="fs-4 mb-3">

        <form method="post" action="<?php echo HTTP_PATH?>/search">
            <div class="row">
                <div class="col-md-6 col-lg-12 mb-3">
<?php
				$ajaxRequestUrl = HTTP_PATH.'/ajax-call.php';
				$vehicleTypeInfoArr = $objDBQuery->getRecord(0, array('vtid', 'vname'), 'tbl_vehicle_type', '', '', '', 'vname', 'ASC');
				makeDropDownFromDB('vtid', $vehicleTypeInfoArr, 'vtid', 'vname', '', "class='form-select' data-live-search='true' aria-label='Default select example' required", '', "onchange=\"setDropDownValue(this.value, 'manufacturerId', 'getManufacturerList', '$ajaxRequestUrl');\"", '', 'Select Vehical Type');			
				
?>              </div>
                <div class="col-md-6 col-lg-12 mb-3" id="manufacturerId">				
                    <select class="form-select" aria-label="Default select example" id="bid">
                        <option>Select Manufacturer</option>                        
                    </select>
					</div>
                </div>
                <div class="col-md-6 col-lg-12 mb-3" id="modelId">
                    <select class="form-select" aria-label="Default select example">
                        <option selected>Select Model</option>
                    </select>
                </div>

                <div class="col-lg-12 my-3">
                    <div class="getintouchBtn"> <button type="submit"
                            class="btn btn-primary btn-block font-bebas-neue fw-normal">Search
                            <img src="<?php echo HTTP_PATH_ASSET?>/img/right-arrow_withour_border_white.svg" alt=""></button> </div>
                </div>
            </div>
        </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src='<?php echo HTTP_PATH_ASSET?>/js/owl.carousel.min.js'></script>
    <script src="<?php echo HTTP_PATH_ASSET?>/js/custom.js"></script>    
</body>

</html>